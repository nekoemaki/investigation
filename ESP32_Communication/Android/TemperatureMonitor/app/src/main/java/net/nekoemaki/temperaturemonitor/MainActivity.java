package net.nekoemaki.temperaturemonitor;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import net.nekoemaki.temperaturemonitor.BLECommunication.BLECommunication;
import net.nekoemaki.temperaturemonitor.BLECommunication.BLEEventListener;
import net.nekoemaki.temperaturemonitor.Common.Logger;
import net.nekoemaki.temperaturemonitor.WifiCommunication.WifiBroadcast;
import net.nekoemaki.temperaturemonitor.WifiCommunication.WifiClient;
import net.nekoemaki.temperaturemonitor.WifiCommunication.WifiEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView temperatureTextView;
    private TextView humidityTextView;
    private TextView illuminanceTextView;

    private TextView modeView;
    private TextView connectStatusView;

    private Spinner devicesSpinner;
    private Button searchDeviceButton;
    private Button connectButton;
    private Button settingButton;
    private Button switchModeButton;
    private Button resetWifiButton;

    private Point dispSizePoint;

    private WifiClient wifiClient;
    private String currentIpAddress;
    private ArrayList<String> deviceIpList;

    private BLECommunication bleCommunication;
    private ArrayList<BluetoothDevice> bluetoothDevices;

    private boolean bleMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WindowManager wm = getWindowManager();
        Display disp = wm.getDefaultDisplay();
        dispSizePoint = new Point();
        disp.getSize(dispSizePoint);

        //---------------------------------------------------------------------------------

        modeView = findViewById(R.id.modeTextView);
        modeView.setText("MODE : BLE");

        //---------------------------------------------------------------------------------

        temperatureTextView = findViewById(R.id.temperatureTextView);
        humidityTextView = findViewById(R.id.humidityTextView);
        illuminanceTextView = findViewById(R.id.illuminanceTextView);

        connectStatusView = findViewById(R.id.statusTextView);

        //---------------------------------------------------------------------------------

        // select device spinner
        devicesSpinner = findViewById(R.id.devicesSpinner);

        // connect to device button
        connectButton = findViewById(R.id.connectButton);
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectButtonEvent();
            }
        });

        //---------------------------------------------------------------------------------

        // switch mode button
        switchModeButton = findViewById(R.id.switchModeButton);
        switchModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bleMode = !bleMode;
                if (bleMode) {
                    if ((wifiClient != null) && (wifiClient.isConnected())) {
                        wifiClient.disconnect();
                    }
                    if (WifiBroadcast.isActive()) {
                        WifiBroadcast.stop();
                    }
                    if (bleCommunication == null) {
                        modeView.setText("MODE : BLE (disable)");
                    } else {
                        modeView.setText("MODE : BLE");
                    }
                } else {
                    if (bleCommunication.isConnected()) {
                        bleCommunication.disconnect();
                    }
                    modeView.setText("MODE : WiFi");
                    ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    Network network = connectivityManager.getActiveNetwork();
                    NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(network);

                    if (capabilities != null) {
                        if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                            if (!WifiBroadcast.isActive()) {
                                WifiBroadcast.start(getApplicationContext(), 9000, wifiBroadcastEventListener);
                            }
                        }
                    }
                }
                deviceIpList.clear();
                bluetoothDevices.clear();
                devicesSpinner.setAdapter(null);
            }
        });

        // reset wifi setting button
        resetWifiButton = findViewById(R.id.resetSsidButton);
        resetWifiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (wifiClient.isConnected()) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("commandId", 5);
                        wifiClient.sendData(jsonObject);
                    }
                } catch (JSONException je) {
                    je.printStackTrace();
                }
            }
        });

        // find device button
        searchDeviceButton = findViewById(R.id.searchDeviceButton);
        searchDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findDevice();
            }
        });

        // setting button
        settingButton = findViewById(R.id.settingButton);
        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingButtonEvent();
            }
        });

        //---------------------------------------------------------------------------------

        bleMode = true;
        currentIpAddress = "";
        deviceIpList = new ArrayList<>();

        if (getApplicationContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            BluetoothManager bluetoothManager = (BluetoothManager) getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
            if (bluetoothManager != null) {
                bleCommunication = new BLECommunication(getApplicationContext());
                bleCommunication.setBleEventListener(bleEventListener);
            }
        }

        bluetoothDevices = new ArrayList<>();

        showStatusHandler.postDelayed(showStatusRunnable, 100);
    }

    /**
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                bleCommunication = new BLECommunication(getApplicationContext());
                bleCommunication.setBleEventListener(bleEventListener);
            }
        }
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();

        if (!bleMode) {
            ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            Network network = connectivityManager.getActiveNetwork();
            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(network);

            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    if (!WifiBroadcast.isActive()) {
                        WifiBroadcast.start(getApplicationContext(), 9000, wifiBroadcastEventListener);
                    }
                }
            }
        }
    }

    /**
     *
     */
    private void findDevice() {

        if (bleMode) {
            if (bleCommunication != null) {
                statusText = "scan ble devices";
                bluetoothDevices.clear();
                devicesSpinner.setAdapter(null);
                bleCommunication.startScan();
            }

        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            Network network = connectivityManager.getActiveNetwork();
            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(network);

            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    if (!WifiBroadcast.isActive()) {
                        WifiBroadcast.start(getApplicationContext(), 9000, wifiBroadcastEventListener);
                    }

                    if (WifiBroadcast.isActive()) {
                        statusText = "scan wifi devices";
                        deviceIpList.clear();
                        devicesSpinner.setAdapter(null);

                        try {
                            JSONObject data = new JSONObject();
                            data.put("commandId", 1);
                            WifiBroadcast.sendData(data);
                            finishedBroadcastHandler.postDelayed(finishedBroadcastRunnable, 3000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Logger.addLog("send broadcast");
                    }
                }
            }
        }
    }

    /**
     *
     */
    private void connectButtonEvent() {
        if (bleMode) {
            if (bleCommunication != null) {
                if (bleCommunication.isConnected()) {
                    statusText = "disconnecting ...";
                    bleCommunication.disconnect();
                } else {
                    if (bluetoothDevices.size() > 0) {
                        statusText = "connecting ...";
                        bleCommunication.connect(bluetoothDevices.get(devicesSpinner.getSelectedItemPosition()));
                    }
                }
            }

        } else {
            if (wifiClient == null) {
                wifiClient = new WifiClient(getApplicationContext());
                wifiClient.setWifiEventListener(wifiTcpEventListener);
            }

            if (wifiClient.isConnected()) {
                statusText = "disconnecting ...";
                wifiClient.disconnect();

            } else {
                if (deviceIpList.size() > 0) {
                    statusText = "connecting ...";

                    ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    Network network = connectivityManager.getActiveNetwork();
                    NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(network);

                    if (capabilities != null) {
                        if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {

                            //wifiClient.connect(deviceIpList.get(devicesSpinner.getSelectedItemPosition()), 9001);
                            wifiClient.connect(deviceIpList.get(0), 9001);
                        }
                    }

                }
            }

        }
    }

    /**
     *
     */
    private void settingButtonEvent() {
        if (!bleMode) {
            if (bleCommunication != null) {
                Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        }
    }


    private final Handler sendBleHandler = new Handler();
    private final Runnable sendBleRunnable = new Runnable() {
        @Override
        public void run() {
            sendBleHandler.removeCallbacks(sendBleRunnable);

            if (bleCommunication.isConnected()) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("commandId", 10);
                    bleCommunication.sendData(jsonObject.toString());
                } catch (JSONException je) {
                    je.printStackTrace();
                }
                sendBleHandler.postDelayed(sendBleRunnable, 3000);
            }
        }
    };

    private final Handler finishedBroadcastHandler = new Handler();
    private final Runnable finishedBroadcastRunnable = new Runnable() {
        @Override
        public void run() {
            finishedBroadcastHandler.removeCallbacks(finishedBroadcastRunnable);

            statusText = "";

            String devices[] = new String[deviceIpList.size()];
            deviceIpList.toArray(devices);
            ArrayAdapter<String> deviceAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_text_item, devices);
            deviceAdapter.setDropDownViewResource(R.layout.spinner_text_item);
            devicesSpinner.setAdapter(deviceAdapter);
        }
    };


    private void setBleDeviceListView() {
        Logger.addLog("show ble device list");
        if (bluetoothDevices.size() > 0) {
            String devices[] = new String[bluetoothDevices.size()];
            for (int i = 0; i < devices.length; i++) {
                devices[i] = bluetoothDevices.get(i).getName();
                if (devices[i] == null) {
                    devices[i] = bluetoothDevices.get(i).getAddress();
                }
            }

            ArrayAdapter<String> bleDeviceAdapter = new ArrayAdapter<>(this, R.layout.spinner_text_item, devices);
            bleDeviceAdapter.setDropDownViewResource(R.layout.spinner_text_item);
            devicesSpinner.setAdapter(bleDeviceAdapter);
        }
    }

    /**
     *
     */
    private WifiEventListener wifiBroadcastEventListener = new WifiEventListener() {
        @Override
        public void onReceivedData(String address, JSONObject receivedData) {
            super.onReceivedData(address, receivedData);

            try {
                if (receivedData.getInt("commandId") == 2) {
                    String ipaddress = receivedData.getString("ipaddress");
                    if (deviceIpList.indexOf(ipaddress) < 0) {
                        deviceIpList.add(receivedData.getString("ipaddress"));
                    }
                }
            } catch(JSONException je) {
                je.printStackTrace();
            }
        }
    };

    /**
     *
     */
    private WifiEventListener wifiTcpEventListener = new WifiEventListener() {
        @Override
        public void onConnected(String address) {
            super.onConnected(address);

            currentIpAddress = address;
            statusText = "connected";
            //setConnectButtonText("disconnect");
        }

        @Override
        public void onDisconnected(String address) {
            super.onDisconnected(address);

            if (address.equals(currentIpAddress)) {
                currentIpAddress = "";
                statusText = "disconnected";
                //setConnectButtonText("connect");
            }
        }

        @Override
        public void onReceivedData(String address, JSONObject receivedData) {
            super.onReceivedData(address, receivedData);

            try {
                if (receivedData.getInt("commandId") == 10) {
                    curTemperature = (float)receivedData.getDouble("temperature");
                    curHumidity = (float)receivedData.getDouble("humidity");
                    curIlluminance = (float)receivedData.getDouble("illuminance");
                    showEnvHandler.post(showEnvRunnable);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     *
     */
    private BLEEventListener bleEventListener = new BLEEventListener() {
        @Override
        public void onConnected(String deviceName) {
            super.onConnected(deviceName);

            statusText = "connected";
            //setConnectButtonText("disconnect");
            sendBleHandler.postDelayed(sendBleRunnable, 3000);
        }

        @Override
        public void onDisconnected(String deviceName) {
            super.onDisconnected(deviceName);

            statusText = "";
            //setConnectButtonText("connect");
            sendBleHandler.removeCallbacks(sendBleRunnable);
        }

        @Override
        public void onFoundDevice(BluetoothDevice device) {
            super.onFoundDevice(device);

            if ((bluetoothDevices.indexOf(device) < 0) && (device.getName() != null)) {
                bluetoothDevices.add(device);
            }
        }

        @Override
        public void onFinishedScan() {
            super.onFinishedScan();

            statusText = "";

            setBleDeviceListView();
        }

        @Override
        public void onReceivedData(JSONObject receivedData) {
            super.onReceivedData(receivedData);

            try {
                if (receivedData.getInt("commandId") == 10) {
                    curTemperature = (float)receivedData.getDouble("temperature");
                    curHumidity = (float)receivedData.getDouble("humidity");
                    curIlluminance = (float)receivedData.getDouble("illuminance");
                    showEnvHandler.post(showEnvRunnable);
                }
            } catch(JSONException je) {
                je.printStackTrace();
            }
        }
    };

    private float curTemperature;
    private float curHumidity;
    private float curIlluminance;
    private Handler showEnvHandler = new Handler();
    private Runnable showEnvRunnable = new Runnable() {
        @Override
        public void run() {
            showEnvHandler.removeCallbacks(showEnvRunnable);
            temperatureTextView.setText(String.format("%f C", curTemperature));
            humidityTextView.setText(String.format("%f %%", curHumidity));
            illuminanceTextView.setText(String.format("%f %%", curIlluminance));
        }
    };


    private String statusText;
    private Handler showStatusHandler = new Handler();
    private Runnable showStatusRunnable = new Runnable() {
        @Override
        public void run() {
            showStatusHandler.removeCallbacks(showStatusRunnable);
            connectStatusView.setText(statusText);
            showStatusHandler.postDelayed(showStatusRunnable, 100);
        }
    };
}
