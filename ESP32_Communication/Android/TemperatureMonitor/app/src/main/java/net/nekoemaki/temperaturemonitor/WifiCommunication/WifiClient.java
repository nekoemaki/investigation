package net.nekoemaki.temperaturemonitor.WifiCommunication;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import net.nekoemaki.temperaturemonitor.Common.Logger;

import org.json.JSONObject;

import static android.content.Context.WIFI_SERVICE;


/**
 * WifiClient class
 */
public class WifiClient {

    private Socket wifiSocket;

    private WifiEventListener wifiEventListener;
    private WifiClientTask wifiClientTask;

    private String selfIpAddress;
    private String ipAddress;
    private int port;

    /**
     * Constructor
     */
    public WifiClient(Context context) {
        wifiSocket = null;
        wifiEventListener = null;
        wifiClientTask = null;

        WifiManager manager = (WifiManager)context.getSystemService(WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        int ipAddr = info.getIpAddress();
        ByteBuffer ipBuf = ByteBuffer.allocate(4);
        ipBuf.order(ByteOrder.LITTLE_ENDIAN);
        byte[] addrBytes = ipBuf.putInt(ipAddr).array();
        selfIpAddress = String.format("%d.%d.%d.%d", addrBytes[0]&0xFF, addrBytes[1]&0xFF, addrBytes[2]&0xFF, addrBytes[3]&0xFF);

        ipAddress = "";
        port = 9002;
    }

    /**
     * get connected status
     * @return
     */
    public boolean isConnected() {
        return (wifiSocket != null) && (wifiSocket.isConnected());
    }

    /**
     * set ip address
     * @param address
     * @return
     */
    public boolean setAddress(String address) {
        if (isConnected()) {
            return false;
        } else {
            ipAddress = address;
            return true;
        }
    }

    /**
     * get ip address
     * @return
     */
    public String getAddress() {
        return ipAddress;
    }

    /**
     * set port
     * @param portNumber
     * @return
     */
    public boolean setPort(int portNumber) {
        if (isConnected()) {
            return false;
        } else {
            port = portNumber;
            return true;
        }
    }

    /**
     * get port
     * @return
     */
    public int getPort() {
        return port;
    }

    /**
     * set event callback
     * @param callback
     */
    public void setWifiEventListener(WifiEventListener callback) {
        wifiEventListener = callback;
    }

    /**
     * find devices
     * @return
     */
    private String[] findDevices() {
        
        return null;
    }


    /**
     * connect to Device
     * @return
     */
    public boolean connect() {
        if (!isConnected() && (ipAddress != null) && (!ipAddress.equals("")) && (port != 0)) {
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        Logger.addLog(String.format("connect to %s:%d", ipAddress, port));
                        wifiSocket = new Socket();
                        wifiSocket.connect(new InetSocketAddress(ipAddress, port), 10000); // connect timeout is 10seconds

                        if (wifiSocket.isConnected()) {
                            wifiClientTask = new WifiClientTask(selfIpAddress, wifiSocket, wifiEventListener);

                            if (wifiEventListener != null) {
                                wifiEventListener.onConnected(ipAddress);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Logger.addLog("connection failed");

                        if (wifiClientTask != null) {
                            wifiClientTask.disconnect();
                            wifiClientTask = null;
                        }

                        if (wifiSocket != null) {
                            try {
                                wifiSocket.close();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            wifiSocket = null;
                        }

                        if (wifiEventListener != null) {
                            wifiEventListener.onConnectFailed(ipAddress);
                        }
                    }
                }
            }.start();

            return true;
        } else {
            return false;
        }
    }

    /**
     * connect to Device
     * @param address
     * @param portNumber
     * @return
     */
    public boolean connect(String address, int portNumber) {
        if (isConnected()) {
            return false;
        }

        ipAddress = address;
        port = portNumber;
        return connect();
    }

    /**
     * disconnect socket
     */
    public void disconnect() {
        if (wifiClientTask != null) {
            wifiClientTask.disconnect();
            wifiClientTask = null;
        }

        if (wifiSocket != null) {
            try {
                wifiSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            wifiSocket = null;
        }

        if (wifiEventListener != null){
            wifiEventListener.onDisconnected(ipAddress);
        }
    }

    /**
     * send data to connected socket
     * @return
     */
    public boolean sendData(JSONObject data) {
        if (wifiClientTask != null) {
            wifiClientTask.sendData(data);
            return true;
        } else {
            return false;
        }
    }
}

