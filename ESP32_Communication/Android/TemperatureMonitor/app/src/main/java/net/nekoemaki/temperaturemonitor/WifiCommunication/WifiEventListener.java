package net.nekoemaki.temperaturemonitor.WifiCommunication;

import org.json.JSONObject;

public class WifiEventListener {
    public void onConnected(String address){}
    public void onConnectFailed(String address){}
    public void onDisconnected(String address){}
    public void onSentData(String address){}
    public void onReceivedData(String address, JSONObject receivedData){}
}
