package net.nekoemaki.temperaturemonitor.BLECommunication;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Handler;

import net.nekoemaki.temperaturemonitor.Common.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class BLECommunication {
    private static String DATA_HEADER = "COMMUNICATION_HEADER";

    private Context context;

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothLeScanner bluetoothLeScanner;
    private ArrayList<BluetoothDevice> deviceList;
    private BluetoothDevice bluetoothDevice;
    private BluetoothGatt bluetoothGatt;
    private BLEEventListener bleEventListener;

    private boolean scaning = false;

    private int bleGattMtu;
    private int changedBleGattMtu = 23;
    private boolean bleGattMtuRequesting = false;

    private final UUID SERVICE_UUID = UUID.fromString("7c73ee15-c58f-44ef-b9a7-3e128e651012");
    private final UUID CHARACTERISTIC_UUID = UUID.fromString("fd9b8ae1-55aa-4c31-89f8-6f2dfbb0c046");
    private final UUID DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");


    /**
     * constructor
     * @param context
     */
    public BLECommunication(Context context) {
        this.context = context;
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = bluetoothManager.getAdapter();
        this.bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
        this.deviceList = new ArrayList<>();
        this.bluetoothDevice = null;
        this.bluetoothGatt = null;
        this.scaning = false;
    }


    /**
     * set ble notify
     */
    private void setNotify() {
        if ((bluetoothGatt != null)  && (SERVICE_UUID != null)) {
            BluetoothGattService service = bluetoothGatt.getService(SERVICE_UUID);
            if ((service != null) && (CHARACTERISTIC_UUID != null)) {
                BluetoothGattCharacteristic characteristic = service.getCharacteristic(CHARACTERISTIC_UUID);
                if ((characteristic != null) && (DESCRIPTOR_UUID != null)) {
                    bluetoothGatt.setCharacteristicNotification(characteristic, true);
                    BluetoothGattDescriptor descriptor = characteristic.getDescriptor(DESCRIPTOR_UUID);
                    if (descriptor != null) {
                        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        bluetoothGatt.writeDescriptor(descriptor);
                    }
                }
            }
        }
    }

    /**
     * set event listener
     * @param eventListener
     */
    public void setBleEventListener(BLEEventListener eventListener) {
        this.bleEventListener = eventListener;
    }

    /**
     * is changing mtu
     * @return
     */
    public boolean isChangingMTU(){
        return bleGattMtuRequesting;
    }

    /**
     * get mtu
     */
    public int getMTU(){
        return changedBleGattMtu;
    }

    /**
     * set mtu
     * @param mtu
     */
    public void setMTU(int mtu){
        if (mtu < 0) return;

        bleGattMtu = mtu;
        bleGattMtuRequesting = true;

        if (bluetoothGatt != null) {
            bleGattRequestMtuHandler.post(bleGattRequestMtuRunnable);
        }
    }

    /**
     * start scan ble devices
     */
    public void startScan() {
        if (!scaning) {
            try {
                Logger.addLog("start scan");
                scaning = true;
                deviceList.clear();
                ScanSettings scanSettings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_POWER).build();
                bluetoothLeScanner.startScan(null, scanSettings, bleScanCallback);
                bleScanHandler.postDelayed(bleScanRunnable, 1000);
            } catch(Exception e) {
                Logger.addErrorLog("start scan ble error : " + e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * stop scan ble devices
     */
    public void stopScan() {
        if (scaning) {
            Logger.addLog("stop scan");
            scaning = false;
            bluetoothLeScanner.stopScan(bleScanCallback);

            if (bleEventListener != null) bleEventListener.onFinishedScan();
        }
    }

    /**
     * get scaned ble devices addresses
     * @return
     */
    public String[] getBLEDeviceAddresses() {
        if (deviceList.size() == 0) {
            return null;
        } else {
            String[] deviceNames = new String[deviceList.size()];
            for (int i=0; i<deviceList.size(); i++) {
                deviceNames[i] = deviceList.get(i).getAddress();
            }
            return deviceNames;
        }
    }

    /**
     * get scaned ble device names
     * @return
     */
    public String[] getBLEDeviceNames() {
        if (deviceList.size() == 0) {
            return null;
        } else {
            String[] deviceNames = new String[deviceList.size()];
            for (int i=0; i<deviceList.size(); i++) {
                deviceNames[i] = deviceList.get(i).getName();
                if (deviceNames[i] == null) { deviceNames[i] = "none"; }
            }
            return deviceNames;
        }
    }

    /**
     * get service uuids
     * @return
     */
    public String[] getServiceUuids() {
        if (bluetoothGatt == null) {
            return null;
        } else {
            List<BluetoothGattService> services = bluetoothGatt.getServices();
            if (services.size() == 0) {
                return null;
            } else {
                String[] ret = new String[services.size()];
                for (int i=0; i<services.size(); i++) {
                    ret[i] = services.get(i).getUuid().toString();
                }
                return ret;
            }
        }
    }

    /**
     * get characteristic uuids
     * @param serviceUuid
     * @return
     */
    public String[] getCharacteristicUuids(String serviceUuid) {
        if (bluetoothGatt == null) {
            return null;
        } else {
            BluetoothGattService service = bluetoothGatt.getService(UUID.fromString(serviceUuid));
            if (service == null) {
                return null;
            } else {
                List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                if (characteristics.size() == 0) {
                    return null;
                } else {
                    String[] ret = new String[characteristics.size()];
                    for (int i=0; i<characteristics.size(); i++) {
                        ret[i] = characteristics.get(i).getUuid().toString();
                    }
                    return ret;
                }
            }
        }
    }

    /**
     * get descriptor uuid
     * @param serviceUuid
     * @param characteristicsUuid
     * @return
     */
    public String[] getDescriptorUuids(String serviceUuid, String characteristicsUuid) {
        if (bluetoothGatt == null) {
            return null;
        } else {
            BluetoothGattService service = bluetoothGatt.getService(UUID.fromString(serviceUuid));
            if (service == null) {
                return null;
            } else {
                BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(characteristicsUuid));
                if (characteristic == null) {
                    return null;
                } else {
                    List<BluetoothGattDescriptor> descriptors = characteristic.getDescriptors();
                    if (descriptors.size() == 0) {
                        return null;
                    } else {
                        String[] ret = new String[descriptors.size()];
                        for (int i = 0; i < descriptors.size(); i++) {
                            ret[i] = descriptors.get(i).getUuid().toString();
                        }
                        return ret;
                    }
                }
            }
        }
    }

    /**
     * is scaning
     * @return
     */
    public boolean isScaning() {
        return scaning;
    }

    /**
     * get connected status
     * @return
     */
    public boolean isConnected() {
        if (bluetoothGatt == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * connect ble device by address
     * @param address
     */
    public void connectByAddress(String address) {
        for (BluetoothDevice device : deviceList) {
            if (device.getAddress().equals(address)) {
                connect(device);
                break;
            }
        }
    }

    /**
     * connect ble device
     * @param device
     */
    public void connect(BluetoothDevice device) {
        if (!isConnected()) {
            bluetoothDevice = device;
            if (bluetoothDevice != null) {
                Logger.addLog(String.format("connect to ble device %s", device.getAddress()));
                BluetoothGatt gatt = bluetoothDevice.connectGatt(context, false, gattCallback);
                gatt.connect();
            }
        } else {
            Logger.addLog("connect() : already connected");
        }
    }

    /**
     * disconnect ble device
     */
    public void disconnect() {
        if (isConnected()) {
            bluetoothGatt.disconnect();
            Logger.addLog("disconnected ble device");
        }
    }

    /**
     * send data to ble device
     * @param jsonText
     */
    public void sendData(String jsonText) {
        if (isConnected()) {
            List<BluetoothGattService> services = bluetoothGatt.getServices();
            for (BluetoothGattService service : services) {
                if ((SERVICE_UUID == null) || (SERVICE_UUID.compareTo(service.getUuid()) == 0)) {
                    List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                    for (BluetoothGattCharacteristic characteristic : characteristics) {
                        if ((CHARACTERISTIC_UUID == null) || (CHARACTERISTIC_UUID.compareTo(characteristic.getUuid()) == 0)) {

                            byte[] sendBytes = new byte[jsonText.length()+56];

                            // data header
                            System.arraycopy(DATA_HEADER.getBytes(), 0, sendBytes, 0, DATA_HEADER.length());

                            // md5
                            String md5Str = getMD5(jsonText.getBytes());
                            byte[] md5Bytes = md5Str.getBytes();
                            System.arraycopy(md5Bytes, 0, sendBytes, 20, md5Bytes.length);

                            // data size
                            ByteBuffer intBuf = ByteBuffer.allocate(4);
                            intBuf.order(ByteOrder.LITTLE_ENDIAN);
                            intBuf.putInt(jsonText.length());
                            byte[] bDatasize = intBuf.array();
                            System.arraycopy(bDatasize, 0, sendBytes, 52, bDatasize.length);

                            // main data
                            byte[] jsonBytes = jsonText.getBytes();
                            System.arraycopy(jsonBytes, 0, sendBytes, 56, jsonBytes.length);

                            characteristic.setValue(sendBytes);
                            bluetoothGatt.writeCharacteristic(characteristic);

                        }
                    }
                }
            }
        }
    }

    /**
     * scan ble device callback
     */
    private ScanCallback bleScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            if (result != null && result.getDevice() != null) {
                if (deviceList.indexOf(result.getDevice()) < 0) {
                    deviceList.add(result.getDevice());
                    Logger.addLog(String.format("found ble device : %s , %s", result.getDevice().getName(), result.getDevice().getAddress()));

                    if (bleEventListener != null) bleEventListener.onFoundDevice(result.getDevice());
                }
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            switch (errorCode) {
                case SCAN_FAILED_ALREADY_STARTED:
                    Logger.addWarningLog("already scanning ble device");
                    break;
                case SCAN_FAILED_APPLICATION_REGISTRATION_FAILED:
                    Logger.addWarningLog("cannot start ble scan");
                    break;
                case SCAN_FAILED_FEATURE_UNSUPPORTED:
                    Logger.addWarningLog("ble scan is not supported");
                    break;
                case SCAN_FAILED_INTERNAL_ERROR:
                    Logger.addErrorLog("ble scan internal error");
                    break;
            }
        }
    };

    /**
     * stop scan timer
     */
    private Handler bleScanHandler = new Handler();
    private Runnable bleScanRunnable = new Runnable() {
        @Override
        public void run() {
            bleScanHandler.removeCallbacks(bleScanRunnable);
            stopScan();
        }
    };

    /**
     * request mtu
     */
    private Handler bleGattRequestMtuHandler = new Handler();
    private Runnable bleGattRequestMtuRunnable = new Runnable() {
        @Override
        public void run() {
            bleGattRequestMtuHandler.removeCallbacks(bleGattRequestMtuRunnable);

            if ((bluetoothGatt != null) && (bleGattMtuRequesting)){
                bluetoothGatt.requestMtu(bleGattMtu);
                bleGattRequestMtuHandler.postDelayed(bleGattRequestMtuRunnable, 200);
            }
        }
    };

    /**
     * ble gatt callback
     */
    private BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            if (newState == BluetoothGatt.STATE_CONNECTED) {

                Logger.addLog("gatt discoverServices");
                gatt.discoverServices();

            } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {

                String deviceAddress = "";
                if (bluetoothGatt != null) {
                    deviceAddress = bluetoothGatt.getDevice().getAddress();
                    bluetoothGatt.close();
                    bluetoothGatt = null;
                }

                Logger.addLog(String.format("disconnected ble : %s", deviceAddress));

                if (bleEventListener != null) bleEventListener.onDisconnected(deviceAddress);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);

            if (status == BluetoothGatt.GATT_SUCCESS) {

                boolean checkUUID = true;

                if (SERVICE_UUID != null) {
                    BluetoothGattService service = gatt.getService(SERVICE_UUID);
                    if (service == null) {
                        Logger.addLog("ble connected : service uuid is mismatch");
                        checkUUID = false;
                    } else {
                        if (CHARACTERISTIC_UUID != null) {
                            BluetoothGattCharacteristic characteristic = service.getCharacteristic(CHARACTERISTIC_UUID);
                            if (characteristic == null) {
                                Logger.addLog("ble connected : characteristic uuid is mismatch");
                                checkUUID = false;
                            } else {
                                gatt.setCharacteristicNotification(characteristic, true);
                                if (DESCRIPTOR_UUID != null) {
                                    BluetoothGattDescriptor descriptor = characteristic.getDescriptor(DESCRIPTOR_UUID);
                                    if (descriptor == null) {
                                        Logger.addLog("ble connected : descriptor uuid is mismatch");
                                        checkUUID = false;
                                    } else {
                                        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                                        gatt.writeDescriptor(descriptor);
                                    }
                                }
                            }
                        }
                    }
                }
                if (checkUUID) {
                    bluetoothGatt = gatt;

                    changedBleGattMtu = 23;
                    if (bleGattMtu > 0) {
                        bleGattMtuRequesting = true;
                        bleGattRequestMtuHandler.post(bleGattRequestMtuRunnable);
                    }

                    Logger.addLog(String.format("connected ble : %s", bluetoothGatt.getDevice().getAddress()));

                    if (bleEventListener != null) bleEventListener.onConnected(bluetoothGatt.getDevice().getAddress());

                } else {
                    // disconnect when uuid is not match
                    gatt.disconnect();
                    gatt.close();
                }
            }

        }


        private long receivedTime = System.currentTimeMillis();
        private int receivedDataOffset = 0;
        private String receivedMd5;
        private int receivedDatasize;
        private String receivedJsonText;

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            if (CHARACTERISTIC_UUID.toString().equals(characteristic.getUuid().toString())) {
                byte[] notification_data = characteristic.getValue();

                // received data timeout 5seconds
                if (System.currentTimeMillis() - receivedTime > 5000) {
                    receivedDataOffset = 0;
                }

                if (receivedDataOffset == 0) {
                    // data header
                    String str = new String(notification_data);
                    if (str.equals(DATA_HEADER)) {
                        receivedTime = System.currentTimeMillis();
                        receivedDataOffset = notification_data.length;
                        receivedJsonText = "";
                        receivedMd5 = "";
                    }
                } else if (receivedDataOffset < 52) {
                    // md5
                    String str = new String(notification_data);
                    receivedMd5 += str;
                    receivedDataOffset += notification_data.length;
                } else if (receivedDataOffset < 56) {
                    // data size
                    ByteBuffer intBuf = ByteBuffer.wrap(notification_data);
                    intBuf.order(ByteOrder.LITTLE_ENDIAN);
                    receivedDatasize = intBuf.getInt();
                    receivedDataOffset += notification_data.length;
                } else {
                    // main data
                    String str = new String(notification_data);
                    receivedJsonText += str;
                    receivedDataOffset += notification_data.length;

                    // received all data
                    if (receivedJsonText.length() >= receivedDatasize) {
                        // check md5
                        String md5Str = getMD5(receivedJsonText.getBytes());
                        if (md5Str.equals(receivedMd5)) {
                            Logger.addLog(receivedJsonText);
                            // received data callback
                            try {
                                JSONObject data = new JSONObject(receivedJsonText);
                                if (bleEventListener != null) bleEventListener.onReceivedData(data);
                            } catch(JSONException jsonEx) {
                                Logger.addErrorLog(jsonEx.getMessage());
                            }
                        } else {
                            Logger.addWarningLog(String.format("mismatch received data md5 : %s", receivedMd5));
                        }
                        receivedDataOffset = 0;
                    }
                }
            }
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
            bleGattMtuRequesting = false;
            changedBleGattMtu = mtu;
            Logger.addLog(String.format("onMtuChanged ... mtu=%d", mtu));
        }
    };

    /**
     * get md5 from byte array
     * @param data
     * @return
     */
    private static String getMD5(byte[] data) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(data);
            byte[] md5Bytes = md.digest();
            String strMd5 = "";
            for (int i=0; i<md5Bytes.length; i++){
                strMd5 += String.format("%02x", md5Bytes[i]);
            }
            return strMd5;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
