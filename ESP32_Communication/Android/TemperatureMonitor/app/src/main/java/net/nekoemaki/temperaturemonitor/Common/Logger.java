package net.nekoemaki.temperaturemonitor.Common;

import android.util.Log;

public class Logger {
    public static void addLog(String msg) {
        Log.d("temperaturemonitorLog", msg);
    }

    public static void addWarningLog(String msg) {
        Log.w("temperaturemonitorLog", msg);
    }

    public static void addErrorLog(String msg) {
        Log.e("temperaturemonitorLog", msg);
    }

}
