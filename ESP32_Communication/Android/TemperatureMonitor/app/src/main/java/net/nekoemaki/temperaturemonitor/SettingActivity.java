package net.nekoemaki.temperaturemonitor;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.nekoemaki.temperaturemonitor.BLECommunication.BLECommunication;
import net.nekoemaki.temperaturemonitor.BLECommunication.BLEEventListener;
import net.nekoemaki.temperaturemonitor.Common.Logger;

import org.json.JSONObject;

import java.util.ArrayList;

public class SettingActivity extends AppCompatActivity {

    private Button backButton;
    private TextView statusText;
    private Button scanBleButton;
    private Spinner bleDeviceSpinner;
    private EditText wifiSsidEdit;
    private EditText wifiPassEdit;
    private Button sendButton;

    private Activity selfActivity;

    private BluetoothDevice currentDevice;
    private ArrayList<BluetoothDevice> bleDevices;

    private BLECommunication bleCommunication;
    private boolean timeoutFlg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        selfActivity = this;

        backButton = findViewById(R.id.backButton);
        statusText = findViewById(R.id.statusText);
        scanBleButton = findViewById(R.id.scanBleButton);
        bleDeviceSpinner = findViewById(R.id.bleDeviceSpinner);
        wifiSsidEdit = findViewById(R.id.wifiSsidEdit);
        wifiPassEdit = findViewById(R.id.wifiPassEdit);
        sendButton = findViewById(R.id.sendButton);

        wifiSsidEdit.setText("");
        wifiPassEdit.setText("");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        scanBleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanBleButtonEvent();
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendButtonEvent();
            }
        });

        bleDevices = new ArrayList<>();

        bleCommunication = new BLECommunication(getApplicationContext());
        bleCommunication.setBleEventListener(bleEventListener);

        scanBleButtonEvent();

        showStatusHandler.postDelayed(showStattusRunnable, 100);
    }

    private void scanBleButtonEvent() {
        statusMessage = "scan ble device ...";
        bleDevices.clear();
        bleDeviceSpinner.setAdapter(null);
        bleCommunication.startScan();

    }

    private void sendButtonEvent() {
        if (bleDevices.size() == 0) {
            showToast("no ble device");
            return;
        }

        if (bleCommunication.isConnected()) {
            sendData();
        } else {
            timeoutFlg = false;
            timeoutMessage = "connect timeout";
            currentDevice = bleDevices.get(bleDeviceSpinner.getSelectedItemPosition());
            bleCommunication.connect(currentDevice);
            statusMessage = "connecting to ble device ...";
            timeoutHandler.postDelayed(timeoutRunnable, 10000);
        }
    }

    private void setDeviceListView() {
        if (bleDevices.size() > 0) {

            String deviceNames[] = new String[bleDevices.size()];
            for (int i = 0; i < bleDevices.size(); i++) {
                deviceNames[i] = bleDevices.get(i).getName();
                if (deviceNames[i] == null) {
                    deviceNames[i] = bleDevices.get(i).getAddress();
                }
            }

            ArrayAdapter<String> bleDeviceAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, deviceNames);
            bleDeviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            bleDeviceSpinner.setAdapter(bleDeviceAdapter);

        }
    }

    private void showToast(String toastMessage) {
        final String message = toastMessage;
        Thread th = new Thread(){
            @Override
            public void run() {
                super.run();
                Toast toast = Toast.makeText(selfActivity, message, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        };
        th.start();
    }

    private void sendData() {
        try{
            retryCnt = 2;
            timeoutFlg = false;
            timeoutMessage = "send data timeout";
            statusMessage = "sending data ...";

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("commandId", 0);
            jsonObject.put("ssid", wifiSsidEdit.getText());
            jsonObject.put("pass", wifiPassEdit.getText());

            bleCommunication.sendData(jsonObject.toString());

            timeoutHandler.postDelayed(timeoutRunnable, 10000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int retryCnt = 0;
    private String timeoutMessage;
    private final Handler timeoutHandler = new Handler();
    private final Runnable timeoutRunnable = new Runnable() {
        @Override
        public void run() {
            timeoutHandler.removeCallbacks(timeoutRunnable);
            if (retryCnt == 0) {
                timeoutFlg = true;
                statusMessage = timeoutMessage;
            } else {
                retryCnt--;

                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("commandId", 0);
                    jsonObject.put("ssid", wifiSsidEdit.getText());
                    jsonObject.put("pass", wifiPassEdit.getText());
                    bleCommunication.sendData(jsonObject.toString());
                    timeoutHandler.postDelayed(timeoutRunnable, 10000);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private String statusMessage = "";
    private final Handler showStatusHandler = new Handler();
    private final Runnable showStattusRunnable = new Runnable() {
        @Override
        public void run() {
            showStatusHandler.removeCallbacks(showStattusRunnable);
            statusText.setText(statusMessage);
            showStatusHandler.postDelayed(showStattusRunnable, 100);
        }
    };

    private Handler sendSettingHandler = new Handler();
    private Runnable sendSettingRunnable = new Runnable() {
        @Override
        public void run() {
            sendSettingHandler.removeCallbacks(sendSettingRunnable);
            sendData();
        }
    };

    private BLEEventListener bleEventListener = new BLEEventListener(){
        @Override
        public void onConnected(String deviceName) {
            super.onConnected(deviceName);

            if (!timeoutFlg) {
                statusMessage = "connected";

                // send data after 3 seconds
                sendSettingHandler.postDelayed(sendSettingRunnable, 3000);

            } else {
                bleCommunication.disconnect();
            }
        }

        @Override
        public void onDisconnected(String deviceName) {
            super.onDisconnected(deviceName);
        }

        @Override
        public void onFoundDevice(BluetoothDevice device) {
            super.onFoundDevice(device);
            Logger.addLog(String.format("found device : %s --- %s", device.getName(), device.getAddress()));
            bleDevices.add(device);
        }

        @Override
        public void onFinishedScan() {
            super.onFinishedScan();
            statusMessage = "";
            setDeviceListView();
        }

        @Override
        public void onReceivedData(JSONObject receivedData) {
            super.onReceivedData(receivedData);

            if (!timeoutFlg) {
                timeoutHandler.removeCallbacks(timeoutRunnable);
                bleCommunication.disconnect();
                finish();
            } else {
                bleCommunication.disconnect();
            }
        }
    };
}
