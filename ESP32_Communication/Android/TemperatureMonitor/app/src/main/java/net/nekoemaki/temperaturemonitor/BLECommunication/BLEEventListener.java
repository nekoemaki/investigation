package net.nekoemaki.temperaturemonitor.BLECommunication;

import android.bluetooth.BluetoothDevice;

import org.json.JSONObject;

public class BLEEventListener {
    public void onFoundDevice(BluetoothDevice device) {}
    public void onFinishedScan() {}
    public void onConnected(String deviceName) {}
    public void onDisconnected(String deviceName) {}
    public void onReceivedData(JSONObject receivedData){}
}
