package net.nekoemaki.temperaturemonitor.WifiCommunication;



import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;

import net.nekoemaki.temperaturemonitor.Common.Logger;

import org.json.JSONObject;

import static android.content.Context.WIFI_SERVICE;

/**
 * WifiSocketServer class
 */
public class WifiServer {

    private ServerSocket serverSocket;
    private WaitConnectionThread waitConnectionThread;

    private String selfIpAddress;
    private int port;
    private WifiEventListener wifiEventListener;

    HashMap<String, WifiClientTask> connectedClients;

    /**
     * constructor
     */
    public WifiServer(Context context) {
        serverSocket = null;
        connectedClients = new HashMap<>();

        WifiManager manager = (WifiManager)context.getSystemService(WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        int ipAddr = info.getIpAddress();
        ByteBuffer ipBuf = ByteBuffer.allocate(4);
        ipBuf.order(ByteOrder.LITTLE_ENDIAN);
        byte[] addrBytes = ipBuf.putInt(ipAddr).array();
        selfIpAddress = String.format("%d.%d.%d.%d", addrBytes[0]&0xFF, addrBytes[1]&0xFF, addrBytes[2]&0xFF, addrBytes[3]&0xFF);

        port = 9002;
    }

    /**
     * set port number
     * @param portNumber
     */
    public void setPort(int portNumber) {
        port = portNumber;
    }

    /**
     * set event callback
     * @param listener
     */
    public void setWifiEventListener(WifiEventListener listener) {
        wifiEventListener = listener;
    }

    /**
     * get connected client name and address
     * @return
     */
    public String[] getConnectedClientAddress() {
        String [] ret = new String[connectedClients.size()];
        int cnt = 0;
        for (String key : connectedClients.keySet()) {
            WifiClientTask cs = connectedClients.get(key);
            ret[cnt] = cs.ipAddress;
        }
        return ret;
    }

    /**
     * return true when exists connected client
     * @return
     */
    public boolean hasConnectedClient() {
        if (connectedClients.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * tcp server is active
     * @return
     */
    public boolean isActive() {
        return (waitConnectionThread != null);
    }

    /**
     * start to wait connecting client
     */
    public boolean startWatingConnection() {
        if (serverSocket == null) {
            try {
                serverSocket = new ServerSocket(port);

                if (serverSocket == null) {
                    return false;
                } else {
                    waitConnectionThread = new WaitConnectionThread();
                    waitConnectionThread.start();
                    return true;
                }

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

        } else {
            return false;
        }
    }

    /**
     * stop to wait connecting client
     */
    public void stopWaitingConnection() {
        if (waitConnectionThread != null) {
            if (waitConnectionThread.isAlive()) {
                waitConnectionThread.interrupt();
                try {
                    waitConnectionThread.join(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            waitConnectionThread = null;
        }

        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            serverSocket = null;
        }
    }

    /**
     * send data to all connected client
     * @param data
     */
    public boolean sendDataAll(JSONObject data) {
        if (hasConnectedClient()) {
            for (String key : connectedClients.keySet()) {
                final WifiClientTask cs = connectedClients.get(key);
                cs.sendData(data);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * send data to connected client by device name
     * @param address
     * @param data
     * @return
     */
    public boolean sendDataByName(String address, JSONObject data) {
        if (hasConnectedClient()) {
            boolean ret = false;
            for (String key : connectedClients.keySet()) {
                final WifiClientTask cs = connectedClients.get(key);
                if (cs.ipAddress.equals(address)) {
                    ret = true;
                    cs.sendData(data);
                }
            }
            return ret;
        } else {
            return false;
        }
    }

    /**
     * send data to connected client by address
     * @param address
     * @param data
     * @return
     */
    public boolean sendDataByAddress(String address, JSONObject data) {
        if (hasConnectedClient()) {
            boolean ret = false;
            for (String key : connectedClients.keySet()) {
                final WifiClientTask cs = connectedClients.get(key);
                if (cs.ipAddress.equals(address)) {
                    ret = true;
                    cs.sendData(data);
                    break;
                }
            }
            return ret;
        } else {
            return false;
        }
    }

    /**
     * disconnect client
     * @param ipAddress
     */
    public void disconnect(String ipAddress) {
        for (String key : connectedClients.keySet()) {
            if (connectedClients.get(key).ipAddress.equals(ipAddress)) {
                connectedClients.get(key).disconnect();
                connectedClients.remove(key);

                if (wifiEventListener != null) {
                    wifiEventListener.onDisconnected(ipAddress);
                }

                break;
            }
        }
    }

    /**
     * disconnect all connected client
     */
    public void disconnect() {
        if (waitConnectionThread != null) {
            if (waitConnectionThread.isAlive()) {
                waitConnectionThread.interrupt();
                try {
                    waitConnectionThread.join(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            waitConnectionThread = null;
        }

        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            serverSocket = null;
        }

        for (String key : connectedClients.keySet()) {
            connectedClients.get(key).disconnect();
        }
        connectedClients.clear();

        if (wifiEventListener != null) {
            wifiEventListener.onDisconnected(null);
        }
    }

    /**
     * wait to connect client thread
     */
    class WaitConnectionThread extends Thread {

        /**
         * constructor
         */
        public WaitConnectionThread() {
            //
        }

        @Override
        public void run() {
            try {

                while ((serverSocket != null)) {
                    if (Thread.interrupted()) {
                        break;
                    }

                    // wait connecting client
                    Socket clientSocket = serverSocket.accept();

                    if (clientSocket != null) {
                        // client connected
                        String clientAddress = clientSocket.getInetAddress().getHostAddress();
                        Logger.addLog(String.format("connected client : %s", clientAddress));

                        // wait to receive data
                        if (connectedClients.containsKey(clientAddress)) {
                            connectedClients.get(clientAddress).setSocket(clientSocket);

                        } else {
                            WifiClientTask task = new WifiClientTask(selfIpAddress, clientSocket, wifiEventListener);
                            connectedClients.put(clientAddress, task);

                        }

                        if (wifiEventListener != null) {
                            wifiEventListener.onConnected(clientAddress);
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

