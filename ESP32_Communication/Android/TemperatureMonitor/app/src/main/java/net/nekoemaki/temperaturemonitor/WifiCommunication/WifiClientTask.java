package net.nekoemaki.temperaturemonitor.WifiCommunication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.util.ArrayList;

import net.nekoemaki.temperaturemonitor.Common.Logger;

/**
 * Wifi : send and receive data task manager
 */
public class WifiClientTask {
    private static final byte[] DATA_HEADER = "COMMUNICATION_HEADER".getBytes();
    private static final int DATASIZE_LIMIT = 10 * 1024;
    private static final int PACKET_SIZE = 512;
    private static final int SEND_DATA_QUE_LIMIT = 10;

    public Socket socket;
    public String selfIpAddress;
    public String ipAddress;
    public SendDataThread sendDataThread;
    public ReceiveDataThread receiveDataThread;

    /**
     * constructor
     * @param sc
     */
    public WifiClientTask(String selfIpAddr, Socket sc, WifiEventListener event) {
        socket = sc;
        if (socket != null) {
            try {
                selfIpAddress = selfIpAddr;
                ipAddress = socket.getInetAddress().getHostAddress();

                receiveDataThread = new ReceiveDataThread(ipAddress, socket, event);
                receiveDataThread.start();

                sendDataThread = new SendDataThread(ipAddress, socket, event);
                sendDataThread.start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param socket
     */
    public void setSocket(Socket socket) {
        this.socket = socket;
        if (receiveDataThread != null) {
            receiveDataThread.setSocket(socket);
        }
        if (sendDataThread != null) {
            sendDataThread.setSocket(socket);
        }
    }

    /**
     * set received data callback
     * @param callback
     */
    public void setReceivedDataCallback(WifiEventListener callback) {
        receiveDataThread.setReceivedDataCallback(callback);
    }

    /**
     *
     * @param callback
     */
    public void setSentDataCallback(WifiEventListener callback) {
        sendDataThread.setSentDataCallback(callback);
    }

    /**
     *
     * @param commandData
     */
    public boolean sendData(JSONObject commandData) {
        return sendDataThread.sendData(commandData);
    }

    /**
     * disconnect client socket
     */
    public void disconnect() {

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket = null;
        }

        if (receiveDataThread != null) {
            receiveDataThread.receiving = false;

            receiveDataThread.interrupt();
            receiveDataThread = null;
        }

        if (sendDataThread != null) {
            sendDataThread.sending = false;

            sendDataThread.interrupt();
            sendDataThread = null;
        }

    }

    /**
     * get md5 from byte array
     * @param data
     * @return
     */
    private String getMD5(byte[] data) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(data);
            byte[] md5Bytes = md.digest();
            String strMd5 = "";
            for (int i=0; i<md5Bytes.length; i++){
                strMd5 += String.format("%02x", md5Bytes[i]);
            }
            return strMd5;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     *
     */
    class SendDataThread extends Thread {
        public boolean sending;
        private String ipAddress;
        private Socket socket;
        private ArrayList<JSONObject> sendDataQue;
        private WifiEventListener sentDataCallback;

        public SendDataThread(String address, Socket socket, WifiEventListener callback) {
            this.ipAddress = address;
            this.socket = socket;
            this.sendDataQue = new ArrayList<>();
            this.sentDataCallback = callback;
        }

        /**
         *
         * @param socket
         */
        public void setSocket(Socket socket) {
            this.socket = socket;
        }

        /**
         *
         * @param callback
         */
        public void setSentDataCallback(WifiEventListener callback) {
            sentDataCallback = callback;
        }

        /**
         *
         * @param data
         */
        public boolean sendData(JSONObject data) {
            synchronized (sendDataQue) {
                if (sendDataQue.size() >= SEND_DATA_QUE_LIMIT) {
                    return false;
                } else {
                    sendDataQue.add(data);
                    return true;
                }
            }
        }

        /**
         *
         */
        @Override
        public void run() {
            sending = true;

            try {

                while(sending) {

                    while (sending && sendDataQue.size() > 0) {
                        JSONObject sendDat;

                        synchronized (sendDataQue) {
                            sendDat = sendDataQue.get(0);
                            sendDataQue.remove(0);
                        }

                        if (sendDat == null) continue;

                        OutputStream outputStream = socket.getOutputStream();

                        String jsonText = sendDat.toString();

                        // send data header (20bytes)
                        outputStream.write(DATA_HEADER);

                        // send md5 (32bytes)
                        String md5Str = getMD5(jsonText.getBytes());
                        outputStream.write(md5Str.getBytes());

                        // send data size (4bytes)
                        ByteBuffer intBuf = ByteBuffer.allocate(4);
                        intBuf.order(ByteOrder.LITTLE_ENDIAN);
                        intBuf.putInt(jsonText.length());
                        outputStream.write(intBuf.array());

                        // send data
                        outputStream.write(jsonText.getBytes());

                        // execute callback
                        if (sending && (sentDataCallback != null)) {
                            sentDataCallback.onSentData(ipAddress);
                        }
                    }

                    Thread.sleep(10);
                }

            } catch(Exception e) {
                e.printStackTrace();
                sending = false;
            }
        }


    }

    /**
     * thread for receiving data from connected socket
     */
    class ReceiveDataThread extends Thread {
        public boolean receiving;
        private String ipAddress;
        private Socket socket;
        private WifiEventListener receivedDataCallback;

        /**
         * constructor
         */
        public ReceiveDataThread(String address, Socket socket, WifiEventListener callback) {
            this.ipAddress = address;
            this.socket = socket;
            this.receivedDataCallback = callback;
        }

        /**
         *
         * @param socket
         */
        public void setSocket(Socket socket) {
            this.socket = socket;
        }

        /**
         *
         * @param callback
         */
        public void setReceivedDataCallback(WifiEventListener callback) {
            receivedDataCallback = callback;
        }

        /**
         *
         */
        @Override
        public void run() {
            receiving = true;

            try {
                InputStream inputStream = socket.getInputStream();

                byte[] byDataHeader = new byte[20];
                byte[] byMd5 = new byte[32];
                byte[] byDatasize = new byte[4];
                byte[] byData = new byte[DATASIZE_LIMIT];

                String md5 = "";
                int datasize = 0;

                int offset = 0;
                int topOfMd5 = byDataHeader.length;
                int topOfDatasize = byDataHeader.length + byMd5.length;
                int topOfData = byDataHeader.length + byMd5.length + byDatasize.length;

                while(receiving) {

                    // data header (20bytes)
                    if (offset < topOfMd5) {
                        // read data header
                        int bytes = inputStream.read(byDataHeader, offset, byDataHeader.length - offset);
                        if (bytes > 0) {
                            // check data header per byte
                            for (int i=0; i<bytes; i++) {
                                if (byDataHeader[i] == DATA_HEADER[offset]) {
                                    offset++;
                                } else {
                                    offset = 0;
                                }
                            }
                        }
                    }
                    // md5 (32bytes)
                    else if (offset < topOfDatasize) {
                        // read md5
                        int bytes = inputStream.read(byMd5, offset - topOfMd5, byMd5.length - (offset - topOfMd5));
                        if (bytes > 0) {
                            offset += bytes;
                            if (offset == topOfMd5 + byMd5.length) {
                                md5 = new String(byMd5, "UTF-8");
                            }
                        }
                    }
                    // data size (4bytes)
                    else if (offset < topOfData) {
                        // read data size
                        int bytes = inputStream.read(byDatasize, offset - topOfDatasize, byDatasize.length - (offset - topOfDatasize));
                        if (bytes > 0) {
                            offset += bytes;
                            if (offset == topOfDatasize + byDatasize.length) {
                                ByteBuffer intBuf = ByteBuffer.wrap(byDatasize);
                                intBuf.order(ByteOrder.LITTLE_ENDIAN);
                                datasize = intBuf.getInt();
                                if ((datasize <= 0) || (datasize > DATASIZE_LIMIT)) {
                                    byData = new byte[PACKET_SIZE];
                                } else {
                                    byData = new byte[datasize];
                                }
                            }
                        }
                    }
                    // data
                    else {
                        // read data
                        int bytes;
                        if (DATASIZE_LIMIT < datasize) {
                            bytes = inputStream.read(byData, 0, byData.length);
                        } else {
                            bytes = inputStream.read(byData, offset - topOfData, datasize - (offset - topOfData));
                        }

                        if (bytes > 0) {
                            offset += bytes;
                            if (offset - topOfData == datasize) {
                                if (DATASIZE_LIMIT > datasize) {
                                    String actualMd5 = getMD5(byData);
                                    if (md5.equals(actualMd5)) {

                                        String jsonText = new String(byData, "UTF-8");

                                        try {

                                            JSONObject jsonObject = new JSONObject(jsonText);

                                            if (receivedDataCallback != null) {
                                                receivedDataCallback.onReceivedData(ipAddress, jsonObject);
                                            }

                                        } catch (JSONException jsonExcption) {
                                            jsonExcption.printStackTrace();
                                            offset = 0;
                                        }

                                    } else {
                                        Logger.addLog("completed to receive data : md5 mismatch");
                                    }
                                } else {
                                    Logger.addLog("received data size is over limit");
                                }
                                offset = 0;
                            }
                        }
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                receiving = false;
            }
        }
    }
}
