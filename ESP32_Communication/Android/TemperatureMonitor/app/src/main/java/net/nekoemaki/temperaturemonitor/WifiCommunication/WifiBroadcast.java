package net.nekoemaki.temperaturemonitor.WifiCommunication;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import net.nekoemaki.temperaturemonitor.Common.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.DatagramChannel;
import java.security.MessageDigest;
import java.util.ArrayList;

import static android.content.Context.WIFI_SERVICE;


/**
 * udp broadcast send and receive data
 */
public class WifiBroadcast {
    private static final byte[] DATA_HEADER = "COMMUNICATION_HEADER".getBytes();
    private static final int LimitBroadcastSize = 10 * 1024;

    private static String broadcastAddress = "255.255.255.255";
    private static int broadcastPort;
    private static String selfIpAddress;

    private static ArrayList<JSONObject> sendDataQue;

    private static ReceiveDataThread receiveDataThread;
    private static SendDataThread sendDataThread;

    private static DatagramChannel receiveDataCh;
    private static DatagramChannel sendDataCh;

    public static WifiEventListener eventListener;

    public static String getSelfIpAddress() {
        return selfIpAddress;
    }

    /**
     * start send and receive broadcast
     * @param context
     * @param port
     * @param event
     */
    public static void start(Context context, int port, WifiEventListener event) {

        if ((receiveDataCh == null) && (sendDataCh == null))  {

            // get self ip address
            WifiManager manager = (WifiManager) context.getSystemService(WIFI_SERVICE);
            WifiInfo info = manager.getConnectionInfo();
            int ipAddr = info.getIpAddress();
            ByteBuffer ipBuf = ByteBuffer.allocate(4);
            ipBuf.order(ByteOrder.LITTLE_ENDIAN);
            byte[] addrBytes = ipBuf.putInt(ipAddr).array();
            selfIpAddress = String.format("%d.%d.%d.%d", addrBytes[0]&0xFF, addrBytes[1]&0xFF, addrBytes[2]&0xFF, addrBytes[3]&0xFF);
            broadcastAddress = String.format("%d.%d.%d.%d", addrBytes[0]&0xFF, addrBytes[1]&0xFF, addrBytes[2]&0xFF, 0xFF);

            broadcastPort = port;
            eventListener = event;

            sendDataQue = new ArrayList<>();

            // start receive data thread
            receiveDataThread = new ReceiveDataThread(port);
            receiveDataThread.start();

            // start send data thread
            sendDataThread = new SendDataThread(port);
            sendDataThread.start();
        }
    }

    /**
     * stop send and receive broadcast
     */
    public static void stop() {
        if (receiveDataCh != null) {
            if (receiveDataCh.isOpen()) {
                try {
                    receiveDataCh.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            receiveDataCh = null;
        }

        if (sendDataCh != null) {
            if (sendDataCh.isConnected()) {
                try {
                    sendDataCh.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            sendDataCh = null;
        }

        if (receiveDataThread != null) {
            receiveDataThread.dispose();
        }

        if (sendDataThread != null) {
            sendDataThread.dispose();
        }

        if (sendDataQue != null) {
            sendDataQue.clear();
        }
    }

    /**
     * get broadcast server is active
     * @return
     */
    public static boolean isActive() {
        return (receiveDataCh != null);
    }

    /**
     * get md5 from byte array
     * @param data
     * @return
     */
    private static String getMD5(byte[] data) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(data);
            byte[] md5Bytes = md.digest();
            String strMd5 = "";
            for (int i=0; i<md5Bytes.length; i++){
                strMd5 += String.format("%02x", md5Bytes[i]);
            }
            return strMd5;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * send broadcast
     * @param data
     */
    public static void sendData(JSONObject data) {
        if (sendDataCh != null) {
            sendDataQue.add(data);
        }
    }


    /**
     * receive data thread
     */
    static class ReceiveDataThread extends Thread {

        private int port;
        private boolean disposeFlg;

        /**
         * constructor
         * @param port
         */
        public ReceiveDataThread(int port) {
            this.port = port;
            this.disposeFlg = false;
        }

        /**
         * dispose thread
         */
        public void dispose() {
            disposeFlg = true;
        }

        /**
         * received data is composed of data header, md5, data size and main data
         */
        @Override
        public void run() {
            receiveDataCh = null;

            try {
                receiveDataCh = DatagramChannel.open();

                receiveDataCh.socket().bind(new InetSocketAddress(port));

                ByteBuffer dataBuff = ByteBuffer.allocate(LimitBroadcastSize);

                int dataHeaderLength = DATA_HEADER.length;
                int md5Length = 32;
                int datasizeLength = 4;

                byte[] bMd5 = new byte[md5Length];
                byte[] bDatasize = new byte[datasizeLength];
                byte[] bJson = null;

                int offset = 0;
                int datasize = 0;

                while (!disposeFlg) {
                    dataBuff.clear();
                    receiveDataCh.receive(dataBuff);
                    dataBuff.flip();
                    byte[] buf = new byte[dataBuff.limit()];
                    dataBuff.get(buf, 0, dataBuff.limit());
                    int idx = 0;

                    // check data header
                    while ((offset < dataHeaderLength) && (idx < buf.length)) {
                        if (buf[idx] == DATA_HEADER[offset]) {
                            offset++;
                        } else {
                            offset = 0;
                        }
                        idx++;
                        if (offset == dataHeaderLength) {
                            break;
                        }
                    }

                    // md5
                    if ((offset >= dataHeaderLength) && (offset < dataHeaderLength + md5Length)) {
                        if (dataHeaderLength + md5Length - offset < buf.length - idx) {
                            System.arraycopy(buf, idx, bMd5, offset - dataHeaderLength, dataHeaderLength + md5Length - offset);
                            idx += dataHeaderLength + md5Length - offset;
                            offset += dataHeaderLength + md5Length - offset;
                        } else {
                            System.arraycopy(buf, idx, bMd5, offset - dataHeaderLength, buf.length - idx);
                            offset += buf.length - idx;
                            idx += buf.length - idx;
                        }
                    }

                    // datasize
                    if ((offset >= dataHeaderLength + md5Length) && (offset < dataHeaderLength + md5Length + datasizeLength)) {
                        if (dataHeaderLength + md5Length + datasizeLength - offset < buf.length - idx) {
                            System.arraycopy(buf, idx, bDatasize, offset - dataHeaderLength - md5Length, dataHeaderLength + md5Length + datasizeLength - offset);
                            idx += dataHeaderLength + md5Length + datasizeLength - offset;
                            offset += dataHeaderLength + md5Length + datasizeLength - offset;
                        } else {
                            System.arraycopy(buf, idx, bDatasize, offset - dataHeaderLength - md5Length, buf.length - idx);
                            offset += buf.length - idx;
                            idx += buf.length - idx;
                        }
                        if (offset == dataHeaderLength + md5Length + datasizeLength) {
                            ByteBuffer intBuf = ByteBuffer.wrap(bDatasize);
                            intBuf.order(ByteOrder.LITTLE_ENDIAN);
                            byte[] ar = intBuf.array();
                            datasize = intBuf.getInt();
                            bJson = new byte[datasize];
                        }
                    }

                    // data
                    if (offset >= dataHeaderLength + md5Length + datasizeLength) {

                        if (dataHeaderLength + md5Length + datasizeLength + datasize - offset < buf.length - idx) {
                            System.arraycopy(buf, idx, bJson, offset - dataHeaderLength - md5Length - datasizeLength, dataHeaderLength + md5Length + datasizeLength + datasize - offset);
                            idx += dataHeaderLength + md5Length + datasizeLength + datasize - offset;
                            offset += dataHeaderLength + md5Length + datasizeLength + datasize - offset;
                        } else {
                            System.arraycopy(buf, idx, bJson, offset - dataHeaderLength - md5Length - datasizeLength, buf.length - idx);
                            offset += buf.length - idx;
                            idx += buf.length - idx;
                        }
                        String tmpStr = new String(bJson);
                        Logger.addLog(tmpStr);

                        if (offset == dataHeaderLength + md5Length + datasizeLength + datasize) {

                            // check md5
                            String md5Str = getMD5(bJson);
                            String recMd5 = new String(bMd5);
                            if (md5Str.equals(recMd5)) {

                                String jsonText = new String(bJson);
                                Logger.addLog(jsonText);


                                try {

                                    JSONObject jsonObject = new JSONObject(jsonText);
                                    String ipAddress = jsonObject.getString("ipaddress");
                                    Logger.addLog(jsonText);

                                    if (!ipAddress.equals(selfIpAddress)) {

                                        if (eventListener != null) {
                                            eventListener.onReceivedData(ipAddress, jsonObject);
                                        }
                                    }

                                } catch (JSONException jsonExcption) {
                                    jsonExcption.printStackTrace();
                                    offset = 0;
                                }
                            }

                            offset = 0;
                        }
                    }

                }

            } catch(Exception e) {
                e.printStackTrace();
            } finally {

                if (receiveDataCh != null) {
                    try {
                        receiveDataCh.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }

    /**
     * send data thread
     */
    static class SendDataThread extends Thread {

        private int port;
        private boolean disposeFlg;

        /**
         * constructor
         * @param port
         */
        public SendDataThread(int port) {
            this.port = port;
            this.disposeFlg = false;
        }

        /**
         * dispose thread
         */
        public void dispose() {
            disposeFlg = true;
        }

        /**
         * send data is composed of data header, md5, data size and main data
         */
        @Override
        public void run() {
            sendDataCh = null;

            try {
                sendDataCh = DatagramChannel.open();
                sendDataCh.socket().setBroadcast(true);

                InetSocketAddress sendToAddress;

                while (!disposeFlg) {
                    if (sendDataQue.size() > 0) {
                        JSONObject sendDat = sendDataQue.get(0);
                        sendDataQue.remove(0);

                        if (sendDat == null) {
                            continue;
                        }

                        String jsonText = sendDat.toString();
                        byte[] jsonBytes = jsonText.getBytes();
                        // data to json <<<<<<<<<<<<<<<<<<<<<<<<<<<<

                        byte[] buffData = new byte[20 + 32 + 4 + jsonBytes.length];
                        int offset = 0;

                        // data header (20bytes)
                        System.arraycopy(DATA_HEADER, 0, buffData, offset, DATA_HEADER.length);
                        offset += DATA_HEADER.length;

                        // md5 (32bytes)
                        String strMd5 = getMD5(jsonText.getBytes());
                        byte[] bMd5 = strMd5.getBytes();
                        System.arraycopy(bMd5, 0, buffData, offset, bMd5.length);
                        offset += bMd5.length;

                        // data size (4bytes)
                        ByteBuffer intBuf = ByteBuffer.allocate(4);
                        intBuf.order(ByteOrder.LITTLE_ENDIAN);
                        intBuf.putInt(jsonText.length());
                        byte[] bDatasize = intBuf.array();
                        System.arraycopy(bDatasize, 0, buffData, offset, bDatasize.length);
                        offset += bDatasize.length;

                        // data body
                        System.arraycopy(jsonBytes, 0, buffData, offset, jsonBytes.length);

                        ByteBuffer sendBuf = ByteBuffer.wrap(buffData);

                        // send data
                        sendToAddress = new InetSocketAddress(broadcastAddress, port);
                        sendDataCh.send(sendBuf, sendToAddress);
                    }

                    Thread.sleep(100);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                if (sendDataCh != null) {
                    try {
                        sendDataCh.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

        }
    }

}
