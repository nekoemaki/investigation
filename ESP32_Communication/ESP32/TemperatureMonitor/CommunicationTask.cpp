#include <Arduino.h>

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#include <WiFi.h>
#include <WiFiUdp.h>
#include <WiFiServer.h>
#include <WiFiClient.h>

#include <MD5Builder.h>

#include "CommunicationTask.h"



const int _maxPacketSize = 2 * 1024 + 56;


/**
 * send data queue element
 */
class SendDataElement {
public:
    String address;
    uint8_t* data;
    size_t length;
};

/**
 * received data element
 */
class ReceivedDataElement {
public:
    
    ReceivedDataElement();
    
    String receivingAddress;
    unsigned long receiveStartTime;
    int receiveTimeout;
    
    uint8_t bMd5[33];
    uint8_t bDataSize[4];
    
    uint32_t datasize;
    int offset;
    
    uint8_t* jsonData;
};

/**
 * received data element constructor
 */
ReceivedDataElement::ReceivedDataElement() {
    
    receiveStartTime = 0;
    receiveTimeout = 30000;
    bMd5[32] = '\0';
    datasize = 0;
    offset = 0;
    
}



void createSendDataBuffer(uint8_t* data, size_t length);
void checkReceivedData(String remoteAddress, uint8_t* data, size_t length, ReceivedDataElement* recBuf, ReceivedDataFunction callback);
char* getDataMd5(uint8_t *dat, int length);
void byteArrayCopy(uint8_t* src, uint32_t srcOffset, uint8_t* dest, uint32_t destOffset, uint32_t length);




// common variant -----------------------------------

bool _task_enable = false;
bool _task_executing = false;
TaskHandle_t _taskHandler;
const int _sendDataQueueMax = 5;


uint8_t _sendDataBuf[_maxPacketSize];
char _dataHeaderStr[] = "COMMUNICATION_HEADER";
int _dataHeaderLength = 20;
int _md5Length = 32;
int _datasizeLength = 4;
int _headerSize = _dataHeaderLength + _md5Length + _datasizeLength;
StaticJsonDocument<_maxPacketSize> jsonDoc;


// broadcast -----------------------------------
bool _enableBroadcast;
WiFiUDP _broadcast;
IPAddress _broadcastIp;
int _broadcastPort = 9000;
bool _broadcastActive = false;
int _broadcastBufLength = _maxPacketSize;
char _broadcastBuf[_maxPacketSize];
int _broadcastSendDataQueueCount = 0;
SendDataElement _broadcastSendDataQueue[_sendDataQueueMax];
ReceivedDataElement _broadcastReceivedData;

ReceivedDataFunction _broadcastReceivedDataCallback;


// tcp -----------------------------------
bool _enableTcp;
WiFiServer _tcpServer;
const int _tcpClientMax = 3;
bool _tcpClientEbabled[_tcpClientMax];
WiFiClient _tcpClients[_tcpClientMax];
int _tcpPort = 9001;
int _tcpBufLength = _maxPacketSize;
char _tcpBuf[_maxPacketSize];
int _tcpSendDataQueueCount = 0;
SendDataElement _tcpSendDataQueue[_sendDataQueueMax];
ReceivedDataElement _tcpReceivedData[_tcpClientMax];

ConnectedClientFunction _tcpConnectedCallback;
DisconnectedClientFunction _tcpDisconnectedCallback;
ReceivedDataFunction _tcpReceivedDataCallback;


// bluetooth -----------------------------------
bool _enableBle;
std::string _bleName = "ESP32";
int _bleMtu = 20;
BLEServer* _bleServer;
BLEService* _bleService;
BLECharacteristic* _bleCharacteristic;
bool _bleConnected = false;
bool _beforeBleConnected = false;
int _bleSendDataQueueCount = 0;
SendDataElement _bleSendDataQueue[_sendDataQueueMax];
#define BLE_SERVICE_UUID        "7c73ee15-c58f-44ef-b9a7-3e128e651012"
#define BEL_CHARACTERISTIC_UUID "fd9b8ae1-55aa-4c31-89f8-6f2dfbb0c046"

ConnectedClientFunction _bleConnectedCallback;
DisconnectedClientFunction _bleDisconnectedCallback;
ReceivedDataFunction _bleReceivedDataCallback;

class BleCallback: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
        Serial.println("BLE connected");
        _bleConnected = true;
        if (_bleConnectedCallback != NULL) {
            _bleConnectedCallback("");
        }
    }
    void onDisconnect(BLEServer* pServer) {
        Serial.println("BLE disconnected");
        _bleConnected = false;
        if (_bleDisconnectedCallback != NULL) {
            _bleDisconnectedCallback("");
        }
    }
};
BleCallback* _bleCallback;

ReceivedDataElement _receivedBleData;
class BleChrCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
        std::string recData = pCharacteristic->getValue();
        if (recData.length() > 0) {
            Serial.println("ble received");
            const char* recChar = recData.c_str();
            checkReceivedData("", (uint8_t*)recChar, recData.length(), &_receivedBleData, _bleReceivedDataCallback);
        }
    }
};
BleChrCallbacks* _bleChrCallback;



/**
 * start broadcast server
 */
void _start_broadcast_server() {
    if (!_broadcastActive) {
        Serial.println("start broadcast server");
        IPAddress localIp = WiFi.localIP();
        char broadcastIpStr[16];
        sprintf(broadcastIpStr, "%d.%d.%d.%d", localIp[0], localIp[1], localIp[2], (uint8_t)255);
        _broadcastIp.fromString(broadcastIpStr);
        _broadcast.begin(_broadcastPort);
        _broadcastActive = true;
    }
}

/**
 * stop broadcast server
 */
void _stop_broadcast_server() {
    if (_broadcastActive) {
        _broadcast.stop();
        _broadcastActive = false;
    }
}

/**
 * start tcp server
 */
void _start_tcp_server() {
    if (!_tcpServer) {
        Serial.println("start tcp server");
        int i;
        for (i=0; i<_tcpClientMax; i++) {
            _tcpClientEbabled[i] = false;
        }
        _tcpServer.begin(_tcpPort);
    }
}

/**
 * stop tcp server
 */
void _stop_tcp_server() {
    if (_tcpServer) {
        _tcpServer.stop();
        int i;
        for (i=0; i<_tcpClientMax; i++) {
            _tcpClientEbabled[i] = false;
            if (_tcpClients[i].connected() == true) {
                _tcpClients[i].stop();
            }
        }
    }
}

/**
 * start ble server
 */
void _start_ble_server() {
    if (_bleServer == NULL) {
        Serial.println("start ble server");
        BLEDevice::init(_bleName);
        _bleServer = BLEDevice::createServer();
        
        _bleCallback = new BleCallback();
        _bleServer->setCallbacks(_bleCallback);
        
        _bleService = _bleServer->createService(BLE_SERVICE_UUID);
        
        _bleCharacteristic = _bleService->createCharacteristic(
                            BEL_CHARACTERISTIC_UUID,
                            BLECharacteristic::PROPERTY_READ   |
                            BLECharacteristic::PROPERTY_WRITE  |
                            BLECharacteristic::PROPERTY_NOTIFY |
                            BLECharacteristic::PROPERTY_INDICATE
                          );
        _bleCharacteristic->addDescriptor(new BLE2902());
        
        _receivedBleData.receiveTimeout = 2800;
        _bleChrCallback = new BleChrCallbacks();
        _bleCharacteristic->setCallbacks(_bleChrCallback);
        
        _bleService->start();
        
        BLEAdvertising *bleAdvertising = _bleServer->getAdvertising();
        bleAdvertising->addServiceUUID(BLE_SERVICE_UUID);
        bleAdvertising->setScanResponse(false);
        bleAdvertising->setMinPreferred(0x0);
        bleAdvertising->start();
    }
}

/**
 * stop ble server
 */
void _stop_ble_server() {
    if (_bleServer != NULL) {
        _bleService->stop();
        _bleServer->removeService(_bleService);
        delete _bleCallback;
        delete _bleChrCallback;
        delete _bleServer;
        _bleServer = NULL;
    }
}



/**
 * start communication task
 */
void _start_communication_task() {
    
    _task_executing = true;
    
    // start broadcast server
    if (_enableBroadcast) {
        _start_broadcast_server();
    }
    
    
    // start tcp server
    if (_enableTcp) {
        _start_tcp_server();
    }
    
    
    // start bluetooth server
    if (_enableBle) {
        _start_ble_server();
    }
    
}

/**
 * communication task
 */
//void _communication_loop(void* arg){
void communicationLoop(){
    
    if (_task_enable) {
        // broadcast >>>-------------------------------------------------------
        if (_enableBroadcast) {
            if (WiFi.status() == WL_CONNECTED) {
                
                // receive broadcast
                int broadcastPacketSize = _broadcast.parsePacket();
                if (broadcastPacketSize > 0) {
                    int broadcastDataSize = _broadcast.read(_broadcastBuf, _broadcastBufLength);
                    if (broadcastDataSize > 0) {
                        checkReceivedData(_broadcast.remoteIP().toString(), (uint8_t*)_broadcastBuf, broadcastDataSize, &_broadcastReceivedData, _broadcastReceivedDataCallback);
                    }
                }
                
                // send broadcast
                while (_broadcastSendDataQueueCount > 0) {
                    Serial.println("broadcast send");
                    
                    createSendDataBuffer(_broadcastSendDataQueue[_broadcastSendDataQueueCount-1].data, _broadcastSendDataQueue[_broadcastSendDataQueueCount-1].length);
                    
                    size_t sendLength = _broadcastSendDataQueue[_broadcastSendDataQueueCount-1].length + _dataHeaderLength + _md5Length + _datasizeLength;
                    
                    _broadcast.beginPacket(_broadcastIp, _broadcastPort);
                    _broadcast.write((uint8_t*)_sendDataBuf, sendLength);
                    _broadcast.endPacket();
                    
                    free(_broadcastSendDataQueue[_broadcastSendDataQueueCount-1].data);
                    
                    _broadcastSendDataQueueCount--;
                }
                
            } else {
                // stop broadcast server when disconnected wifi
                _stop_broadcast_server();
            }
        }
        
        
        // tcp >>>-------------------------------------------------------
        if (_enableTcp) {
            if (WiFi.status() == WL_CONNECTED) {
                
                // check client socket is connected
                int i;
                for (i=0; i<_tcpClientMax; i++) {
                    if (!_tcpClients[i].connected() && _tcpClientEbabled[i]) {
                        _tcpClientEbabled[i] = false;
                        Serial.print("disconnected tcp client : ");
                        Serial.print(_tcpClients[i].remoteIP().toString());
                        Serial.println();
                        
                        // call disconnected callback
                        if (_tcpDisconnectedCallback != NULL) {
                            _tcpDisconnectedCallback(_tcpClients[i].remoteIP().toString());
                        }
                        _tcpClients[i].stop();
                    }
                }
                
                // tcp socket server
                for (i=0; i<_tcpClientMax; i++) {
                    if (!_tcpClients[i].connected()) {
                        _tcpClients[i] = _tcpServer.available();
                        if (_tcpClients[i]) {
                            if (_tcpClients[i].connected()) {
                                _tcpClientEbabled[i] = true;
                                Serial.print("connected new tcp client : ");
                                Serial.print(_tcpClients[i].remoteIP().toString());
                                Serial.println();
                                
                                // call connected callback
                                if (_tcpConnectedCallback != NULL) {
                                    _tcpConnectedCallback(_tcpClients[i].remoteIP().toString());
                                }
                                
                            }
                        }
                        break;
                    }
                }
                
                // receive data from connected tcp socket
                for (i=0; i<_tcpClientMax; i++) {
                    if (_tcpClients[i].connected()) {
                       int tcpDataSize = _tcpClients[i].read((uint8_t*)_tcpBuf, _tcpBufLength);
                       if (tcpDataSize > 0) {
                           Serial.println("tcp received data");
                           checkReceivedData(_tcpClients[i].remoteIP().toString(), (uint8_t*)_tcpBuf, tcpDataSize, &_tcpReceivedData[i], _tcpReceivedDataCallback);
                       }
                    }
                }
                
                // send data to connected tcp socket
                while (_tcpSendDataQueueCount > 0) {
                    Serial.println("tcp send data");
                    
                    int cnt = 0;
                    if (_tcpSendDataQueue[_tcpSendDataQueueCount-1].address != "") {
                        // send to target client
                        for (i=0; i<_tcpClientMax; i++) {
                            if (_tcpClients[i].connected()) {
                                if (_tcpSendDataQueue[_tcpSendDataQueueCount-1].address == _tcpClients[i].remoteIP().toString()) {
                                    createSendDataBuffer(_tcpSendDataQueue[_tcpSendDataQueueCount-1].data, _tcpSendDataQueue[_tcpSendDataQueueCount-1].length);
                                    size_t sendLength = _tcpSendDataQueue[_tcpSendDataQueueCount-1].length + _dataHeaderLength + _md5Length + _datasizeLength;
                                    
                                    _tcpClients[i].write((uint8_t*)_sendDataBuf, sendLength);
                                    break;
                                }
                            }
                            cnt++;
                        }
                    }
                    else {
                        // send to all client
                        createSendDataBuffer(_tcpSendDataQueue[_tcpSendDataQueueCount-1].data, _tcpSendDataQueue[_tcpSendDataQueueCount-1].length);
                        size_t sendLength = _tcpSendDataQueue[_tcpSendDataQueueCount-1].length + _dataHeaderLength + _md5Length + _datasizeLength;
                        for (i=0; i<_tcpClientMax; i++) {
                            if (_tcpClients[i].connected()) {
                                _tcpClients[i].write((uint8_t*)_sendDataBuf, sendLength);
                                cnt++;
                            }
                        }
                    }
                    if (cnt == _tcpClientMax) {
                        Serial.print("cannot send tcp data : client is not connected. --- ");
                        Serial.print(_tcpSendDataQueue[_tcpSendDataQueueCount-1].address);
                        Serial.println();
                    }
                    
                    _tcpSendDataQueueCount--;
                }
                
            } else {
                // stop tcp server when disconnected wifi
                _stop_tcp_server();
            }
        }
        
        
        // bluetooth >>>-------------------------------------------------------
        if (_enableBle) {
            
            if (_bleConnected) {
                _bleMtu = BLEDevice::getMTU() - 3;
                
                // send bluetooth socket
                while (_bleSendDataQueueCount > 0) {
                    if (_bleMtu < 20) {
                        break;
                    }
                    Serial.println("ble send data");
                    
                    // send data header
                    _bleCharacteristic->setValue((uint8_t*)_dataHeaderStr, _dataHeaderLength);
                    _bleCharacteristic->notify();
                    delay(10);
                    
                    // send md5
                    char* bMd5 = getDataMd5(_bleSendDataQueue[_bleSendDataQueueCount-1].data, (int)_bleSendDataQueue[_bleSendDataQueueCount-1].length);
                    int remainSize = _md5Length;
                    int sendDataOffset = 0;
                    while (remainSize > 0) {
                        int sendSize = _bleMtu;
                        if (remainSize < _bleMtu) {
                            sendSize = remainSize;
                        }
                        _bleCharacteristic->setValue((uint8_t*)&bMd5[sendDataOffset], sendSize);
                        _bleCharacteristic->notify();
                        delay(10);
                        remainSize -= sendSize;
                        sendDataOffset += sendSize;
                    }
                    
                    // send datasize
                    uint32_t intDataSize = (uint32_t)_bleSendDataQueue[_bleSendDataQueueCount-1].length;
                    _bleCharacteristic->setValue((uint8_t*)&intDataSize, 4);
                    _bleCharacteristic->notify();
                    delay(10);
                    
                    // send main data
                    remainSize = _bleSendDataQueue[_bleSendDataQueueCount-1].length;
                    sendDataOffset = 0;
                    while (remainSize > 0) {
                        int sendSize = _bleMtu;
                        if (remainSize < _bleMtu) {
                            sendSize = remainSize;
                        }
                        _bleCharacteristic->setValue((uint8_t*)&_bleSendDataQueue[_bleSendDataQueueCount-1].data[sendDataOffset], sendSize);
                        _bleCharacteristic->notify();
                        delay(10);
                        remainSize -= sendSize;
                        sendDataOffset += sendSize;
                    }
                    _bleSendDataQueueCount--;
                }
                
            }
            if (!_bleConnected && _beforeBleConnected) {
                // start advertising when ble disconnected
                _bleServer->startAdvertising();
                _beforeBleConnected = _bleConnected;
            }
            if (_bleConnected && !_beforeBleConnected) {
                // reconnected
                _beforeBleConnected = _bleConnected;
            }
        }
        
        
        // stop task when communication is not activating
        if (!isActiveBroadcast() && !isActiveTcp() && !isActiveBle()) {
            stopCommunicationTask();
        }
        
    }
}


/**
 * stop communication task
 */
void _stop_communication_task() {
    // stop broadcast server
    if (_enableBroadcast) {
        _stop_broadcast_server();
    }
    
    
    // stop tcp server and disconnect client
    if (_enableTcp) {
        _stop_tcp_server();
    }
    
    
    // stop bluetooth server and disconnect client
    if (_enableBle) {
        _stop_ble_server();
    }
    
    
    _task_executing = false;
    
}


/**
 * create send data
 */
void createSendDataBuffer(uint8_t* data, size_t length) {
    
    uint8_t* sendBuf = _sendDataBuf;
    
    // data header
    byteArrayCopy((uint8_t*)_dataHeaderStr, 0, sendBuf, 0, _dataHeaderLength);
    
    // md5
    char* bMd5 = getDataMd5(data, (int)length);
    byteArrayCopy((uint8_t*)bMd5, 0, sendBuf, _dataHeaderLength, _md5Length);
    
    // data size
    uint32_t intDataSize = (uint32_t)length;
    byteArrayCopy((uint8_t*)&intDataSize, 0, sendBuf, _dataHeaderLength + _md5Length, _datasizeLength);
    
    // main data
    byteArrayCopy(data, 0, sendBuf, _dataHeaderLength + _md5Length + _datasizeLength, (uint32_t)length);
    
}


/**
 * check received data
 */
void checkReceivedData(String remoteAddress, uint8_t* data, size_t length, ReceivedDataElement* recBuf, ReceivedDataFunction callback) {
    
    int idx = 0;
    
    // if receiving data timeout, reset received data
    if ((recBuf->receiveStartTime != 0) && (millis() - recBuf->receiveStartTime > recBuf->receiveTimeout)) {
        recBuf->offset = 0;
        if (recBuf->jsonData != NULL) {
            free(recBuf->jsonData);
            recBuf->jsonData = NULL;
        }
    }
    
    
    // check data header
    while ((idx < length) && (recBuf->offset < _dataHeaderLength)) {
        if (_dataHeaderStr[recBuf->offset] == data[idx]) {
            recBuf->offset++;
        } else {
            recBuf->offset = 0;
        }
        idx++;
        if (recBuf->offset == _dataHeaderLength) {
            recBuf->receiveStartTime = millis();
        }
    }
    
    // md5
    if ((recBuf->offset >= _dataHeaderLength) && (recBuf->offset < _dataHeaderLength + _md5Length)) {
        if ((_dataHeaderLength + _md5Length - recBuf->offset) < length - idx) {
            byteArrayCopy(data, idx, recBuf->bMd5, (recBuf->offset - _dataHeaderLength), (_dataHeaderLength + _md5Length - recBuf->offset));
            idx += _dataHeaderLength + _md5Length - recBuf->offset;
            recBuf->offset += _dataHeaderLength + _md5Length - recBuf->offset;
        } else {
            byteArrayCopy(data, idx, recBuf->bMd5, (recBuf->offset - _dataHeaderLength), (length - idx));
            recBuf->offset += length - idx;
            idx += length - idx;
        }
    }
    
    // data size
    if ((recBuf->offset >= _dataHeaderLength + _md5Length) && (recBuf->offset < _dataHeaderLength + _md5Length + _datasizeLength)) {
        if ((_dataHeaderLength + _md5Length + _datasizeLength - recBuf->offset) < length - idx) {
            byteArrayCopy(data, idx, (uint8_t*)&recBuf->bDataSize, (recBuf->offset - _dataHeaderLength - _md5Length), (_dataHeaderLength + _md5Length + _datasizeLength - recBuf->offset));
            idx += _dataHeaderLength + _md5Length + _datasizeLength - recBuf->offset;
            recBuf->offset += _dataHeaderLength + _md5Length + _datasizeLength - recBuf->offset;
        } else {
            byteArrayCopy(data, idx, (uint8_t*)&recBuf->bDataSize, (recBuf->offset - _dataHeaderLength + _md5Length), (length - idx));
            recBuf->offset += length - idx;
            idx += length - idx;
        }
        if (recBuf->offset == _dataHeaderLength + _md5Length + _datasizeLength) {
            // datasize byte array to int
            memcpy(&recBuf->datasize, recBuf->bDataSize, sizeof(recBuf->datasize));
            recBuf->jsonData = (uint8_t*)malloc(recBuf->datasize);
        }
    }
    
    // data
    if (recBuf->offset >= _dataHeaderLength + _md5Length + _datasizeLength) {
        
        if ((_dataHeaderLength + _md5Length + _datasizeLength + recBuf->datasize - recBuf->offset) < length - idx) {
            byteArrayCopy(data, idx, recBuf->jsonData, (recBuf->offset - _dataHeaderLength - _md5Length - _datasizeLength), (_dataHeaderLength + _md5Length + _datasizeLength + recBuf->datasize - recBuf->offset));
            recBuf->offset += _dataHeaderLength + _md5Length + _datasizeLength + recBuf->datasize - recBuf->offset;
        } else {
            byteArrayCopy(data, idx, recBuf->jsonData, (recBuf->offset - _dataHeaderLength - _md5Length - _datasizeLength), (length - idx));
            recBuf->offset += length - idx;
        }
        
        if (recBuf->offset >= _dataHeaderLength + _md5Length + _datasizeLength + recBuf->datasize) {
            
            // check md5
            char* datMd5 = getDataMd5(recBuf->jsonData, recBuf->datasize);
            if (strcmp((char*)recBuf->bMd5, datMd5) == 0) {
                
                // decode json
                jsonDoc.clear();
                auto deserializeError = deserializeJson(jsonDoc, (char*)recBuf->jsonData);
                
                if (!deserializeError) {
                    
                    // callback
                    if (callback != NULL) {
                        callback(remoteAddress, jsonDoc);
                    }
                }
                
            }
            
            free(recBuf->jsonData);
            recBuf->jsonData = NULL;
            recBuf->receiveStartTime = 0;
            recBuf->offset = 0;
            delete datMd5;
            
        }
    }
    
}

/**
 * get md5
 */
char* getDataMd5(uint8_t *dat, int length) {
    MD5Builder mdb;
    mdb.begin();
    mdb.add(dat, length);
    mdb.calculate();
    char* md5Chars = new char[33];
    md5Chars[32] = '\0';
    mdb.getChars(md5Chars);
    return md5Chars;
}


/**
 * copy byte array
 */
void byteArrayCopy(uint8_t* src, uint32_t srcOffset, uint8_t* dest, uint32_t destOffset, uint32_t length) {
    uint32_t srcIdx = srcOffset;
    uint32_t destIdx = destOffset;
    while (srcIdx < (srcOffset + length)) {
        dest[destIdx] = src[srcIdx];
        srcIdx++;
        destIdx++;
    }
}


//////////////////////////////////////////////////////////////////////////////////

/**
 * start communication task
 */
void startCommunicationTask(bool enableBroadcast, bool enableTcp, bool enableBle) {
    if ((_task_executing == false) && (_task_enable == false)) {
        _task_enable = true;
        
        _enableBroadcast = enableBroadcast;
        _enableTcp = enableTcp;
        _enableBle = enableBle;
        
        _broadcastSendDataQueueCount = 0;
        _tcpSendDataQueueCount = 0;
        
        Serial.println("start communication task");
        _start_communication_task();
        
        // create new task
        //  task method ... _communication_task
        //  task name ... communication_task
        //  task buffer size ... 10KB
        //  task parameters ... NULL
        //  task priority ... 2
        //  task handler ... _taskHandler
        //xTaskCreate(_communication_loop, "communication_task", 10*1024, NULL, 2, &_taskHandler);
    }
}


/**
 * stop communication task
 */
void stopCommunicationTask() {
    
    _stop_communication_task();
    _task_enable = false;
    
}


/**
 * switch server state while task is active
 */
void switchCommunicationEnabled(bool enableBroadcast, bool enableTcp, bool enableBle) {
    if (_task_enable) {
        
        // switch broadcast server >>-------------------
        if (enableBroadcast) {
            // start broadcast server
            if (!_enableBroadcast) {
                _start_broadcast_server();
                _enableBroadcast = enableBroadcast;
            }
        } else {
            // stop broadcast server
            if (_enableBroadcast) {
                _enableBroadcast = enableBroadcast;
                _stop_broadcast_server();
            }
        }
        
        // switch tcp server >>-------------------
        if (enableTcp) {
            // start tcp server
            if (!_enableTcp) {
                _start_tcp_server();
                _enableTcp = enableTcp;
            }
        } else {
            // stop tcp server
            if (_enableTcp) {
                _enableTcp = enableTcp;
                _stop_tcp_server();
            }
        }
        
        // switch ble server >>-------------------
        if (enableBle) {
            // start ble server
            if (!_enableBle) {
                _start_ble_server();
                _enableBle = enableBle;
            }
        } else {
            // stop ble server
            if (_enableBle) {
                _enableBle = enableBle;
                _stop_ble_server();
            }
        }
        
    }
}


/**
 * communication task is active
 */
bool isActiveCommunicationTask() {
    return _task_executing;
}


/**
 * broadcast is active
 */
bool isActiveBroadcast() {
    if (_task_enable) {
        return _broadcastActive;
    } else {
        return false;
    }
}


/**
 * tcp is active
 */
bool isActiveTcp() {
    if (_task_enable) {
        return (_tcpServer == true);
    } else {
        return false;
    }
}


/**
 * ble is active
 */
bool isActiveBle() {
    if (_task_enable) {
        return (_bleServer != NULL);
    } else {
        return false;
    }
}


/**
 * set ble name
 */
void setBLEName(char* name) {
    _bleName = (std::string)name;
}


/**
 * set broadcast callback
 */
void setBroadcastCallback(ReceivedDataFunction receivedDataCallback) {
    _broadcastReceivedDataCallback = receivedDataCallback;
}


/**
 * set tcp callback
 */
void setTcpCallback(ConnectedClientFunction connectedClientCallback, DisconnectedClientFunction disconnectedClientCallback, ReceivedDataFunction receivedDataCallback) {
    _tcpConnectedCallback = connectedClientCallback;
    _tcpDisconnectedCallback = disconnectedClientCallback;
    _tcpReceivedDataCallback = receivedDataCallback;
}


/**
 * set ble callback
 */
void setBleCallback(ConnectedClientFunction connectedClientCallback, DisconnectedClientFunction disconnectedClientCallback, ReceivedDataFunction receivedDataCallback) {
    _bleConnectedCallback = connectedClientCallback;
    _bleDisconnectedCallback = disconnectedClientCallback;
    _bleReceivedDataCallback = receivedDataCallback;
}


/**
 * connect tcp socket
 */
bool connectTcpClient(IPAddress address, int port) {
    bool ret = false;
    
    int i;
    for (i=0; i<_tcpClientMax; i++) {
        if (!_tcpClients[i].connected()) {
            if (_tcpClients[i].connect(address, port)) {
                _tcpClientEbabled[i] = true;
                
                // call connected callback
                if (_tcpConnectedCallback != NULL) {
                    _tcpConnectedCallback(_tcpClients[i].remoteIP().toString());
                }
                
                ret = true;
            }
        }
    }
    
    return ret;
}


/**
 * get connected tcp client count
 */
int getTcpClientCount() {
    int i;
    int cnt = 0;
    for (i=0; i<_tcpClientMax; i++) {
        if (_tcpClients[i].connected()) {
            cnt++;
        }
    }
    return cnt;
}


/**
 * send broadcast
 */
void sendBroadcast(uint8_t* data, size_t length) {
    if (length > _maxPacketSize - _headerSize) {
        return;
    }
    if ((_task_enable) && (_broadcastSendDataQueueCount < _sendDataQueueMax)) {
        _broadcastSendDataQueue[_broadcastSendDataQueueCount].data = (uint8_t*)malloc(length);
        memcpy(_broadcastSendDataQueue[_broadcastSendDataQueueCount].data, data, length);
        _broadcastSendDataQueue[_broadcastSendDataQueueCount].length = length;
        _broadcastSendDataQueueCount++;
    }
}


/**
 * send tcp
 */
void sendTcp(String address, uint8_t* data, size_t length) {
    if (length > _maxPacketSize - _headerSize) {
        return;
    }
    if ((_task_enable) && (_tcpSendDataQueueCount < _sendDataQueueMax)) {
        _tcpSendDataQueue[_tcpSendDataQueueCount].data = (uint8_t*)malloc(length);
        memcpy(_tcpSendDataQueue[_tcpSendDataQueueCount].data, data, length);
        _tcpSendDataQueue[_tcpSendDataQueueCount].address = address;
        _tcpSendDataQueue[_tcpSendDataQueueCount].length = length;
        _tcpSendDataQueueCount++;
    }
}


/**
 * send tcp
 */
void sendTcp(uint8_t* data, size_t length) {
    if (length > _maxPacketSize - _headerSize) {
        return;
    }
    if ((_task_enable) && (_tcpSendDataQueueCount < _sendDataQueueMax)) {
        _tcpSendDataQueue[_tcpSendDataQueueCount].data = (uint8_t*)malloc(length);
        memcpy(_tcpSendDataQueue[_tcpSendDataQueueCount].data, data, length);
        _tcpSendDataQueue[_tcpSendDataQueueCount].address = "";
        _tcpSendDataQueue[_tcpSendDataQueueCount].length = length;
        _tcpSendDataQueueCount++;
    }
}


/**
 * send ble
 */
void sendBle(uint8_t* data, size_t length) {
    if (length > _maxPacketSize) {
        return;
    }
    if ((_task_enable) && (_bleSendDataQueueCount < _sendDataQueueMax)) {
        _bleSendDataQueue[_bleSendDataQueueCount].data = (uint8_t*)malloc(length);
        memcpy(_bleSendDataQueue[_bleSendDataQueueCount].data, data, length);
        _bleSendDataQueue[_bleSendDataQueueCount].length = length;
        _bleSendDataQueueCount++;
    }
}
