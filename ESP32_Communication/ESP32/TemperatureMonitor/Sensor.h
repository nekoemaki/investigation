#ifndef SENSOR_H
#define SENSOR_H

#include <DHT.h>


class Sensor {
private:
    
    
public:
    
   void initialize();
   float getTemperature();
   float getHumidity();
   float getIlluminance();
    
};


#endif

