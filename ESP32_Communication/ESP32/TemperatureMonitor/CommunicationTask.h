#ifndef COMMUNICATION_TASK_H
#define COMMUNICATION_TASK_H

#include <functional>
#include <ArduinoJson.h>

typedef std::function<void(String address)> ConnectedClientFunction;
typedef std::function<void(String address)> DisconnectedClientFunction;
typedef std::function<void(String address, JsonDocument data)> ReceivedDataFunction;

void startCommunicationTask(bool enableBroadcast=true, bool enableTcp=true, bool enableBle=true);
void communicationLoop();
void stopCommunicationTask();
void switchCommunicationEnabled(bool enableBroadcast, bool enableTcp, bool enableBle);

bool isActiveCommunicationTask();
bool isActiveBroadcast();
bool isActiveTcp();
bool isActiveBle();

void setBLEName(char* name);

void setBroadcastCallback(ReceivedDataFunction receivedDataCallback);
void setTcpCallback(ConnectedClientFunction connectedClientCallback, DisconnectedClientFunction disconnectedClientCallback, ReceivedDataFunction receivedDataCallback);
void setBleCallback(ConnectedClientFunction connectedClientCallback, DisconnectedClientFunction disconnectedClientCallback, ReceivedDataFunction receivedDataCallback);

bool connectTcpClient(IPAddress address, int port);
int getTcpClientCount();

void sendBroadcast(uint8_t* data, size_t length);
void sendTcp(String address, uint8_t* data, size_t length);
void sendTcp(uint8_t* data, size_t length);
void sendBle(uint8_t* data, size_t length);


#endif
