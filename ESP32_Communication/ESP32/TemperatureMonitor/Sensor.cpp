
#include "Sensor.h"


DHT* dht;

int dhtPin = 13;
int illuminancePin = 39;

/**
 * initialize sensors
 */
void Sensor::initialize() {
    
    if (dht != NULL) {
        delete dht;
    }
    dht = new DHT(dhtPin, DHT11);
    dht->begin();
    
    pinMode(illuminancePin, INPUT);
    
    Serial.println("sensor initialized");
}


/**
 * get temperature value
 */
float Sensor::getTemperature() {
    float ret = dht->readTemperature();
    if (isnan(ret)){
        ret = -1000;
    }
    
    /*
    pinMode(dhtPin, INPUT_PULLUP);
    delayMicroseconds(80);
    int count = 0;
    while (digitalRead(dhtPin) == LOW) {
        if (count++ >= 1000) {
            Serial.println("DHT waiting signal LOW to HIGH timeout");
            break;
        }
    }
    count = 0;
    while (digitalRead(dhtPin) == HIGH) {
        if (count++ >= 1000) {
            Serial.println("DHT waiting signal HIGH to LOW timeout");
            break;
        }
    }
    */
    
    return ret;
}


/**
 * get humidity value
 */
float Sensor::getHumidity() {
    float ret = dht->readHumidity();
    if (isnan(ret)){
        ret = -1000;
    }
    return ret;
}


/**
 * get illuminance value
 */
float Sensor::getIlluminance() {
    float val = analogRead(illuminancePin);
    
    if (val > 0) {
        val = (float)100 - (val / (float)4095 * (float)100);
    }
    
    return val;
}
