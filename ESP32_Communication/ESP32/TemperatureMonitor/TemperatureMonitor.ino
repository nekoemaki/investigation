/**
 * 
 * get temperature, humidity and illuminance from esp32 to android by wifi or ble communication.
 * 
 * << pin settings >>
 *  pin 25 = DHT11 sensor
 *  pin 33 = illuminance sensor
 *  pin 17 = blue LED
 *  pin 16 = green LED
 * 
 * << operation >>
 *  1. receive wifi ssid and password by ble
 *  2. save wifi ssid and password to file
 *  3. connect to wifi
 *  4. receive udp broadcast data
 *  5. connect tcp socket to udp broadcast remote ip
 *  6. send temperature, humidity and illuminance by tcp socket per 3 seconds
 * 
 *  ** reboot esp32 per 24 hours.
 *  ** if cannot connect to wifi 5 times, wait receiving wifi ssid and password by ble.
 *  ** if wifi setting file is exists when device booted, connect to wifi by saved ssid.
 * 
 *  +++ attention +++
 *  pin 2,4,12,14,15,26,27 cannot use analogRead while ble or wifi is active
 *  pin 34,35,36,39 cannot use output (DHT11 cannot use these pins)
 *  pin 6,7,8,9,10,11 cannot use output and input
 */


#include <Arduino.h>

#include <WiFi.h>
#include <ArduinoJson.h>
#include <ESP.h>

#include "FS.h"
#include "SPIFFS.h"

#include "CommunicationTask.h"
#include "Sensor.h"

char wifi_ssid[30];
char wifi_pswd[30];

String wifiSettingPath = "/wifi.setting";

const int blueLedPin = 17;
const int greenLedPin = 16;

// wifi disconnected time
unsigned long failedToConnectWifiTime = 0;
// start connecting wifi time
unsigned long wifiStartConnectingTime = 0;
// wifi connect timeout milli seconds
unsigned long wifiConnectTimeout = 20000;
// wifi reconnect interval
unsigned long reconnectWaitMilliSec = 20000;

int retryWifiConnCount;

unsigned long beforeSendTime = 0;

unsigned long beforeOutputTime = 0;

StaticJsonDocument<500> jsonDoc1;
StaticJsonDocument<500> jsonDoc2;

Sensor sensor;


/**
 * connect to wifi
 */
void connectWifi() {
    if (strlen(wifi_ssid) > 0) {
        if (WiFi.status() != WL_CONNECTED) {
            // wait reconnect interval
            if (
                ((failedToConnectWifiTime != 0) && (millis() - failedToConnectWifiTime > reconnectWaitMilliSec)) ||
                (wifiStartConnectingTime == 0) ||
                ((wifiStartConnectingTime != 0) && (millis() - wifiStartConnectingTime > wifiConnectTimeout))
            ){
                // reconnect to wifi
                Serial.println("connect to wifi");
                WiFi.mode(WIFI_STA);
                WiFi.begin(wifi_ssid, wifi_pswd);
                failedToConnectWifiTime = 0;
                wifiStartConnectingTime = millis();
                retryWifiConnCount++;
            }
        } else {
            failedToConnectWifiTime = millis();
            wifiStartConnectingTime = 0;
            retryWifiConnCount = 0;
        }
    }
}


/**
 * connected tcp socket callback
 */
void connectedTcp(String address) {
    Serial.print("connected tcp : ");
    Serial.println(address);
    Serial.println("--------------------------");
    
    digitalWrite(greenLedPin, HIGH);
}


/**
 * disconnected tcp socket callback
 */
void disconnectedTcp(String address) {
    Serial.print("disconnected tcp : ");
    Serial.println(address);
    Serial.println("--------------------------");
    
    digitalWrite(greenLedPin, LOW);
}


/**
 * connected ble callback
 */
void connectedBle(String address) {
    Serial.println("connected ble");
    Serial.println("--------------------------");
}


/**
 * disconnected ble callback
 */
void disconnectedBle(String address) {
    Serial.println("disconnected ble");
    Serial.println("--------------------------");
}


/**
 * received broadcast data callback
 */
void receivedBroadcastData(String address, JsonDocument data) {
    
    Serial.println("received broadcast data");
    Serial.println("--------------------------");
    
    if (data["commandId"] == 1) {
        // response data
        jsonDoc1["commandId"] = 2;
        jsonDoc1["ipaddress"] = WiFi.localIP().toString();
        
        size_t jsonTextLength = measureJson(jsonDoc1) + 1;
        char jsonText[jsonTextLength];
        jsonTextLength = serializeJson(jsonDoc1, jsonText, jsonTextLength);
        
        sendBroadcast((uint8_t*)jsonText, jsonTextLength);
        
        jsonDoc1.clear();
        
    }
    
}


/**
 * received tcp socket data callback
 */
void receivedTcpData(String address, JsonDocument data) {
    
    Serial.println("received tcp data");
    Serial.println("--------------------------");
    
    if (data["commandId"] == 5) {
        // reset ssid and password
        wifi_ssid[0] = '\0';
        wifi_pswd[0] = '\0';
        
        digitalWrite(greenLedPin, LOW);
        digitalWrite(blueLedPin, HIGH);
        
        // broadcast=off, tcp=off, ble=on
        switchCommunicationEnabled(false, false, true);

        WiFi.disconnect();
        
    }
    
}


/**
 * received ble data callback
 */
void receivedBleData(String address, JsonDocument data) {
    
    Serial.println("received ble data");
    Serial.println("--------------------------");
    
    if (data["commandId"] == 0) {
        // set wifi ssid and password -------------
        
        // wifi ssid and password
        const char* ssid = data["ssid"];
        const char* pass = data["pass"];
        strcpy(wifi_ssid, ssid);
        strcpy(wifi_pswd, pass);
        
        Serial.println(wifi_ssid);
        Serial.println(wifi_pswd);
        
        // save ssid and password
        if (saveWifiSetting()) {
            Serial.println("saved wifi setting");
        }

        if (WiFi.status() == WL_CONNECTED) {
            WiFi.disconnect();
        }
        
        // response data
        jsonDoc1["commandId"] = 0;
        
        size_t jsonTextLength = measureJson(jsonDoc1) + 1;
        char jsonText[jsonTextLength];
        jsonTextLength = serializeJson(jsonDoc1, jsonText, jsonTextLength);
        
        // send response
        sendBle((uint8_t*)jsonText, jsonTextLength);
        
        jsonDoc1.clear();
        
    }
    else if (data["commandId"] == 10) {
        // send sensor data -------------
        Serial.println("received data request");
        
        // get sensor value
        float temperature = sensor.getTemperature();
        float humidity = sensor.getHumidity();
        float illuminance = sensor.getIlluminance();
        
        // response data
        jsonDoc1["commandId"] = 10;
        jsonDoc1["temperature"] = temperature;
        jsonDoc1["humidity"] = humidity;
        jsonDoc1["illuminance"] = illuminance;
        
        size_t jsonTextLength = measureJson(jsonDoc1) + 1;
        char jsonText[jsonTextLength];
        jsonTextLength = serializeJson(jsonDoc1, jsonText, jsonTextLength);
        
        // send response
        sendBle((uint8_t*)jsonText, jsonTextLength);
        
        jsonDoc1.clear();
        
    }
    
}


/**
 * save wifi setting
 */
bool saveWifiSetting() {
    bool ret = false;
    SPIFFS.begin(true);
    
    // remove file if exists
    if (SPIFFS.exists(wifiSettingPath)) {
        SPIFFS.remove(wifiSettingPath);
    }
    
    // create file
    File file = SPIFFS.open(wifiSettingPath, FILE_WRITE);
    if (file) {
        
        // save as json
        jsonDoc1["ssid"] = wifi_ssid;
        jsonDoc1["pass"] = wifi_pswd;
        
        size_t jsonTextLength = measureJson(jsonDoc1) + 1;
        char jsonText[jsonTextLength];
        jsonTextLength = serializeJson(jsonDoc1, jsonText, jsonTextLength);
        
        file.write((uint8_t*)jsonText, jsonTextLength);
        
        file.close();
        
        jsonDoc1.clear();
        
        ret = true;
    } else {
        Serial.println("cannot open setting file");
    }
    
    SPIFFS.end();
    
    return ret;
}


/**
 * load wifi setting
 */
bool loadWifiSetting() {
    bool ret = false;
    SPIFFS.begin(true);
    
    if (SPIFFS.exists(wifiSettingPath)) {
        File file = SPIFFS.open(wifiSettingPath);
        if (file) {
            
            // read json text from file
            char jsonText[200];
            file.read((uint8_t*)jsonText, 200);
            file.close();
            
            // json text to object
            auto deserializeError = deserializeJson(jsonDoc1, jsonText);
            
            if (!deserializeError) {
                const char* ssid = jsonDoc1["ssid"];
                const char* pass = jsonDoc1["pass"];
                
                strcpy(wifi_ssid, ssid);
                strcpy(wifi_pswd, pass);
            }
            
            jsonDoc1.clear();
            
            ret = true;
        } else {
            Serial.println("cannot open setting file");
        }
        
    }
    
    SPIFFS.end();
    
    return ret;
}


/**
 * initialize
 */
void setup() {
    Serial.begin(9600);
    
    pinMode(blueLedPin, OUTPUT); // blue LED
    pinMode(greenLedPin, OUTPUT); // green LED
    
    setBLEName("TemperatureMonitor");
    
    // set broadcast callback
    setBroadcastCallback(receivedBroadcastData);
    
    // set tcp callback
    setTcpCallback(connectedTcp, disconnectedTcp, receivedTcpData);
    
    // set ble callback
    setBleCallback(connectedBle, disconnectedBle, receivedBleData);
    
    sensor.initialize();
    
    
    if (loadWifiSetting()) {
        Serial.println("wifi setting loaded");
        
    } else {
        Serial.println("wifi setting is none");
        
        wifi_ssid[0] = '\0';
        wifi_pswd[0] = '\0';
        
        digitalWrite(blueLedPin, HIGH);
        
        // broadcast=off, tcp=off, ble=on
        startCommunicationTask(false, false, true);
    }
    
}


/**
 * main loop
 */
void loop() {
    
    connectWifi();
    
    if (WiFi.status() == WL_CONNECTED) {
        if (!isActiveCommunicationTask()) {
            // broadcast=on, tcp=on, ble=off
            startCommunicationTask(true, true, true);
            
        }
        if (isActiveBle()) {
            digitalWrite(blueLedPin, LOW);
            // broadcast=on, tcp=on, ble=off
            switchCommunicationEnabled(true, true, true);
        }
        
        int clientCnt = getTcpClientCount();
        if (clientCnt > 0) {
            // send data per 3seconds
            if (millis() - beforeSendTime >= 3000) {
                
                // get sensor value
                float temperature = sensor.getTemperature();
                float humidity = sensor.getHumidity();
                float illuminance = sensor.getIlluminance();
                
                // create json data
                jsonDoc2["commandId"] = 10;
                jsonDoc2["temperature"] = temperature;
                jsonDoc2["humidity"] = humidity;
                jsonDoc2["illuminance"] = illuminance;
                
                // json object to text
                size_t jsonTextLength = measureJson(jsonDoc2) + 1;
                char jsonText[jsonTextLength];
                jsonTextLength = serializeJson(jsonDoc2, jsonText, jsonTextLength);
                
                // send data
                sendTcp((uint8_t*)jsonText, jsonTextLength);
                
                jsonDoc2.clear();
                
                beforeSendTime = millis();
            }
        }
        
    } else {
        
        if (strlen(wifi_ssid) > 0) {
            // if cannot connect to wifi
            if (retryWifiConnCount > 5) {
                // reset ssid and password
                wifi_ssid[0] = '\0';
                wifi_pswd[0] = '\0';
                
                digitalWrite(blueLedPin, HIGH);
                
                // broadcast=off, tcp=off, ble=on
                switchCommunicationEnabled(false, false, true);
            }
        }
        
    }
    
    // reboot per 24hours
    if (millis() > 86400000) {
        ESP.restart();
    }
    
    communicationLoop();
    
    
    // output serial monitor
    if (millis() - beforeOutputTime > 2000) {
        float temperature = sensor.getTemperature();
        float humidity = sensor.getHumidity();
        float illuminance = sensor.getIlluminance();
        
        Serial.printf("%f -- %f -- %f", temperature, humidity, illuminance);
        Serial.println();
        
        beforeOutputTime = millis();
    }
    
    delay(100);
}
