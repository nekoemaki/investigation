import numpy as np
from BluetoothServer import BluetoothServer
import cv2


if __name__ == '__main__':

    size = (400, 300)
    jpeg_encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]

    # wait to connect client
    bluetooth_server = BluetoothServer()
    bluetooth_server.startWatingConnection()

    # get usb camera capture
    camera_cap = cv2.VideoCapture(0)

    print('camera ready')
    while bluetooth_server.isWaitingConnection():
        # if camera could open
        if not camera_cap.isOpened():
            camera_cap = cv2.VideoCapture(0)

        if (camera_cap is None) or (not camera_cap.isOpened()):
            print('cannot open camera')
            break

        if bluetooth_server.existsClient() :
            print('connected client')

        # camera capture and send loop
        while (bluetooth_server.existsClient()) and (camera_cap.isOpened()):
            # load image file by camera
            ret, cv_img = camera_cap.read()

            # send image data to all client
            if ret:
                cv_img = cv2.resize(cv_img, size)
                ret, jpeg_img = cv2.imencode('.jpg', cv_img, jpeg_encode_param)
                if ret:
                    bluetooth_server.sendDataAll(jpeg_img)

            # break when 'q' key pressed
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    # close camera
    if camera_cap is not None:
        camera_cap.release()
    # disconnect bluetooth
    bluetooth_server.disconnect()


