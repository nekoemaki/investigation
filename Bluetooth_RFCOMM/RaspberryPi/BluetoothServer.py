from time import sleep
from datetime import datetime
import threading
import sys
import hashlib
import bluetooth

from BluetoothTask import BluetoothTask

########################################################
# 
# BluetoothServer
# 
# wait to connect client socket
# receive data from client, and send data to client
# 
########################################################
class BluetoothServer :

    ########################################################
    # constructor
    def __init__(self):
        # waiting connection flag
        self.__waitingConnection = False
        # max connection count
        self.__maxConnection = 1
        # server port number
        self.__port = 1
        # callback when received data
        self.__receivedDataCallback = None
        # callback when sent data
        self.__sentDataCallback = None
        # server socket
        self.__socket = None
        # destination socket
        self.__clientSockets = {}
        # task
        self.__clientTasks = {}

    ########################################################
    # Property : received data callback
    @property
    def receivedDataCallback(self):
        pass
    @receivedDataCallback.getter
    def receivedDataCallback(self):
        return self.__receivedDataCallback
    @receivedDataCallback.setter
    def receivedDataCallback(self,value):
        self.__receivedDataCallback = value
        return

    ########################################################
    # Property : sent data callback
    @property
    def sentDataCallback(self):
        pass
    @sentDataCallback.getter
    def sentDataCallback(self):
        return self.__sentDataCallback
    @sentDataCallback.setter
    def sentDataCallback(self,value):
        self.__sentDataCallback = value
        return

    ########################################################
    # Property : max connection
    @property
    def maxConnection(self):
        pass
    @maxConnection.getter
    def maxConnection(self):
        return self.__maxConnection
    @maxConnection.setter
    def maxConnection(self, value):
        self.__maxConnection = value
        return

    ########################################################
    # Property : port
    @property
    def port(self):
        pass
    @port.getter
    def port(self):
        return self.__port
    @port.setter
    def port(self, value):
        self.__port = value
        return

    ########################################################
    # get waiting connection status
    def isWaitingConnection(self):
        return self.__waitingConnection

    ########################################################
    # exists connected client
    def existsClient(self):
        return (len(self.__clientSockets) > 0)

    ########################################################
    # get connected clients address
    def getClientsAddr(self):
        return list(self.__clientSockets.keys())

    ########################################################
    # start to wait connection by destination client
    def startWatingConnection(self):
        if not self.__waitingConnection :
            self.__waitingConnection = True
            self.__waitingConnectionThread = threading.Thread(target=self.waitingConnectionThread, args=(), daemon=True)
            self.__waitingConnectionThread.start()

    ########################################################
    # stop to wait connection
    def stopWatingConnection(self):
        self.__waitingConnection = False

    ########################################################
    # send data to all connected clients
    def sendDataAll(self, data):
        for key in self.__clientTasks:
            self.__clientTasks[key].sendData(data)

    ########################################################
    # send data to addr client
    def sendData(self, addr, data):
        if self.__clientTasks[addr] is not None:
            return self.__clientTasks[addr].sendData(data)
        else:
            return False

    ########################################################
    # wait to connect destination
    # called from startWatingConnection method
    def waitingConnectionThread(self):
        while self.__waitingConnection:
            # create server socket
            self.__socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
            self.__socket.bind(("", self.__port))
            self.__socket.listen(self.__maxConnection)

            print('wait to connect {}'.format(self.__port))
            while self.__waitingConnection and self.__socket is not None:
                try:
                    dest_socket, dest_addr = self.__socket.accept()

                    print('new destination connected : addr={}'.format(dest_addr))

                    if dest_addr in self.__clientSockets:
                        self.__clientSockets[dest_addr].close()
                        self.__clientSockets[dest_addr] = None

                    self.__clientSockets[dest_addr] = dest_socket

                    if dest_addr in self.__clientTasks:
                        self.__clientTasks[dest_addr].dispose()
                        self.__clientTasks[dest_addr] = None
                    
                    self.__clientTasks[dest_addr] = BluetoothTask(dest_socket, dest_addr, self.__sentDataCallback, self.__receivedDataCallback)

                except Exception as e:
                    print('exception on connected client : {}'.format(e))
                    self.__socket.close()
                    self.__socket = None
                    break

            # if server socket error, reconnect after 3seconds
            if self.__waitingConnection:
               sleep(3)

        if self.__socket is not None:
            self.__socket.close()
            self.__socket = None

    ########################################################
    # disconnect socket
    def disconnect(self):
        self.__waitingConnection = False
        
        for key in self.__clientSockets:
            self.__clientSockets[key].close()
        self.__clientSockets.clear()
        
        for key in self.__clientTasks:
            self.__clientTasks[key].dispose()
        self.__clientTasks.clear()
        
        if (self.__socket is not None) :
            self.__socket.close()
        self.__socket = None


