import numpy as np
from BluetoothClient import BluetoothClient
import cv2
import argparse


receivedAddr = ''
receivedData = None

# received data callback
def onReceivedData(addr, data):
    if receivedAddr == '':
        receivedAddr = addr

    if receivedAddr == addr:
        receivedData = data


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('address')
    args = parser.parse_args()

    # bluetooth client
    bluetooth_client = BluetoothClient(args.address)
    # set received data callback
    bluetooth_client.receivedDataCallback = onReceivedData
    # start to wait connection
    res = bluetooth_client.connect()

    if res:
        while (bluetooth_client.isConnected()):
            # show received image
            if receivedData is not None:
                cv_img = cv2.imdecode(receivedData)
                cv2.imshow('camera image', cv_img)
                receivedData = None

            # break when 'q' key pressed
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cv2.destroyAllWindows()
        # disconnect bluetooth
        bluetooth_client.disconnect()

