from time import sleep
from datetime import datetime
import threading
import sys
import hashlib
import bluetooth


########################################################
# 
# BluetoothTask
# 
# send and receive data manager
# 
########################################################
class BluetoothTask :

    # top of data
    DATA_HEADER = 'BLUETOOTH-DATAHEADER'.encode('utf-8')

    # limit data size is 20MB
    LIMIT_SEND_DATASIZE = 20*1024*1024

    # data size when receive un-need data
    PACKET_SIZE = 512

    # max length send data que
    SEND_DATA_QUE_LIMIT = 10

    ########################################################
    # constructor
    #  socket ... client socket
    #  addr ... client address
    #  sentDataCallback ... callback when completed to send data
    #  receivedDataCallback ... callback when completed to receive data
    def __init__(self, socket, addr, sentDataCallback, receivedDataCallback):
        self.__socket = socket
        self.__address = addr
        self.__sendDataQue = []

        self.__sending = True
        self.__sendDataThread = threading.Thread(target=self.sendDataThread, args=(), daemon=True)
        self.__sendDataThread.start()

        self.__receiving = True
        self.__receiveDataThread = threading.Thread(target=self.receiveDataThread, args=(), daemon=True)
        self.__receiveDataThread.start()

        self.__sentDataCallback = sentDataCallback
        self.__receivedDataCallback = receivedDataCallback


    ########################################################
    # add data to send data que
    def sendData(self, data):
        if (len(self.__sendDataQue) >= BluetoothTask.SEND_DATA_QUE_LIMIT) :
            return False
        else :
            self.__sendDataQue.append(data)
            return True


    ########################################################
    # dispose
    def dispose(self):
        self.__sending = False
        self.__receiving = False


    ########################################################
    # send data thread
    def sendDataThread(self):

        while self.__sending :
            # loop while exists data in que
            while len(self.__sendDataQue) > 0 :
                try :
                    data = self.__sendDataQue[0]
                    self.__sendDataQue.pop(0)

                    # send data header
                    self.sendDataToSocket(BluetoothTask.DATA_HEADER)

                    # send data id
                    dataId = datetime.now().strftime('%Y%m%d%H%M%S%f')
                    self.sendDataToSocket(dataId.encode('utf-8'))

                    # send md5
                    hash=hashlib.md5(bytes(data)).hexdigest()
                    self.sendDataToSocket(hash.encode('utf-8'))

                    # send data size (use BigEndian for communicate to Android)
                    datasizeBytes = len(data).to_bytes(4, 'big')
                    self.sendDataToSocket(datasizeBytes)

                    # send data
                    self.sendDataToSocket(data)

                    # execute callback
                    if self.__sentDataCallback is not None:
                        self.__sentDataCallback()

                except Exception as e:
                    #
                    print('send data thread error : {}'.format(e))

            sleep(0.1)


    ########################################################
    # send data to socket
    def sendDataToSocket(self, data) :
        totalSent = 0
        while totalSent < len(data):
            sent = self.__socket.send(data[totalSent:])
            totalSent += sent


    ########################################################
    # receive data thread
    def receiveDataThread(self):

        offset = 0
        dataHeaderSize = 20
        dataIdSize = 20
        md5Size = 32
        datasizeSize = 4

        topOfDataId = dataHeaderSize
        topOfMd5 = dataHeaderSize + dataIdSize
        topOfDatasize = dataHeaderSize + dataIdSize + md5Size
        topOfData = dataHeaderSize + dataIdSize + md5Size + datasizeSize

        byDataId = []
        byMd5 = []
        byDatasize = []
        byData = []
        datasize = 0

        while self.__receiving :
            try:

                # receive data header
                if offset < topOfDataId:
                    res = self.__socket.recv(dataHeaderSize - offset)
                    if len(res) > 0:
                        for idx in range(len(res)):
                            if res[idx]==BluetoothTask.DATA_HEADER[offset]:
                                offset += 1
                            else:
                                offset = 0

                # receive data id
                elif offset < topOfMd5:
                    res = self.__socket.recv(dataIdSize - (offset - topOfDataId))
                    if len(res) > 0:
                        byDataId.extend(res)
                        offset += len(res)

                # receive md5
                elif offset < topOfDatasize:
                    res = self.__socket.recv(md5Size - (offset - topOfMd5))
                    if len(res) > 0:
                        byMd5.extend(res)
                        offset += len(res)

                # receive datasize
                elif offset < topOfData:
                    res = self.__socket.recv(datasizeSize - (offset - topOfDatasize))
                    if len(res) > 0:
                        byDatasize.extend(res)
                        offset += len(res)
                        if len(byDatasize) == datasizeSize:
                            datasize = int.from_bytes(byDatasize, 'big')
                            if datasize > BluetoothTask.LIMIT_SEND_DATASIZE:
                                offset = 0

                # receive data
                else:
                    res = self.__socket.recv(datasize - (offset - topOfData))
                    if len(res) > 0:
                        byData.extend(res)
                        offset += len(res)

                        # completed to receive data
                        if len(byData) == datasize:

                            # check hash
                            receiveBufferHash = hashlib.md5(bytes(byData)).hexdigest()
                            if (byMd5).decode('utf-8') == receiveBufferHash :

                                # execute callback
                                if self.__receivedDataCallback is not None:
                                    self.__receivedDataCallback(self.__address, byData)

                            # reset parameter
                            offset = 0
                            byDataId = []
                            byMd5 = []
                            byDatasize = []
                            byData = []
                            datasize = 0

            except :
                #
                print('receive thread error')


