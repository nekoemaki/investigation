package net.nekoemaki.bluetoothcontroller.BluetoothCommunication;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import net.nekoemaki.bluetoothcontroller.Common.Logger;

/**
 * BluetoothServer class
 */
public class BluetoothServer {
    private static final String SECURE_NAME = "BluetoothServer";
    private static final UUID UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothServerSocket bluetoothServerSocket;
    private WaitConnectionThread waitConnectionThread;

    private BluetoothEventListener bluetoothEventListener;

    HashMap<String, BluetoothClientTask> connectedClients;

    /**
     * constructor
     */
    public BluetoothServer() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothServerSocket = null;
        connectedClients = new HashMap<>();
    }

    /**
     * set event callback
     * @param listener
     */
    public void setBluetoothEventListener(BluetoothEventListener listener) {
        bluetoothEventListener = listener;
    }

    /**
     * get connected client name and address
     * @return
     */
    public String[][] getConnectedClientAddress() {
        String [][] ret = new String[connectedClients.size()][2];
        int cnt = 0;
        for (String key : connectedClients.keySet()) {
            BluetoothClientTask cs = connectedClients.get(key);
            ret[cnt][0] = cs.device.getName();
            ret[cnt][1] = cs.device.getAddress();
        }
        return ret;
    }

    /**
     * return true when exists connected client
     * @return
     */
    public boolean hasConnectedClient() {
        if (connectedClients.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * start to wait connecting client
     */
    public boolean startWatingConnection() {
        if (bluetoothServerSocket == null) {
            try {
                bluetoothServerSocket = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(SECURE_NAME, UUID_SECURE);

                if (bluetoothServerSocket == null) {
                    return false;
                } else {
                    waitConnectionThread = new WaitConnectionThread();
                    waitConnectionThread.start();
                    return true;
                }

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

        } else {
            return false;
        }
    }

    /**
     * stop to wait connecting client
     */
    public void stopWaitingConnection() {
        if (waitConnectionThread != null) {
            if (waitConnectionThread.isAlive()) {
                waitConnectionThread.interrupt();
                try {
                    waitConnectionThread.join(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            waitConnectionThread = null;
        }

        if (bluetoothServerSocket != null) {
            try {
                bluetoothServerSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bluetoothServerSocket = null;
        }
    }

    /**
     * send data to all connected client
     * @param data
     */
    public boolean sendDataAll(byte[] data) {
        if (hasConnectedClient()) {
            final byte[] bytes = new byte[data.length];
            System.arraycopy(data, 0, bytes, 0, data.length);

            for (String key : connectedClients.keySet()) {
                final BluetoothClientTask cs = connectedClients.get(key);
                cs.sendData(bytes);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * send data to connected client by device name
     * @param deviceName
     * @param data
     * @return
     */
    public boolean sendDataByName(String deviceName, byte[] data) {
        if (hasConnectedClient()) {
            boolean ret = false;

            final byte[] bytes = new byte[data.length];
            System.arraycopy(data, 0, bytes, 0, data.length);

            for (String key : connectedClients.keySet()) {
                final BluetoothClientTask cs = connectedClients.get(key);
                if (cs.device.getName().equals(deviceName)) {
                    ret = true;
                    cs.sendData(bytes);
                }
            }
            return ret;
        } else {
            return false;
        }
    }

    /**
     * send data to connected client by address
     * @param address
     * @param data
     * @return
     */
    public boolean sendDataByAddress(String address, byte[] data) {
        if (hasConnectedClient()) {
            boolean ret = false;

            final byte[] bytes = new byte[data.length];
            System.arraycopy(data, 0, bytes, 0, data.length);

            for (String key : connectedClients.keySet()) {
                final BluetoothClientTask cs = connectedClients.get(key);
                if (cs.device.getAddress().equals(address)) {
                    ret = true;
                    cs.sendData(bytes);
                    break;
                }
            }
            return ret;
        } else {
            return false;
        }
    }

    /**
     * disconnect client
     * @param name
     */
    public void disconnect(String name) {
        for (String key : connectedClients.keySet()) {
            if (connectedClients.get(key).device.getName().equals(name)) {
                connectedClients.get(key).disconnect();
                BluetoothDevice device = connectedClients.get(key).device;
                connectedClients.remove(key);

                if (bluetoothEventListener != null) {
                    bluetoothEventListener.onDisconnected(device);
                }

                break;
            }
        }
    }

    /**
     * disconnect all connected client
     */
    public void disconnect() {
        if (waitConnectionThread != null) {
            if (waitConnectionThread.isAlive()) {
                waitConnectionThread.interrupt();
                try {
                    waitConnectionThread.join(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            waitConnectionThread = null;
        }

        if (bluetoothServerSocket != null) {
            try {
                bluetoothServerSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bluetoothServerSocket = null;
        }

        for (String key : connectedClients.keySet()) {
            connectedClients.get(key).disconnect();
        }
        connectedClients.clear();

        if (bluetoothEventListener != null) {
            bluetoothEventListener.onDisconnected(null);
        }
    }

    /**
     * wait to connect client thread
     */
    class WaitConnectionThread extends Thread {

        /**
         * constructor
         */
        public WaitConnectionThread() {
            //
        }

        @Override
        public void run() {
            try {

                while ((bluetoothServerSocket != null)) {
                    if (Thread.interrupted()) {
                        break;
                    }

                    // wait connecting client
                    BluetoothSocket bluetoothClientSocket = bluetoothServerSocket.accept();

                    if (bluetoothClientSocket != null) {
                        // client connected
                        Logger.addLog(String.format("connected client : %s", bluetoothClientSocket.getRemoteDevice().getName()));

                        // wait to receive data
                        if (connectedClients.containsKey(bluetoothClientSocket.getRemoteDevice().getAddress())) {
                            connectedClients.get(bluetoothClientSocket.getRemoteDevice().getAddress()).setSocket(bluetoothClientSocket);

                        } else {
                            BluetoothClientTask task = new BluetoothClientTask(bluetoothClientSocket, bluetoothEventListener);
                            connectedClients.put(task.device.getAddress(), task);

                        }

                        if (bluetoothEventListener != null) {
                            bluetoothEventListener.onConnected(bluetoothClientSocket.getRemoteDevice());
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

