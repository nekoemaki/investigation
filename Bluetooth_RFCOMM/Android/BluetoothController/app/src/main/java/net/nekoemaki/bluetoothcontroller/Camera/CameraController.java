package net.nekoemaki.bluetoothcontroller.Camera;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.YuvImage;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Surface;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

import net.nekoemaki.bluetoothcontroller.Common.Logger;

public class CameraController {
    private Context context;
    private String currentCameraId;
    private CameraManager cameraManager;
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSession;
    private SurfaceTexture surfaceTexture;
    private Surface surface;
    private ImageReader imageReader;
    private Point cameraImageSize;
    private Handler handler;
    private CaptureRequest.Builder previewRequestBuilder;

    public CameraEventListener cameraEventListener;

    /**
     *
     * @param context
     */
    public CameraController(Context context, SurfaceTexture surfaceTexture) {
        this.context = context;
        this.surfaceTexture = surfaceTexture;
        this.cameraImageSize = new Point(1280, 720);

        cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
    }

    /**
     *
     * @param width
     * @param height
     */
    public void SetImageSize(int width, int height) {
        this.cameraImageSize.set(width, height);

        if (this.cameraCaptureSession != null) {
            try {
                this.cameraCaptureSession.stopRepeating();

                this.previewRequestBuilder.removeTarget(this.imageReader.getSurface());
                imageReader.close();

                this.imageReader = ImageReader.newInstance(this.cameraImageSize.x, this.cameraImageSize.y, ImageFormat.YUV_420_888, 2);
                this.imageReader.setOnImageAvailableListener(onImageAvailableListener, handler);
                this.previewRequestBuilder.addTarget(this.imageReader.getSurface());

                this.cameraCaptureSession.setRepeatingRequest(this.previewRequestBuilder.build(), capturedCallback, handler);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 画像サイズを取得
     * @return
     */
    public Point GetImageSize() {
        return this.cameraImageSize;
    }

    /**
     * カメラが開いているかチェック
     * @return
     */
    public boolean IsOpened() {
        return (this.cameraCaptureSession != null);
    }

    /**
     * 正面カメラIDを取得
     * @return
     */
    public String GetFrontCameraId() {
        String cameraId = "";
        try {
            String[] cameraIds = this.cameraManager.getCameraIdList();
            for (int i=0; i<cameraIds.length; i++) {
                CameraCharacteristics cameraCharacteristics = this.cameraManager.getCameraCharacteristics(cameraIds[i]);
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT) {
                    cameraId = cameraIds[i];
                    break;
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return cameraId;
    }

    /**
     * 背面カメラIDを取得
     * @return
     */
    public String GetBackCameraId() {
        String cameraId = "";
        try {
            String[] cameraIds = this.cameraManager.getCameraIdList();
            for (int i=0; i<cameraIds.length; i++) {
                CameraCharacteristics cameraCharacteristics = this.cameraManager.getCameraCharacteristics(cameraIds[i]);
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_BACK) {
                    cameraId = cameraIds[i];
                    break;
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return cameraId;
    }

    /**
     * カメラを開く
     * @param cameraId
     */
    public void OpenCamera(String cameraId){

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            Logger.addLog("none permission");
            return;
        }

        if (cameraDevice != null){
            CloseCamera();
        }

        try {
            Logger.addLog(String.format("set camera size : %dx%d", cameraImageSize.x, cameraImageSize.y));

            handler = new Handler();

            imageReader = ImageReader.newInstance(cameraImageSize.x, cameraImageSize.y, ImageFormat.YUV_420_888, 2);
            imageReader.setOnImageAvailableListener(onImageAvailableListener, handler);

            currentCameraId = cameraId;
            Logger.addLog("camera opening id=" + currentCameraId);
            cameraManager.openCamera(cameraId, openCallback, handler);
        } catch(CameraAccessException e){
            e.printStackTrace();
        }

    }

    /**
     * カメラを閉じる
     */
    public void CloseCamera() {
        Logger.addLog("camera close id=" + currentCameraId);
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }
        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
        if (cameraEventListener != null) {
            cameraEventListener.onCameraClosed();
        }
    }

    private CameraDevice.StateCallback openCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            Logger.addLog("camera opened id=" + currentCameraId);
            cameraDevice = camera;
            try {

                surfaceTexture.setDefaultBufferSize(cameraImageSize.x, cameraImageSize.y);
                surface = new Surface(surfaceTexture);

                List<Surface> outputs;
                if (surface != null) {
                    outputs = Arrays.asList(surface, imageReader.getSurface());
                } else {
                    outputs = Arrays.asList(imageReader.getSurface());
                }
                cameraDevice.createCaptureSession(outputs, configuredCallback, handler);

                if (cameraEventListener != null) {
                    cameraEventListener.onCameraOpened();
                }

            } catch (CameraAccessException e){
                e.printStackTrace();
                if (cameraEventListener != null) {
                    cameraEventListener.onCameraError();
                }
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            Logger.addLog("camera disconnected id=" + currentCameraId);
            if (cameraDevice != null) {
                cameraDevice.close();
                cameraDevice = null;
            }
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            Log.e("SingleCameraController", "camera open error id=" + currentCameraId);
            if (cameraDevice != null) {
                cameraDevice.close();
                cameraDevice = null;
            }
            if (cameraEventListener != null) {
                cameraEventListener.onCameraError();
            }
        }
    };

    private CameraCaptureSession.StateCallback configuredCallback = new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
            Logger.addLog("camera configured id=" + currentCameraId);
            cameraCaptureSession = session;
            try{

                previewRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                previewRequestBuilder.addTarget(imageReader.getSurface());

                if (surface != null) {
                    previewRequestBuilder.addTarget(surface);
                }
                cameraCaptureSession.setRepeatingRequest(previewRequestBuilder.build(), capturedCallback, handler);

            } catch(CameraAccessException e){
                e.printStackTrace();
                if (cameraEventListener != null) {
                    cameraEventListener.onCameraError();
                }
            }
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
            Log.e("SingleCameraController", "camera configured error2 id=" + currentCameraId);
        }
    };

    private CameraCaptureSession.CaptureCallback capturedCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);

        }
    };

    private ImageReader.OnImageAvailableListener onImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {

            // ImageReaderから画像データ(byte配列)を取得 (YUV)
            Image image = reader.acquireLatestImage();
            if (image == null) return;

            Image.Plane[] planes = image.getPlanes();
            if (planes == null) return;

            if (!previewConverting) {
                // get YUV image byte array
                ByteBuffer buf0 = planes[0].getBuffer(); // Y
//              ByteBuffer buf1 = planes[1].getBuffer(); // U
                ByteBuffer buf2 = planes[2].getBuffer(); // V
                int Ylength = buf0.remaining();
                int Vlength = buf2.remaining();

                previewImage = new byte[Ylength + Vlength];
                buf0.get(previewImage, 0, Ylength);
                buf2.get(previewImage, Ylength, Vlength);

                previewTimestamp = image.getTimestamp();

                // YUV to JPEG の処理は時間がかかるため別スレッドで行う
                previewHandler.post(previewRunnable);
            }

            if (image != null) image.close();

        }
    };

    private boolean previewConverting = false;
    private byte[] previewImage;
    private long previewTimestamp;
    private Handler previewHandler = new Handler();
    private Runnable previewRunnable = new Runnable() {
        @Override
        public void run() {
            previewHandler.removeCallbacks(previewRunnable);

            previewConverting = true;

            // YUV to JPEG
            byte[] jpegBytes = null;
            try {
                YuvImage yuvImage;
                yuvImage = new YuvImage(previewImage, ImageFormat.NV21, cameraImageSize.x, cameraImageSize.y, null);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                yuvImage.compressToJpeg(new Rect(0, 0, cameraImageSize.x, cameraImageSize.y), 50, out);
                jpegBytes = out.toByteArray();
                out.close();
            } catch(Exception e) {
                e.printStackTrace();
            }

            // callback
            if ((cameraEventListener != null) && (jpegBytes != null)) {
                cameraEventListener.onCameraPreview(jpegBytes, TimestampToKey(previewTimestamp));
            }

            previewConverting = false;

        }
    };

    private int TimestampToKey(long timestamp){
        double v = (double) (timestamp / 1000000000.0);
        double v2 = v - (int)v;
        //9,8桁目のみ使用
        return (int)(v2 * 100);
    }
}
