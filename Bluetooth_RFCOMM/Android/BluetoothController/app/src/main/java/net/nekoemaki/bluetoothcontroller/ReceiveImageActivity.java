package net.nekoemaki.bluetoothcontroller;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import net.nekoemaki.bluetoothcontroller.BluetoothCommunication.BluetoothClient;
import net.nekoemaki.bluetoothcontroller.BluetoothCommunication.BluetoothEventListener;
import net.nekoemaki.bluetoothcontroller.Common.Logger;

public class ReceiveImageActivity extends AppCompatActivity {

    private TextView statusTextView;
    private ImageView receivedImageView;
    private Button startButton;
    private Button stopButton;
    private Spinner targetSpinner;
    private ArrayAdapter<String> spinnerAdapter;

    private BluetoothClient bluetoothClient;
    private Bitmap receivedImage;
    private final Object imageSyncObj = new Object();

    private Bitmap dispImage;
    private Canvas dispCanvas;
    private Paint dispPaint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_image);

        WindowManager wm = getWindowManager();
        Display disp = wm.getDefaultDisplay();
        Point dispSizePoint = new Point();
        disp.getSize(dispSizePoint);

        statusTextView = findViewById(R.id.receiveimageactivity_status_textview);
        targetSpinner = findViewById(R.id.receiveimageactivity_target_spinner);
        receivedImageView = findViewById(R.id.receiveimageactivity_received_imageview);

        LinearLayout buttonLayout = findViewById(R.id.receiveimageactivity_button_layout);

        startButton = new Button(getApplicationContext());
        startButton.setText("開始");
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String targetName = (String)targetSpinner.getSelectedItem();
                if ((targetName != null) && (!targetName.equals(""))) {
                    if (!bluetoothClient.connectByName(targetName)) {
                        statusTextView.setText("failed to connect");
                    }
                }
            }
        });
        buttonLayout.addView(startButton, new ViewGroup.LayoutParams(dispSizePoint.x/2, ViewGroup.LayoutParams.WRAP_CONTENT));

        stopButton = new Button(getApplicationContext());
        stopButton.setText("停止");
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bluetoothClient.isConnected()) {
                    bluetoothClient.disconnect();
                }
            }
        });
        buttonLayout.addView(stopButton, new ViewGroup.LayoutParams(dispSizePoint.x/2, ViewGroup.LayoutParams.WRAP_CONTENT));

        bluetoothClient = new BluetoothClient();
        bluetoothClient.setBluetoothEventListener(bluetoothEventListener);

        updateSpinnerHandler.post(updateSpinnerRunnable);
        updateImageHandler.post(updateImageRunnable);

    }

    private void updateTargetSpinner() {
        BluetoothDevice[] devices = bluetoothClient.getPairingDevices();
        if ((devices != null) && (devices.length > 0)) {
            if ((spinnerAdapter == null) || (spinnerAdapter.getCount() != devices.length)) {
                String[] items = new String[devices.length];
                for (int i = 0; i < devices.length; i++) {
                    items[i] = devices[i].getName();
                }
                spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, items);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                targetSpinner.setAdapter(spinnerAdapter);
            }
        }
    }

    private BluetoothEventListener bluetoothEventListener = new BluetoothEventListener(){
        @Override
        public void onConnected(BluetoothDevice device) {
            super.onConnected(device);

            statusTextView.setText("connected");
        }

        @Override
        public void onConnectFailed(BluetoothDevice device) {
            super.onConnectFailed(device);

            statusTextView.setText("failed to connect");
        }

        @Override
        public void onReceivedData(BluetoothDevice device, byte[] receivedData) {
            super.onReceivedData(device, receivedData);

            synchronized (imageSyncObj) {
                //if (receivedImage != null) {
                //    receivedImage.recycle();
                //    receivedImage = null;
                //}
                Logger.addLog("update image");
                try {
                    receivedImage = BitmapFactory.decodeByteArray(receivedData, 0, receivedData.length);
                } catch (Exception e) {
                    e.printStackTrace();
                    Logger.addLog("failed to convert bitmap");
                }
            }
        }

        @Override
        public void onDisconnected(BluetoothDevice device) {
            super.onDisconnected(device);

            statusTextView.setText("disconnected");
        }
    };

    private Handler updateSpinnerHandler = new Handler();
    private Runnable updateSpinnerRunnable = new Runnable() {
        @Override
        public void run() {
            updateSpinnerHandler.removeCallbacks(updateSpinnerRunnable);

            updateTargetSpinner();

            updateSpinnerHandler.postDelayed(updateSpinnerRunnable, 1000);
        }
    };

    private Handler updateImageHandler = new Handler();
    private Runnable updateImageRunnable = new Runnable() {
        @Override
        public void run() {
            updateImageHandler.removeCallbacks(updateImageRunnable);

            synchronized (imageSyncObj) {
                if ((receivedImage != null) && (!receivedImage.isRecycled())) {

                    if (dispImage == null) {
                        dispImage = receivedImage.copy(Bitmap.Config.ARGB_8888, true);
                        dispCanvas = new Canvas(dispImage);
                        dispPaint = new Paint();
                    }

                    dispCanvas.drawBitmap(receivedImage, 0, 0, dispPaint);

                    receivedImageView.setImageBitmap(dispImage);
                    receivedImageView.invalidate();
                }
            }

            updateImageHandler.postDelayed(updateImageRunnable, 100);
        }
    };

}
