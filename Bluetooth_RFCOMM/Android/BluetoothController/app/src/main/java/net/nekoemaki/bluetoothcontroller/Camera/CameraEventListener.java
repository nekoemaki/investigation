package net.nekoemaki.bluetoothcontroller.Camera;


public class CameraEventListener {
    public void onCameraOpened(){}
    public void onCameraError(){}
    public void onCameraClosed(){}
    public void onCameraPreview(byte[] jpegImg, int key){}
}
