package net.nekoemaki.bluetoothcontroller;

import android.bluetooth.BluetoothDevice;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.nekoemaki.bluetoothcontroller.BluetoothCommunication.BluetoothEventListener;
import net.nekoemaki.bluetoothcontroller.BluetoothCommunication.BluetoothServer;
import net.nekoemaki.bluetoothcontroller.Camera.CameraController;
import net.nekoemaki.bluetoothcontroller.Camera.CameraEventListener;

public class SendImageActivity extends AppCompatActivity {

    private TextView statusTextView;
    private TextureView previewTextureView;
    private Button startButton;
    private Button stopButton;

    private CameraController cameraController;
    private BluetoothServer bluetoothServer;
    private long lastSendTime = System.currentTimeMillis();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_image);

        WindowManager wm = getWindowManager();
        Display disp = wm.getDefaultDisplay();
        Point dispSizePoint = new Point();
        disp.getSize(dispSizePoint);

        LinearLayout buttonLayout = findViewById(R.id.sendimageactivity_button_layout);

        startButton = new Button(getApplicationContext());
        startButton.setText("開始");
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cameraId = cameraController.GetFrontCameraId();
                if ((cameraId == null) || (cameraId.equals(""))) {
                    cameraId = "0";
                }
                cameraController.OpenCamera(cameraId);
                bluetoothServer.startWatingConnection();
            }
        });
        buttonLayout.addView(startButton, new ViewGroup.LayoutParams(dispSizePoint.x/2, ViewGroup.LayoutParams.WRAP_CONTENT));

        stopButton = new Button(getApplicationContext());
        stopButton.setText("停止");
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothServer.disconnect();
                cameraController.CloseCamera();
            }
        });
        buttonLayout.addView(stopButton, new ViewGroup.LayoutParams(dispSizePoint.x/2, ViewGroup.LayoutParams.WRAP_CONTENT));

        statusTextView = findViewById(R.id.sendimageactivity_status_textview);

        previewTextureView = findViewById(R.id.sendimageactivity_preview_textureview);
        previewTextureView.setSurfaceTextureListener(surfaceTextureListener);

        bluetoothServer = new BluetoothServer();
        bluetoothServer.setBluetoothEventListener(bluetoothEventListener);

    }

    private TextureView.SurfaceTextureListener surfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            cameraController = new CameraController(getApplicationContext(), surface);
            cameraController.cameraEventListener = cameraEventListener;
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };

    private BluetoothEventListener bluetoothEventListener = new BluetoothEventListener(){
        @Override
        public void onConnected(BluetoothDevice device) {
            //statusTextView.setText("connected client : " + device.getName());
        }
    };

    private CameraEventListener cameraEventListener = new CameraEventListener(){
        @Override
        public void onCameraOpened() {
            super.onCameraOpened();

            statusTextView.setText("camera open");
        }

        @Override
        public void onCameraClosed() {
            super.onCameraClosed();

            statusTextView.setText("camera close");
        }

        @Override
        public void onCameraError() {
            super.onCameraError();

            statusTextView.setText("camera error");
        }

        @Override
        public void onCameraPreview(byte[] jpegImg, int key) {
            super.onCameraPreview(jpegImg, key);

            if (bluetoothServer.hasConnectedClient()) {
                if ((System.currentTimeMillis() - lastSendTime) > 200) {
                    bluetoothServer.sendDataAll(jpegImg);
                }
            }
        }
    };
}
