package net.nekoemaki.bluetoothcontroller.BluetoothCommunication;

import android.bluetooth.BluetoothDevice;

public class BluetoothEventListener {
    public void onConnected(BluetoothDevice device){}
    public void onConnectFailed(BluetoothDevice device){}
    public void onDisconnected(BluetoothDevice device){}
    public void onSentData(BluetoothDevice device){}
    public void onReceivedData(BluetoothDevice device, byte[] receivedData){}
}
