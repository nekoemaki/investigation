package net.nekoemaki.bluetoothcontroller.Common;

import android.util.Log;

public class Logger {
    public static void addLog(String msg) {
        Log.d("BluetoothController", msg);
    }

    public static void addWarningLog(String msg) {
        Log.w("BluetoothController", msg);
    }

    public static void addErrorLog(String msg) {
        Log.e("BluetoothController", msg);
    }

}
