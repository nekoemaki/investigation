package net.nekoemaki.bluetoothcontroller.BluetoothCommunication;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import net.nekoemaki.bluetoothcontroller.Common.Logger;

/**
 * Bluetooth : send and receive data task manager
 */
public class BluetoothClientTask {
    private static final byte[] DATA_TOP_MARK = "BLUETOOTH-DATAHEADER".getBytes();
    private static final int DATASIZE_LIMIT = 20 * 1024 * 1024;
    private static final int PACKET_SIZE = 512;
    private static final int SEND_DATA_QUE_LIMIT = 10;

    public BluetoothDevice device;
    public BluetoothSocket socket;
    public SendDataThread sendDataThread;
    public ReceiveDataThread receiveDataThread;

    /**
     * constructor
     * @param bs
     */
    public BluetoothClientTask(BluetoothSocket bs, BluetoothEventListener event) {
        socket = bs;
        if (socket != null) {
            try {
                device = socket.getRemoteDevice();

                receiveDataThread = new ReceiveDataThread(device, socket, event);
                receiveDataThread.start();

                sendDataThread = new SendDataThread(device, socket, event);
                sendDataThread.start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param socket
     */
    public void setSocket(BluetoothSocket socket) {
        this.socket = socket;
        if (receiveDataThread != null) {
            receiveDataThread.setSocket(socket);
        }
        if (sendDataThread != null) {
            sendDataThread.setSocket(socket);
        }
    }

    /**
     * set received data callback
     * @param callback
     */
    public void setReceivedDataCallback(BluetoothEventListener callback) {
        receiveDataThread.setReceivedDataCallback(callback);
    }

    /**
     *
     * @param callback
     */
    public void setSentDataCallback(BluetoothEventListener callback) {
        sendDataThread.setSentDataCallback(callback);
    }

    /**
     *
     * @param data
     */
    public boolean sendData(byte[] data) {
        if (data.length > DATASIZE_LIMIT) {
            Logger.addWarningLog("send data size is over limit");
            return false;
        }
        return sendDataThread.sendData(data);
    }

    /**
     * disconnect client socket
     */
    public void disconnect() {
        if (receiveDataThread != null) {
            receiveDataThread.receiving = false;

            receiveDataThread.interrupt();
            try {
                receiveDataThread.join(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            receiveDataThread = null;
        }

        if (sendDataThread != null) {
            sendDataThread.sending = false;

            sendDataThread.interrupt();
            try {
                sendDataThread.join(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sendDataThread = null;
        }

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket = null;
        }

        device = null;
    }

    /**
     * create send data id
     * @return
     */
    private String createDataId() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        long microSeconds = System.nanoTime() / 1000;
        String dataId = dateFormat.format(System.currentTimeMillis()) + String.format("%03d", (microSeconds % 1000));
        return dataId;
    }

    /**
     * get md5 from byte array
     * @param data
     * @return
     */
    private String getMD5(byte[] data) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(data);
            byte[] md5Bytes = md.digest();
            String strMd5 = "";
            for (int i=0; i<md5Bytes.length; i++){
                strMd5 += String.format("%02x", md5Bytes[i]);
            }
            return strMd5;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     *
     */
    class SendDataThread extends Thread {
        public boolean sending;
        private BluetoothDevice device;
        private BluetoothSocket socket;
        private ArrayList<byte[]> sendDataQue;
        private BluetoothEventListener sentDataCallback;

        public SendDataThread(BluetoothDevice dev, BluetoothSocket socket, BluetoothEventListener callback) {
            this.device = dev;
            this.socket = socket;
            this.sendDataQue = new ArrayList<>();
            this.sentDataCallback = callback;
        }

        /**
         *
         * @param socket
         */
        public void setSocket(BluetoothSocket socket) {
            this.socket = socket;
        }

        /**
         *
         * @param callback
         */
        public void setSentDataCallback(BluetoothEventListener callback) {
            sentDataCallback = callback;
        }

        /**
         *
         * @param data
         */
        public boolean sendData(byte[] data) {
            synchronized (sendDataQue) {
                if (sendDataQue.size() >= SEND_DATA_QUE_LIMIT) {
                    return false;
                } else {
                    sendDataQue.add(data);
                    return true;
                }
            }
        }

        /**
         *
         */
        @Override
        public void run() {
            sending = true;

            try {

                while(sending) {

                    while (sending && sendDataQue.size() > 0) {
                        byte[] bytes = null;

                        synchronized (sendDataQue) {
                            bytes = sendDataQue.get(0);
                            sendDataQue.remove(0);
                        }

                        if (bytes == null) continue;

                        OutputStream outputStream = socket.getOutputStream();

                        // send data header (20bytes)
                        outputStream.write(DATA_TOP_MARK);

                        // send data id (20bytes)
                        String dataId = createDataId();
                        byte[] byDataId = dataId.getBytes();
                        outputStream.write(byDataId);
                        //Logger.addLog(String.format("dataId = %s", dataId));

                        // send md5 (32bytes)
                        String md5 = getMD5(bytes);
                        byte[] byMd5 = md5.getBytes();
                        outputStream.write(byMd5);
                        //Logger.addLog(String.format("md5 = %s", md5));

                        // send data size (4bytes)
                        byte[] byDatasize = ByteBuffer.allocate(4).putInt(bytes.length).array();
                        outputStream.write(byDatasize);
                        //Logger.addLog(String.format("datasize = %d", bytes.length));

                        // send data
                        outputStream.write(bytes);

                        // execute callback
                        if (sending && (sentDataCallback != null)) {
                            sentDataCallback.onSentData(device);
                        }
                    }

                    Thread.sleep(10);
                }

            } catch(Exception e) {
                e.printStackTrace();
                sending = false;
            }
        }


    }

    /**
     * thread for receiving data from connected BluetoothDevice
     */
    class ReceiveDataThread extends Thread {
        public boolean receiving;
        private BluetoothDevice device;
        private BluetoothSocket socket;
        private BluetoothEventListener receivedDataCallback;

        /**
         * constructor
         */
        public ReceiveDataThread(BluetoothDevice dev, BluetoothSocket socket, BluetoothEventListener callback) {
            this.device = dev;
            this.socket = socket;
            this.receivedDataCallback = callback;
        }

        /**
         *
         * @param socket
         */
        public void setSocket(BluetoothSocket socket) {
            this.socket = socket;
        }

        /**
         *
         * @param callback
         */
        public void setReceivedDataCallback(BluetoothEventListener callback) {
            receivedDataCallback = callback;
        }

        /**
         *
         */
        @Override
        public void run() {
            receiving = true;

            try {
                InputStream inputStream = socket.getInputStream();

                byte[] byDataHeader = new byte[20];
                byte[] byDataId = new byte[20];
                byte[] byMd5 = new byte[32];
                byte[] byDatasize = new byte[4];
                byte[] byData = new byte[PACKET_SIZE];

                String dataId = "";
                String md5 = "";
                int datasize = 0;

                int offset = 0;
                int topOfDataid = byDataHeader.length;
                int topOfMd5 = byDataHeader.length + byDataId.length;
                int topOfDatasize = byDataHeader.length + byDataId.length + byMd5.length;
                int topOfData = byDataHeader.length + byDataId.length + byMd5.length + byDatasize.length;

                while(receiving) {

                    // data header (20bytes)
                    if (offset < topOfDataid) {
                        // read data header
                        int bytes = inputStream.read(byDataHeader, offset, byDataHeader.length - offset);
                        if (bytes > 0) {
                            // check data header per byte
                            Logger.addLog(new String(byDataHeader, "UTF-8"));
                            for (int i=0; i<bytes; i++) {
                                if (byDataHeader[i] == DATA_TOP_MARK[offset]) {
                                    offset++;
                                } else {
                                    offset = 0;
                                }
                            }
                        }
                    }
                    // data id (20bytes)
                    else if (offset < topOfMd5) {
                        // read data id
                        int bytes = inputStream.read(byDataId, offset - topOfDataid, byDataId.length - (offset - topOfDataid));
                        if (bytes > 0) {
                            offset += bytes;
                            if (offset == byDataId.length) {
                                dataId = new String(byDataId, "UTF-8");
                                //Logger.addLog(String.format("dataId = %s", dataId));
                            }
                        }
                    }
                    // md5 (32bytes)
                    else if (offset < topOfDatasize) {
                        // read md5
                        int bytes = inputStream.read(byMd5, offset - topOfMd5, byMd5.length - (offset - topOfMd5));
                        if (bytes > 0) {
                            offset += bytes;
                            if (offset == topOfMd5 + byMd5.length) {
                                md5 = new String(byMd5, "UTF-8");
                                //Logger.addLog(String.format("md5 = %s", md5));
                            }
                        }
                    }
                    // data size (4bytes)
                    else if (offset < topOfData) {
                        // read data size
                        int bytes = inputStream.read(byDatasize, offset - topOfDatasize, byDatasize.length - (offset - topOfDatasize));
                        if (bytes > 0) {
                            offset += bytes;
                            if (offset == topOfDatasize + byDatasize.length) {
                                datasize = ByteBuffer.wrap(byDatasize).getInt();
                                if ((datasize <= 0) || (datasize > DATASIZE_LIMIT)) {
                                    byData = new byte[PACKET_SIZE];
                                } else {
                                    byData = new byte[datasize];
                                }
                                //Logger.addLog(String.format("datasize = %s", datasize));
                            }
                        }
                    }
                    // data
                    else {
                        // read data
                        int bytes;
                        if (DATASIZE_LIMIT < datasize) {
                            bytes = inputStream.read(byData, 0, byData.length);
                        } else {
                            bytes = inputStream.read(byData, offset - topOfData, datasize - (offset - topOfData));
                        }

                        if (bytes > 0) {
                            offset += bytes;
                            if (offset - topOfData == datasize) {
                                if (DATASIZE_LIMIT > datasize) {
                                    String actualMd5 = getMD5(byData);
                                    if (md5.equals(actualMd5)) {
                                        if (receivedDataCallback != null) {
                                            receivedDataCallback.onReceivedData(device, byData);
                                        }
                                        //Logger.addLog("completed to receive data");
                                    } else {
                                        Logger.addLog("completed to receive data : md5 mismatch");
                                    }
                                } else {
                                    Logger.addLog("received data size is over limit");
                                }
                                offset = 0;
                            }
                        }
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                receiving = false;
            }
        }
    }
}
