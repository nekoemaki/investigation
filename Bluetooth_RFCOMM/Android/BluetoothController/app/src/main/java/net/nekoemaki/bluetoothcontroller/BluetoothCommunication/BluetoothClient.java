package net.nekoemaki.bluetoothcontroller.BluetoothCommunication;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import net.nekoemaki.bluetoothcontroller.Common.Logger;

/**
 * BluetoothClient class
 */
public class BluetoothClient {
    private static final UUID UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final int PACKET_SIZE = 512;

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothDevice bluetoothDevice;
    private BluetoothSocket bluetoothSocket;

    private BluetoothEventListener bluetoothEventListener;
    private BluetoothClientTask bluetoothClientTask;

    /**
     * Constructor
     */
    public BluetoothClient() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothDevice = null;
        bluetoothSocket = null;
        bluetoothEventListener = null;
        bluetoothClientTask = null;
    }

    /**
     * get connected status
     * @return
     */
    public boolean isConnected() {
        return (bluetoothSocket != null) && (bluetoothSocket.isConnected());
    }

    /**
     * set BluetoothDevice by device name
     * @param deviceName
     * @return
     */
    public boolean setDevice(String deviceName) {
        if (isConnected()) {
            return false;
        } else {
            BluetoothDevice device = findPairingDeviceByName(deviceName);
            if (device == null) {
                return false;
            } else {
                bluetoothDevice = device;
                return true;
            }
        }
    }

    /**
     * get device name
     * @return
     */
    public String getDevice() {
        return bluetoothDevice.getName();
    }

    /**
     * set BluetoothDevice by device address
     * @param address
     * @return
     */
    public boolean setAddress(String address) {
        if (isConnected()) {
            return false;
        } else {
            BluetoothDevice device = findPairingDeviceByAddress(address);
            if (device == null) {
                return false;
            } else {
                bluetoothDevice = device;
                return true;
            }
        }
    }

    /**
     * get device address
     * @return
     */
    public String getAddress() {
        return bluetoothDevice.getAddress();
    }

    /**
     * set event callback
     * @param callback
     */
    public void setBluetoothEventListener(BluetoothEventListener callback) {
        bluetoothEventListener = callback;
    }

    /**
     * get paired devices
     * @return
     */
    public BluetoothDevice[] getPairingDevices() {
        BluetoothDevice[] ret = null;
        if (bluetoothAdapter != null) {
            Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
            ret = new BluetoothDevice[devices.size()];
            devices.toArray(ret);
        }
        return ret;
    }

    /**
     * find paired BluetoothDevice by device name
     * @param name
     * @return
     */
    private BluetoothDevice findPairingDeviceByName(String name) {
        BluetoothDevice targetDevice = null;
        if (bluetoothAdapter != null) {
            Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
            for (BluetoothDevice device : devices) {
                if (device.getName().equals(name)) {
                    targetDevice = device;
                    break;
                }
            }
        }
        return targetDevice;
    }

    /**
     * find paired BluetoothDevice by address
     * @param address
     * @return
     */
    private BluetoothDevice findPairingDeviceByAddress(String address) {
        BluetoothDevice targetDevice = null;
        if (bluetoothAdapter != null) {
            Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
            for (BluetoothDevice device : devices) {
                if (device.getAddress().equals(address)) {
                    targetDevice = device;
                    break;
                }
            }
        }
        return targetDevice;
    }

    /**
     * connect to BluetoothDevice
     * @return
     */
    public boolean connect() {
        if (!isConnected() && (bluetoothDevice != null)) {
            try {
                Logger.addLog(String.format("connect to %s", bluetoothDevice.getName()));
                bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(UUID_SECURE);
                bluetoothSocket.connect();

                bluetoothClientTask = new BluetoothClientTask(bluetoothSocket, bluetoothEventListener);

                if (bluetoothEventListener != null) {
                    bluetoothEventListener.onConnected(bluetoothDevice);
                }

                return true;
            } catch (Exception e) {
                e.printStackTrace();
                Logger.addLog("connection failed");

                if (bluetoothClientTask != null) {
                    bluetoothClientTask.disconnect();
                    bluetoothClientTask = null;
                }

                if (bluetoothSocket != null) {
                    try {
                        bluetoothSocket.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    bluetoothSocket = null;
                }

                if (bluetoothEventListener != null) {
                    bluetoothEventListener.onConnectFailed(bluetoothDevice);
                }

                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * connect to BluetoothDevice by device name
     * @param deviceName
     * @return
     */
    public boolean connectByName(String deviceName) {
        if (isConnected()) {
            return false;
        }

        BluetoothDevice device = findPairingDeviceByName(deviceName);
        if (device == null) {
            return false;
        } else {
            bluetoothDevice = device;
            return connect();
        }
    }

    /**
     * connect to BluetoothDevice by device address
     * @param address
     * @return
     */
    public boolean connectByAddress(String address) {
        if (isConnected()) {
            return false;
        }

        BluetoothDevice device = findPairingDeviceByAddress(address);
        if (device == null) {
            return false;
        } else {
            bluetoothDevice = device;
            return connect();
        }
    }

    /**
     * disconnect BluetoothDevice
     */
    public void disconnect() {
        if (bluetoothClientTask != null) {
            bluetoothClientTask.disconnect();
            bluetoothClientTask = null;
        }

        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bluetoothSocket = null;
        }

        if (bluetoothEventListener != null){
            bluetoothEventListener.onDisconnected(bluetoothDevice);
        }
    }

    /**
     * send data to connected BluetoothDevice
     * @return
     */
    public boolean sendData(byte[] data) {
        if (bluetoothClientTask != null) {
            bluetoothClientTask.sendData(data);
            return true;
        } else {
            return false;
        }
    }
}

