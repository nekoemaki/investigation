'''

画像からオブジェクトを検出し、その矩形領域とラベル、一致度を出力する

'''

import argparse
import matplotlib.pyplot as plt

import chainer

from chainercv.links import FasterRCNNVGG16
from chainercv import utils
from chainercv.visualizations import vis_bbox


def main():
    parser = argparse.ArgumentParser()

    # 画像ファイルパス
    parser.add_argument('image')

    args = parser.parse_args()

    # ラベル一覧の読み込み
    label_file = open('./TrainData/ImageSets/Main/labels.txt')
    labels = label_file.read()
    label_file.close()
    self.label_list = labels.split('\n')

    # 学習済みモデルの読み込み
    model = FasterRCNNVGG16(
        n_fg_class=len(self.label_list),
        pretrained_model=args.pretrained_model)

    # GPUを使う場合
    # chainer.cuda.get_device_from_id(GPU_DEVICE_ID).use()
    # model.to_gpu()

    # 引数で指定された画像ファイルを読込
    img = utils.read_image(args.image, color=True)

    # オブジェクト検出を実行
    #  bboxes ... オブジェクト検出した矩形領域(left, top, right, bottom)
    #  labels ... 検出したオブジェクトのラベル(ラベル名配列のインデックス)
    #  scores ... 検出したオブジェクトの一致度(0.0〜1.0)
    #  bboxes, labels, scoresは2次元配列になっている
    # （bboxes[idx1][idx2], labels[idx1][idx2], scores[idx1][idx2]で1つのオブジェクトを表す ... idx1は0固定?）
    bboxes, labels, scores = model.predict([img])

    # コンソールに結果を表示
    for idx1 in range(len(bboxes)) :
        for idx2 in range(len(bboxes[idx1])) :
            print('idx1={}, idx2={}, bbox={}, label={}, score={}'.format(idx1, idx2, bboxes[idx1][idx2], voc_bbox_label_names[labels[idx1][idx2]], scores[idx1][idx2]))


if __name__ == '__main__':
    main()
