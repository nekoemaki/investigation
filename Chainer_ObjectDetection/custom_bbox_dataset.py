"""
画像データ、矩形座標データ、ラベルデータを持ったデータセットのクラス

下記のディレクトリ構造で学習データを作成しておく

TrainData --- ImageSets --- train.txt     ... Trainingに使用するJPEG、AnnotationXMLファイル名（拡張子無し）を列挙したファイル
           |             |- test.txt      ... Testに使用するJPEG、AnnotationXMLファイル名（拡張子無し）を列挙したファイル
           |             |- labels.txt    ... ラベル名称を列挙したファイル
           |
           |- JPEGImages --- XXXX.jpg     ... 学習に使用する画像ファイル
           |                                  対になるAnnotationXMLファイルとファイル名は同じものにしておく
           |
           |- Annotations --- XXXX.xml    ... 学習に使用するAnnotationXMLファイル
                                              labelImgで作成したXML
                                             対になるJPEGファイルとファイル名は同じものにしておく
"""

import numpy as np
import os
import warnings
import xml.etree.ElementTree as ET

from chainercv.chainer_experimental.datasets.sliceable import GetterDataset
from chainercv.utils import read_image


class CustomBboxDataset(GetterDataset):

    def __init__(self, data_dir='TrainData', data_type='train',
                 use_difficult=False, return_difficult=False):
        super(CustomBboxDataset, self).__init__()

        # data_typeはtrainかtestだけ
        if data_type not in ['train', 'test']:
            warnings.warn('wrong data_type')

        # JPEG,AnnotationXMLのファイル名リストのファイルパス
        id_list_file = os.path.join(
            data_dir, 'ImageSets/{0}.txt'.format(data_type))

        # ファイル名リストの読み込み
        self.ids = [id_.strip() for id_ in open(id_list_file)]

        self.data_dir = data_dir
        self.use_difficult = use_difficult

        # ラベルリストの読み込み
        label_file = open(os.path.join(data_dir, 'ImageSets/labels.txt'))
        labels = label_file.read()
        label_file.close()
        self.label_list = labels.split('\n')

        self.add_getter('img', self._get_image)
        self.add_getter(('bbox', 'label', 'difficult'), self._get_annotations)

        if not return_difficult:
            self.keys = ('img', 'bbox', 'label')

    # ラベルリストを返す
    def get_labels(self):
        return self.label_list

    # ID(読み込んだファイル)の数を返す
    def __len__(self):
        return len(self.ids)

    # JPEGファイルの読み込み
    def _get_image(self, i):
        id_ = self.ids[i]
        img_path = os.path.join(self.data_dir, 'JPEGImages', id_ + '.jpg')
        img = read_image(img_path, color=True)
        return img

    # AnnotationXMLファイルの読み込み
    def _get_annotations(self, i):
        id_ = self.ids[i]
        anno = ET.parse(
            os.path.join(self.data_dir, 'Annotations', id_ + '.xml'))
        bbox = []
        label = []
        difficult = []
        for obj in anno.findall('object'):
            if not self.use_difficult and int(obj.find('difficult').text) == 1:
                continue

            difficult.append(int(obj.find('difficult').text))
            bndbox_anno = obj.find('bndbox')
            # subtract 1 to make pixel indexes 0-based
            bbox.append([
                int(bndbox_anno.find(tag).text) - 1
                for tag in ('ymin', 'xmin', 'ymax', 'xmax')])
            name = obj.find('name').text.lower().strip()
            label.append(self.label_list.index(name))
        bbox = np.stack(bbox).astype(np.float32)
        label = np.stack(label).astype(np.int32)
        # When `use_difficult==False`, all elements in `difficult` are False.
        difficult = np.array(difficult, dtype=np.bool)
        return bbox, label, difficult
