import numpy as np
import sys
import os
import tensorflow as tf
import cv2
import time

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt

from utils import label_map_util
from utils import visualization_utils as vis_util
from mvnc import mvncapi as mvnc

classes = ["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train","tvmonitor"]


# detect objects from image
def detect_object(image_np):
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            image_np_expanded = np.expand_dims(image_np, axis=0)

            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            scores = detection_graph.get_tensor_by_name('detection_scores:0')
            classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')

            (boxes, scores, classes, num_detections) = sess.run(
                [boxes, scores, classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})

            return boxes, classes, scores

# calcuate distance between 2 points
def get_distance(x1, y1, x2, y2):
    return np.sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2))

# find rectangle include (x,y)
def get_box_include_point(boxes, x, y):
    ret = []
    for idx in range(len(boxes)):
        if (boxes[idx][1] <= x) and (boxes[idx][3] >= x) \
            and (boxes[idx][0] <= y) and (boxes[idx][2] >= y):
            ret.append(idx)
    return ret


def interpret_output(output, img_width, img_height):
	w_img = img_width
	h_img = img_height
	print ((w_img, h_img))
	threshold = 0.2
	iou_threshold = 0.5
	num_class = 20
	num_box = 2
	grid_size = 7

	probs = np.zeros((7,7,2,20))
	class_probs = (np.reshape(output[0:980],(7,7,20)))
	scales = (np.reshape(output[980:1078],(7,7,2)))
	boxes = (np.reshape(output[1078:],(7,7,2,4)))
	offset = np.transpose(np.reshape(np.array([np.arange(7)]*14),(2,7,7)),(1,2,0))

	boxes[:,:,:,0] += offset
	boxes[:,:,:,1] += np.transpose(offset,(1,0,2))
	boxes[:,:,:,0:2] = boxes[:,:,:,0:2] / 7.0
	boxes[:,:,:,2] = np.multiply(boxes[:,:,:,2],boxes[:,:,:,2])
	boxes[:,:,:,3] = np.multiply(boxes[:,:,:,3],boxes[:,:,:,3])

	boxes[:,:,:,0] *= w_img
	boxes[:,:,:,1] *= h_img
	boxes[:,:,:,2] *= w_img
	boxes[:,:,:,3] *= h_img

	for i in range(2):
		for j in range(20):
			probs[:,:,i,j] = np.multiply(class_probs[:,:,j],scales[:,:,i])

	filter_mat_probs = np.array(probs>=threshold,dtype='bool')
	filter_mat_boxes = np.nonzero(filter_mat_probs)
	boxes_filtered = boxes[filter_mat_boxes[0],filter_mat_boxes[1],filter_mat_boxes[2]]
	probs_filtered = probs[filter_mat_probs]
	classes_num_filtered = np.argmax(probs,axis=3)[filter_mat_boxes[0],filter_mat_boxes[1],filter_mat_boxes[2]]

	argsort = np.array(np.argsort(probs_filtered))[::-1]
	boxes_filtered = boxes_filtered[argsort]
	probs_filtered = probs_filtered[argsort]
	classes_num_filtered = classes_num_filtered[argsort]

	for i in range(len(boxes_filtered)):
		if probs_filtered[i] == 0 : continue
		for j in range(i+1,len(boxes_filtered)):
			if iou(boxes_filtered[i],boxes_filtered[j]) > iou_threshold :
				probs_filtered[j] = 0.0

	filter_iou = np.array(probs_filtered>0.0,dtype='bool')
	boxes_filtered = boxes_filtered[filter_iou]
	probs_filtered = probs_filtered[filter_iou]
	classes_num_filtered = classes_num_filtered[filter_iou]

	result = []
	for i in range(len(boxes_filtered)):
		result.append([classes[classes_num_filtered[i]],boxes_filtered[i][0],boxes_filtered[i][1],boxes_filtered[i][2],boxes_filtered[i][3],probs_filtered[i]])

	return result

def iou(box1,box2):
	tb = min(box1[0]+0.5*box1[2],box2[0]+0.5*box2[2])-max(box1[0]-0.5*box1[2],box2[0]-0.5*box2[2])
	lr = min(box1[1]+0.5*box1[3],box2[1]+0.5*box2[3])-max(box1[1]-0.5*box1[3],box2[1]-0.5*box2[3])
	if tb < 0 or lr < 0 : intersection = 0
	else : intersection =  tb*lr
	return intersection / (box1[2]*box1[3] + box2[2]*box2[3] - intersection)


if __name__ == '__main__':

    mvnc.SetGlobalOption(mvnc.GlobalOption.LOG_LEVEL, 2)
    devices = mvnc.EnumerateDevices()

    # open movidius device
    device = mvnc.Device(devices[0])
    device.OpenDevice()

    # load movidius trained file
    with open('/home/pi/TensorFlow/Movidius/yoloNCS/graph', mode='rb') as f:
	     blob = f.read()
    graph = device.AllocateGraph(blob)

    # get usb camera capture
    print('get usb camera capture')
    right_camera_cap = cv2.VideoCapture(0)
    left_camera_cap = cv2.VideoCapture(1)

    if (right_camera_cap.isOpened()) and (left_camera_cap.isOpened()):

        # create feature detector
        detector = cv2.ORB_create()

        # crossCheck is True when use bf.match()
        # crossCheck is False when use bf.knnMatch()
        bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=False)

        # get camera capture size
        # imageWidth = right_camera_cap.get(3)
        # imageHeight = right_camera_cap.get(4)

        # set camera capture size
        # right_camera_cap.set(3, 640)
        # right_camera_cap.set(4, 480)

        width = 400
        height = 300

        print('start camera capture')
        while (True):
            start = time.time()

            # load image file by camera
            right_ret, cv_right_img = right_camera_cap.read()
            left_ret, cv_left_img = left_camera_cap.read()

            if (right_ret and left_ret) :

                # translate gray image
                gray_right_img = cv2.cvtColor(cv_right_img, cv2.COLOR_BGR2GRAY)
                gray_left_img = cv2.cvtColor(cv_left_img, cv2.COLOR_BGR2GRAY)

                # merge right and left image (connect side by side)
                cv_right_img = cv2.resize(cv_right_img, (width, height))
                cv_left_img = cv2.resize(cv_left_img, (width, height))
                merge_img = cv2.hconcat([cv_left_img, cv_right_img])

                # get image size
                img_height, img_width = merge_img.shape[:2]

                # detect objects
                print('detect objects')
                graph.LoadTensor(merge_img.astype(np.float16), 'user object')
                out, userobj = graph.GetResult()
                result = interpret_output(out.astype(np.float32), img_width, img_height)

                # get image size
                img_height, img_width = cv_right_img.shape[:2]

                right_boxes = []
                right_classes = []
                right_scores = []
                left_boxes = []
                left_classes = []
                left_scores = []

                print('calculate coordinate')
                # get only objects with score over 0.5
                # resize detected objects coordinate to image size
                for idx in range(len(result)):
                    #print(result[idx])
                    if (result[idx][5] > 50):
                        # coordinate value is between 0 and 1, so less than 0.5 is left, over than 0.5 is right
                        if (result[idx][3] <= img_width):
                            # left object
                            left_boxes.append([
                                result[idx][1],
                                result[idx][2],
                                result[idx][3],
                                result[idx][4]
                            ])
                            left_classes.append(result[idx][0])
                            left_scores.append(result[idx][5])
                        elif (result[idx][1] > img_width):
                            # right object
                            right_boxes.append([
                                result[idx][1] - img_width,
                                result[idx][2],
                                result[idx][3] - img_width,
                                result[idx][4]
                            ])
                            right_classes.append(result[idx][0])
                            right_scores.append(result[idx][5])

                # detect keypoints and features
                right_keypoints, right_descriptor = detector.detectAndCompute(gray_right_img, None)
                left_keypoints, left_descriptor = detector.detectAndCompute(gray_left_img, None)

                # matching keypoints
                matches = bf.knnMatch(right_descriptor, left_descriptor, k=2)
                # "sorted()" can use when bf.match()
                #matches = sorted(matches, key = lambda x:x.distance)

                # array to count matched keypoints included right and left rect
                # matched_boxes[] ... index of right_boxes
                # matched_boxes[][] ... index of left_boxes
                # matched_boxes[][][] ... [0]=number of matched keypoints, [1]=total distance of matched keypoints
                matched_boxes = []
                for idx in range(len(right_boxes)):
                    matched_boxes.append([])
                    for box in left_boxes:
                        matched_boxes[idx].append([0,0])

                matched = []
                for right_dmatch, left_dmatch in matches :
                    if right_dmatch.distance < 0.5 * left_dmatch.distance :
                        matched.append([right_dmatch])
                        # find righ rectangles include this keypoint
                        right_box_indexes = get_box_include_point(
                            right_boxes,
                            right_keypoints[right_dmatch.queryIdx].pt[0],
                            right_keypoints[right_dmatch.queryIdx].pt[1]
                        )
                        # find left rectangles include this keypoint
                        left_box_indexes = get_box_include_point(
                            left_boxes,
                            left_keypoints[right_dmatch.trainIdx].pt[0],
                            left_keypoints[right_dmatch.trainIdx].pt[1]
                        )
                        # count left rectangle matched right rectangle
                        if (len(right_box_indexes) > 0) and (len(left_box_indexes) > 0):
                            #print('right box = {}, left box = {}'.format(right_box_indexes, left_box_indexes))
                            for idx1 in right_box_indexes:
                                for idx2 in left_box_indexes:
                                    matched_boxes[idx1][idx2][0] += 1
                                    matched_boxes[idx1][idx2][1] += get_distance(
                                        right_keypoints[right_dmatch.queryIdx].pt[0],
                                        right_keypoints[right_dmatch.queryIdx].pt[1],
                                        left_keypoints[right_dmatch.trainIdx].pt[0],
                                        left_keypoints[right_dmatch.trainIdx].pt[1]
                                        )

                rect_distance = []
                for idx1 in range(len(matched_boxes)):
                    maxIdx = -1 # left box index
                    maxCount = -1
                    # find left rect that matched right rect
                    for idx2 in range(len(matched_boxes[idx1])):
                        if (maxCount < matched_boxes[idx1][idx2][0]):
                            maxCount = matched_boxes[idx1][idx2][0]
                            maxIdx = idx2
                    if (maxCount > 0):
                        # found matched rect
                        # [label, score, rect_left, rect_top, rect_right, rect_bottom, distance]
                        rect_distance.append([
                            right_classes[idx1],
                            right_scores[idx1],
                            round(right_boxes[idx1][0]), round(right_boxes[idx1][1]),
                            round(right_boxes[idx1][2]), round(right_boxes[idx1][3]),
                            matched_boxes[idx1][maxIdx][1] / matched_boxes[idx1][maxIdx][0]
                            ])
                    else:
                        # not found
                        # [label, score, rect_left, rect_top, rect_right, rect_bottom, distance=0]
                        rect_distance.append([
                            right_classes[idx1],
                            right_scores[idx1],
                            round(right_boxes[idx1][0]), round(right_boxes[idx1][1]),
                            round(right_boxes[idx1][2]), round(right_boxes[idx1][3]),
                            0
                            ])

                # draw rect to right camera image
                font = cv2.FONT_HERSHEY_SIMPLEX
                for idx in range(len(rect_distance)):
                    print(rect_distance[idx])
                    cv_right_img = cv2.rectangle(cv_right_img, (round(rect_distance[idx][2]), round(rect_distance[idx][3])),(round(rect_distance[idx][4]), round(rect_distance[idx][5])), (0,255,0), 2)
                    cv_right_img = cv2.rectangle(cv_right_img, (round(rect_distance[idx][2]), round(rect_distance[idx][3])),(round(rect_distance[idx][2])+15, round(rect_distance[idx][3])+200), (0,255,0), -1)
                    cv2.putText(cv_right_img,
                        '{} : {} : {}'.format(rect_distance[0], rect_distance[1], rect_distance[6]), # label : score : distance
                        (round(rect_distance[idx][2])+2, round(rect_distance[idx][3])), font, 10, (255,255,255), 2, cv2.LINE_AA)

                # show detected result
                cv2.imshow('frame', cv_right_img)

            elapsed_time = time.time() - start
            print('time = {}'.format(elapsed_time))

            # break when 'q' key pressed
            if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

    graph.DeallocateGraph()
    device.CloseDevice()

    right_camera_cap.release()
    left_camera_cap.release()
    cv2.destroyAllWindows()

