import numpy as np
import sys
import os
import argparse
import tensorflow as tf
import cv2

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt

from utils import label_map_util
from utils import visualization_utils as vis_util
from mvnc import mvncapi as mvnc

classes = ["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train","tvmonitor"]

def detect_object(image_np):
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            image_np_expanded = np.expand_dims(image_np, axis=0)

            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            scores = detection_graph.get_tensor_by_name('detection_scores:0')
            classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')

            (boxes, scores, classes, num_detections) = sess.run(
                [boxes, scores, classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})

            return boxes, classes, scores

# calcuate distance between 2 points
def get_distance(x1, y1, x2, y2):
    return np.sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2))

# find rectangle include (x,y)
def get_box_include_point(boxes, x, y):
    ret = []
    for idx in range(len(boxes)):
        if (boxes[idx][1] <= x) and (boxes[idx][3] >= x) \
            and (boxes[idx][0] <= y) and (boxes[idx][2] >= y):
            ret.append(idx)
    return ret


def interpret_output(output, img_width, img_height):
	w_img = img_width
	h_img = img_height
	print ((w_img, h_img))
	threshold = 0.2
	iou_threshold = 0.5
	num_class = 20
	num_box = 2
	grid_size = 7

	probs = np.zeros((7,7,2,20))
	class_probs = (np.reshape(output[0:980],(7,7,20)))
	scales = (np.reshape(output[980:1078],(7,7,2)))
	boxes = (np.reshape(output[1078:],(7,7,2,4)))
	offset = np.transpose(np.reshape(np.array([np.arange(7)]*14),(2,7,7)),(1,2,0))

	boxes[:,:,:,0] += offset
	boxes[:,:,:,1] += np.transpose(offset,(1,0,2))
	boxes[:,:,:,0:2] = boxes[:,:,:,0:2] / 7.0
	boxes[:,:,:,2] = np.multiply(boxes[:,:,:,2],boxes[:,:,:,2])
	boxes[:,:,:,3] = np.multiply(boxes[:,:,:,3],boxes[:,:,:,3])

	boxes[:,:,:,0] *= w_img
	boxes[:,:,:,1] *= h_img
	boxes[:,:,:,2] *= w_img
	boxes[:,:,:,3] *= h_img

	for i in range(2):
		for j in range(20):
			probs[:,:,i,j] = np.multiply(class_probs[:,:,j],scales[:,:,i])

	filter_mat_probs = np.array(probs>=threshold,dtype='bool')
	filter_mat_boxes = np.nonzero(filter_mat_probs)
	boxes_filtered = boxes[filter_mat_boxes[0],filter_mat_boxes[1],filter_mat_boxes[2]]
	probs_filtered = probs[filter_mat_probs]
	classes_num_filtered = np.argmax(probs,axis=3)[filter_mat_boxes[0],filter_mat_boxes[1],filter_mat_boxes[2]]

	argsort = np.array(np.argsort(probs_filtered))[::-1]
	boxes_filtered = boxes_filtered[argsort]
	probs_filtered = probs_filtered[argsort]
	classes_num_filtered = classes_num_filtered[argsort]

	for i in range(len(boxes_filtered)):
		if probs_filtered[i] == 0 : continue
		for j in range(i+1,len(boxes_filtered)):
			if iou(boxes_filtered[i],boxes_filtered[j]) > iou_threshold :
				probs_filtered[j] = 0.0

	filter_iou = np.array(probs_filtered>0.0,dtype='bool')
	boxes_filtered = boxes_filtered[filter_iou]
	probs_filtered = probs_filtered[filter_iou]
	classes_num_filtered = classes_num_filtered[filter_iou]

	result = []
	for i in range(len(boxes_filtered)):
		result.append([classes[classes_num_filtered[i]],boxes_filtered[i][0],boxes_filtered[i][1],boxes_filtered[i][2],boxes_filtered[i][3],probs_filtered[i]])

	return result

def iou(box1,box2):
	tb = min(box1[0]+0.5*box1[2],box2[0]+0.5*box2[2])-max(box1[0]-0.5*box1[2],box2[0]-0.5*box2[2])
	lr = min(box1[1]+0.5*box1[3],box2[1]+0.5*box2[3])-max(box1[1]-0.5*box1[3],box2[1]-0.5*box2[3])
	if tb < 0 or lr < 0 : intersection = 0
	else : intersection =  tb*lr
	return intersection / (box1[2]*box1[3] + box2[2]*box2[3] - intersection)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('image')
    args = parser.parse_args()

    mvnc.SetGlobalOption(mvnc.GlobalOption.LOG_LEVEL, 2)
    devices = mvnc.EnumerateDevices()

    # open movidius device
    device = mvnc.Device(devices[0])
    device.OpenDevice()

    # load movidius trained file
    with open('/home/pi/TensorFlow/Movidius/yoloNCS/graph', mode='rb') as f:
	     blob = f.read()
    graph = device.AllocateGraph(blob)

    # load label data
    label_map = label_map_util.load_labelmap('/home/pi/TensorFlow/ObjectDetection/data/mscoco_label_map.pbtxt')
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=90, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    # load image file by OpenCV
    print('load image file')
    cv_img = cv2.imread(args.image)
    dim=(448,448)
    resized_img = cv2.resize(cv_img.copy()/255.0, dim)

    img_height, img_width = cv_img.shape[:2]

    print('detect from image')
    graph.LoadTensor(resized_img.astype(np.float16), 'user object')
    out, userobj = graph.GetResult()

    result = interpret_output(out.astype(np.float32), img_width, img_height)

    font = cv2.FONT_HERSHEY_SIMPLEX

    for idx in range(len(result)):
        cv_img = cv2.rectangle(cv_img, \
            (result[idx][1], result[idx][2]), (result[idx][3], result[idx][4]), \
            (0, 255, 0), 2)
        cv2.putText(cv_img, '{} - score={}'.format(result[idx][0], result[idx][5]), \
            (result[idx][1], result[idx][2]), \
            font, 4, (255,255,255), 2, cv2.LINE_AA)
        print('{} : ({}, {}, {}, {}) {}'.format(result[idx][0], result[idx][1], result[idx][2], result[idx][3], result[idx][4], result[idx][5]))

    graph.DeallocateGraph()
    device.CloseDevice()

    print('show image')
    cv2.imshow('detected', cv_img)
    cv2.waitKey(1)
    cv2.destroyAllWindows()
    print('finished')


