import numpy as np
import sys
import os
import tensorflow as tf
import cv2

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt

from utils import label_map_util
from utils import visualization_utils as vis_util


# detect objects from image
def detect_object(image_np):
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            image_np_expanded = np.expand_dims(image_np, axis=0)

            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            scores = detection_graph.get_tensor_by_name('detection_scores:0')
            classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')

            (boxes, scores, classes, num_detections) = sess.run(
                [boxes, scores, classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})

            return boxes, classes, scores

# calculate distance between 2 points
def get_distance(x1, y1, x2, y2):
    return np.sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2))

# find rectangle include (x,y)
def get_box_include_point(boxes, x, y):
    ret = []
    for idx in range(len(boxes)):
        if (boxes[idx][1] <= x) and (boxes[idx][3] >= x) \
            and (boxes[idx][0] <= y) and (boxes[idx][2] >= y):
            ret.append(idx)
    return ret


if __name__ == '__main__':

    # load trained data
    detection_graph = tf.Graph()
    with detection_graph.as_default():
      od_graph_def = tf.GraphDef()
      with tf.gfile.GFile('data/ssd_inception_v2.pb', 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    # load label data
    label_map = label_map_util.load_labelmap('data/mscoco_label_map.pbtxt')
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=90, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    # get usb camera capture
    right_camera_cap = cv2.VideoCapture(0)
    left_camera_cap = cv2.VideoCapture(1)

    if (right_camera_cap.isOpened()) and (left_camera_cap.isOpened()):

        # create feature detector
        detector = cv2.ORB_create()

        # crossCheck is True when use bf.match()
        # crossCheck is False when use bf.knnMatch()
        bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=False)

        width = 400
        height = 300

        while (True):

            # load image file by camera
            right_ret, cv_right_img = right_camera_cap.read()
            left_ret, cv_left_img = left_camera_cap.read()

            if (right_ret and left_ret) :

                # translate gray image
                gray_right_img = cv2.cvtColor(cv_right_img, cv2.COLOR_BGR2GRAY)
                gray_left_img = cv2.cvtColor(cv_left_img, cv2.COLOR_BGR2GRAY)

                # merge right and left image (connect side by side)
                cv_right_img = cv2.resize(cv_right_img, (width, height))
                cv_left_img = cv2.resize(cv_left_img, (width, height))
                merge_img = cv2.hconcat([cv_left_img, cv_right_img])

                # detect objects
                tmp_merge_boxes, tmp_merge_classes, tmp_merge_scores = detect_object(merge_img)

                # get image size
                img_height, img_width = cv_right_img.shape[:2]

                right_boxes = []
                right_classes = []
                right_scores = []
                left_boxes = []
                left_classes = []
                left_scores = []

                print('calculate coordinate')
                # get only objects with score over 0.5
                # resize detected objects coordinate to image size
                for idx in range(len(tmp_merge_boxes[0])):
                    if (tmp_merge_scores[0][idx] > 0.5):
                        # coordinate value is between 0 and 1, so less than 0.5 is left, over than 0.5 is right
                        if (tmp_merge_boxes[0][idx][3] <= 0.5):
                            # left object
                            left_boxes.append([
                                tmp_merge_boxes[0][idx][0] * img_height,
                                tmp_merge_boxes[0][idx][1] * img_width,
                                tmp_merge_boxes[0][idx][2] * img_height,
                                tmp_merge_boxes[0][idx][3] * img_width
                            ])
                            left_classes.append(tmp_merge_classes[0][idx])
                            left_scores.append(tmp_merge_scores[0][idx])
                        elif (tmp_merge_boxes[0][idx][1] > 0.5):
                            # right object
                            right_boxes.append([
                                tmp_merge_boxes[0][idx][0] * img_height,
                                tmp_merge_boxes[0][idx][1] * img_width - img_width,
                                tmp_merge_boxes[0][idx][2] * img_height,
                                tmp_merge_boxes[0][idx][3] * img_width - img_width
                            ])
                            right_classes.append(tmp_merge_classes[0][idx])
                            right_scores.append(tmp_merge_scores[0][idx])

                # detect keypoints and features
                right_keypoints, right_descriptor = detector.detectAndCompute(gray_right_img, None)
                left_keypoints, left_descriptor = detector.detectAndCompute(gray_left_img, None)

                # matching keypoints
                matches = bf.knnMatch(right_descriptor, left_descriptor, k=2)
                # "sorted()" can use when bf.match()
                #matches = sorted(matches, key = lambda x:x.distance)

                # array to count matched keypoints included right and left rect
                # matched_boxes[] ... index of right_boxes
                # matched_boxes[][] ... index of left_boxes
                # matched_boxes[][][] ... [0]=number of matched keypoints, [1]=total distance of matched keypoints
                matched_boxes = []
                for idx in range(len(right_boxes)):
                    matched_boxes.append([])
                    for box in left_boxes:
                        matched_boxes[idx].append([0,0])

                matched = []
                for right_dmatch, left_dmatch in matches :
                    if right_dmatch.distance < 0.5 * left_dmatch.distance :
                        matched.append([right_dmatch])
                        # find righ rectangles include this keypoint
                        right_box_indexes = get_box_include_point(
                            right_boxes,
                            right_keypoints[right_dmatch.queryIdx].pt[0],
                            right_keypoints[right_dmatch.queryIdx].pt[1]
                        )
                        # find left rectangles include this keypoint
                        left_box_indexes = get_box_include_point(
                            left_boxes,
                            left_keypoints[right_dmatch.trainIdx].pt[0],
                            left_keypoints[right_dmatch.trainIdx].pt[1]
                        )
                        # count left rectangle matched right rectangle
                        if (len(right_box_indexes) > 0) and (len(left_box_indexes) > 0):
                            #print('right box = {}, left box = {}'.format(right_box_indexes, left_box_indexes))
                            for idx1 in right_box_indexes:
                                for idx2 in left_box_indexes:
                                    matched_boxes[idx1][idx2][0] += 1
                                    matched_boxes[idx1][idx2][1] += get_distance(
                                        right_keypoints[right_dmatch.queryIdx].pt[0],
                                        right_keypoints[right_dmatch.queryIdx].pt[1],
                                        left_keypoints[right_dmatch.trainIdx].pt[0],
                                        left_keypoints[right_dmatch.trainIdx].pt[1]
                                        )

                rect_distance = []
                for idx1 in range(len(matched_boxes)):
                    maxIdx = -1 # left box index
                    maxCount = -1
                    # find left rect that matched right rect
                    for idx2 in range(len(matched_boxes[idx1])):
                        if (maxCount < matched_boxes[idx1][idx2][0]):
                            maxCount = matched_boxes[idx1][idx2][0]
                            maxIdx = idx2
                    if (maxCount > 0):
                        # found matched rect
                        # [label, score, rect_left, rect_top, rect_right, rect_bottom, distance]
                        rect_distance.append([
                            category_index[right_classes[idx1]],
                            right_scores[idx1],
                            right_boxes[idx1][1], right_boxes[idx1][0],
                            right_boxes[idx1][3], right_boxes[idx1][2],
                            matched_boxes[idx1][maxIdx][1] / matched_boxes[idx1][maxIdx][0]
                            ])
                    else:
                        # not found
                        # [label, score, rect_left, rect_top, rect_right, rect_bottom, distance=0]
                        rect_distance.append([
                            category_index[right_classes[idx1]],
                            right_scores[idx1],
                            right_boxes[idx1][1], right_boxes[idx1][0],
                            right_boxes[idx1][3], right_boxes[idx1][2],
                            0
                            ])

                # draw rect to right camera image
                font = cv2.FONT_HERSHEY_SIMPLEX
                for idx in range(len(rect_distance)):
                    cv_right_img = cv2.rectangle(cv_right_img, (rect_distance[2], rect_distance[3]),(rect_distance[4], rect_distance[5]), (0,255,0), 2)
                    cv_right_img = cv2.rectangle(cv_right_img, (rect_distance[2], rect_distance[3]),(rect_distance[2]+15, rect_distance[3]+200), (0,255,0), -1)
                    cv2.putText(cv_right_img,
                        '{} : {} : {}'.format(rect_distance[0], rect_distance[1], rect_distance[6]), # label : score : distance
                        (rect_distance[2]+2, rect_distance[3]), font, 10, (255,255,255), 2, cv2.LINE_AA)

                # show detected result
                cv2.imshow('frame', cv_right_img)

            # break when 'q' key pressed
            if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

    right_camera_cap.release()
    left_camera_cap.release()
    cv2.destroyAllWindows()


