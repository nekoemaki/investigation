package net.nekoemaki.objectdetection_tflite;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.YuvImage;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.view.Surface;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CameraController {
    private Context context;
    private String currentCameraId;
    private CameraManager cameraManager;
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSession;
    private SurfaceTexture surfaceTexture;
    private Surface surface;
    private ImageReader imageReader;
    private Point cameraImageSize;
    private Handler handler;
    private CaptureRequest.Builder previewRequestBuilder;

    public CameraPreviewListener cameraPreviewListener;

    /**
     *
     * @param context
     */
    public CameraController(Context context, SurfaceTexture surfaceTexture) {
        this.context = context;
        this.surfaceTexture = surfaceTexture;
        this.cameraImageSize = new Point(0, 0);
        this.cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
    }

    /**
     *
     * @return
     */
    public String[] GetCameraIDs() {
        String[] ret = null;
        try {
            ret = this.cameraManager.getCameraIdList();
        } finally {
            return ret;
        }
    }

    /**
     *
     * @param width
     * @param height
     */
    public void SetImageSize(int width, int height) {
        this.cameraImageSize.set(width, height);

        if (this.cameraCaptureSession != null) {
            try {
                this.cameraCaptureSession.stopRepeating();

                this.previewRequestBuilder.removeTarget(this.imageReader.getSurface());
                imageReader.close();

                this.imageReader = ImageReader.newInstance(this.cameraImageSize.x, this.cameraImageSize.y, ImageFormat.JPEG, 2);
                this.imageReader.setOnImageAvailableListener(onImageAvailableListener, handler);
                this.previewRequestBuilder.addTarget(this.imageReader.getSurface());

                this.cameraCaptureSession.setRepeatingRequest(this.previewRequestBuilder.build(), capturedCallback, handler);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 画像サイズを取得
     * @return
     */
    public Point GetImageSize() {
        return this.cameraImageSize;
    }

    /**
     * カメラが開いているかチェック
     * @return
     */
    public boolean IsOpened() {
        return (this.cameraCaptureSession != null);
    }

    /**
     * カメラを開く
     * @param cameraId
     */
    public void OpenCamera(String cameraId){

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            Logger.addLog("none permission");
            return;
        }

        if (cameraDevice != null){
            CloseCamera();
        }

        try {

            CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            List<Size> previewSizes = Arrays.asList(map.getOutputSizes(SurfaceTexture.class));
            for(Size previewSize : previewSizes) {
                if (cameraImageSize.x < previewSize.getWidth()) {
                    cameraImageSize.set(previewSize.getWidth(), previewSize.getHeight());
                }
                Logger.addLog(String.format("preivew size : %d x %d", previewSize.getWidth(), previewSize.getHeight()));
            }

            Logger.addLog(String.format("set camera size : %dx%d", cameraImageSize.x, cameraImageSize.y));

            handler = new Handler();

            imageReader = ImageReader.newInstance(cameraImageSize.x, cameraImageSize.y, ImageFormat.JPEG, 2);
            imageReader.setOnImageAvailableListener(onImageAvailableListener, handler);

            currentCameraId = cameraId;
            Logger.addLog("camera opening id=" + currentCameraId);
            cameraManager.openCamera(cameraId, openCallback, handler);
        } catch(CameraAccessException e){
            e.printStackTrace();
        }

    }

    /**
     * カメラを閉じる
     */
    public void CloseCamera() {
        Logger.addLog("camera close id=" + currentCameraId);
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }
        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }

    private CameraDevice.StateCallback openCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            Logger.addLog("camera opened id=" + currentCameraId);
            cameraDevice = camera;
            try {

                surfaceTexture.setDefaultBufferSize(cameraImageSize.x, cameraImageSize.y);
                surface = new Surface(surfaceTexture);

                List<Surface> outputs;
                if (surface != null) {
                    outputs = Arrays.asList(surface, imageReader.getSurface());
                } else {
                    outputs = Arrays.asList(imageReader.getSurface());
                }
                cameraDevice.createCaptureSession(outputs, configuredCallback, handler);
            } catch (CameraAccessException e){
                e.printStackTrace();
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            Logger.addLog("camera disconnected id=" + currentCameraId);
            if (cameraDevice != null) {
                cameraDevice.close();
                cameraDevice = null;
            }
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            Log.e("CameraController", "camera open error id=" + currentCameraId);
            if (cameraDevice != null) {
                cameraDevice.close();
                cameraDevice = null;
            }
        }
    };

    private CameraCaptureSession.StateCallback configuredCallback = new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
            Logger.addLog("camera configured id=" + currentCameraId);
            cameraCaptureSession = session;
            try{

                previewRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                previewRequestBuilder.addTarget(imageReader.getSurface());

                if (surface != null) {
                    previewRequestBuilder.addTarget(surface);
                }
                cameraCaptureSession.setRepeatingRequest(previewRequestBuilder.build(), capturedCallback, handler);

            } catch(CameraAccessException e){
                e.printStackTrace();
            }
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
            Log.e("CameraController", "camera configured error2 id=" + currentCameraId);
        }
    };

    private CameraCaptureSession.CaptureCallback capturedCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);

        }
    };

    private ImageReader.OnImageAvailableListener onImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {

            // ImageReaderから画像データ(byte配列)を取得 (YUV)
            Image image = reader.acquireLatestImage();
            if (image == null) return;

            Image.Plane[] planes = image.getPlanes();
            if (planes == null) return;

            Image.Plane plane = image.getPlanes()[0];
            ByteBuffer buf = plane.getBuffer();
            byte[] b = new byte[buf.remaining()];
            buf.get(b);

            if ((cameraPreviewListener != null) && (b != null)) {
                cameraPreviewListener.CameraPreview(b);
            }

            if (image != null) image.close();

        }
    };


    private int TimestampToKey(long timestamp){
        double v = (double) (timestamp / 1000000000.0);
        double v2 = v - (int)v;
        //9,8桁目のみ使用
        return (int)(v2 * 100);
    }

}
