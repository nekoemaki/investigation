package net.nekoemaki.objectdetection_tflite;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.widget.Toast;

import org.tensorflow.demo.Classifier;
import org.tensorflow.demo.TFLiteObjectDetectionAPIModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ObjectDetector {
    private static final int TF_OD_API_INPUT_SIZE = 300;
    private static final boolean TF_OD_API_IS_QUANTIZED = false;
    private static final String TF_OD_API_MODEL_FILE = "detect.tflite";
    private static final String TF_OD_API_LABELS_FILE = "file:///android_asset/coco_labels_list.txt";

    private int previewWidth = 720;
    private int previewHeight = 1280;

    private Classifier detector;

    private int cropSize;
    private Matrix frameToCropTransform;

    /**
     * コンストラクタ
     * @param context
     * @param assetManager
     */
    public ObjectDetector(Context context, AssetManager assetManager) {
        cropSize = TF_OD_API_INPUT_SIZE;
        try {
            detector =
                    TFLiteObjectDetectionAPIModel.create(
                            assetManager,
                            TF_OD_API_MODEL_FILE,
                            TF_OD_API_LABELS_FILE,
                            TF_OD_API_INPUT_SIZE,
                            TF_OD_API_IS_QUANTIZED);
            cropSize = TF_OD_API_INPUT_SIZE;
        } catch (final IOException e) {
            Toast toast =
                    Toast.makeText(
                            context, "Classifier could not be initialized", Toast.LENGTH_SHORT);
            toast.show();
        }

        frameToCropTransform =
                getTransformationMatrix(
                        previewWidth, previewHeight,
                        cropSize, cropSize,
                        0, false);

        // 検出するオブジェクトのラベルを設定
        String[] classNames = ((TFLiteObjectDetectionAPIModel)detector).getClassNames();
        ((TFLiteObjectDetectionAPIModel)detector).addTargetClass(Arrays.asList(classNames).indexOf("person"));
        ((TFLiteObjectDetectionAPIModel)detector).addTargetClass(Arrays.asList(classNames).indexOf("bicycle"));
        ((TFLiteObjectDetectionAPIModel)detector).addTargetClass(Arrays.asList(classNames).indexOf("car"));
        ((TFLiteObjectDetectionAPIModel)detector).addTargetClass(Arrays.asList(classNames).indexOf("motorcycle"));
        ((TFLiteObjectDetectionAPIModel)detector).addTargetClass(Arrays.asList(classNames).indexOf("bus"));
        ((TFLiteObjectDetectionAPIModel)detector).addTargetClass(Arrays.asList(classNames).indexOf("truck"));
        ((TFLiteObjectDetectionAPIModel)detector).addTargetClass(Arrays.asList(classNames).indexOf("keyboard"));
        ((TFLiteObjectDetectionAPIModel)detector).addTargetClass(Arrays.asList(classNames).indexOf("mouse"));
        ((TFLiteObjectDetectionAPIModel)detector).addTargetClass(Arrays.asList(classNames).indexOf("chair"));
        ((TFLiteObjectDetectionAPIModel)detector).setTargetScore(0.7f);
    }

    /**
     * ObjectDetectorに渡す画像のサイズを変更
     * @param width
     * @param height
     */
    public void setPreviewSize(int width, int height) {
        previewWidth = width;
        previewHeight = height;
        frameToCropTransform =
                getTransformationMatrix(
                        previewWidth, previewHeight,
                        cropSize, cropSize,
                        0, false);
    }

    public int[] getPreviewSize() {
        int[] ret = {previewWidth, previewHeight};
        return ret;
    }

    /**
     * 画像からオブジェクトを検出
     * @param img
     * @return
     */
    public List<Classifier.Recognition> detectImage(Bitmap img) {
        // 設定されているプレビューサイズと受け取った画像サイズが違っている場合、frameToCropTransformが使用できないので処理できない
        if ((img.getWidth() != previewWidth) || (img.getHeight() != previewHeight)) {
            Logger.addLog(String.format("ObjectDetector.detectImage : not match image size. : request=%dx%d / actual=%dx%d",
                    previewWidth, previewHeight, img.getWidth(), img.getHeight()));
            return null;
        }

        long starttime = System.currentTimeMillis();

        List<Classifier.Recognition> ret =null;

        try {
            // detectorで使用できる画像は縦横サイズが300x300の画像なので画像サイズをリサイズ
            Bitmap resizedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), frameToCropTransform, true);

            // 画像サイズのリサイズ比率
            float widthRatio = (float) img.getWidth() / (float) cropSize;
            float heightRatio = (float) img.getHeight() / (float) cropSize;

            // オブジェクトを検出
            ret = detector.recognizeImage(resizedImg);

            resizedImg.recycle();

            if (ret != null) {
                for (Classifier.Recognition rec : ret) {
                    // 検出した矩形の座標を元画像サイズでの座標にする
                    RectF rect = rec.getLocation();
                    rect.set(
                            rect.left * widthRatio,
                            rect.top * heightRatio,
                            rect.right * widthRatio,
                            rect.bottom * heightRatio);
                    rec.setLocation(rect);
                }
                Logger.addLog(String.format("detected object : count=%d", ret.size()));
            } else {
                Logger.addLog("cannot detect object");
            }
        } catch (Exception e){
            e.printStackTrace();
            Logger.addErrorLog(e.toString());
        }

        Logger.addLog(String.format("detect time = %dms", (System.currentTimeMillis() - starttime)));

        return ret;
    }

    /**
     * 2点間の距離を算出
     * @param p1
     * @param p2
     * @return
     */
    private float getDistance(Point p1, Point p2) {
        return (float)(Math.sqrt(Math.pow(p1.x-p2.x, 2) + Math.pow(p1.y-p2.y, 2)));
    }

    /**
     * 座標(pt)を含む矩形領域をrectsの中から探してrectsのインデックスを返す
     * @param rects
     * @param pt
     * @return
     */
    private List<Integer> getBoxIncludePoint(List<Classifier.Recognition> rects, Point pt) {
        ArrayList<Integer> ret = new ArrayList<>();

        for (int idx=0; idx<rects.size(); idx++) {
            Classifier.Recognition rec = rects.get(idx);
            if ((rec.getLocation().left <= pt.x) && (rec.getLocation().right >= pt.x)
                    && (rec.getLocation().top <= pt.y) && (rec.getLocation().bottom >= pt.y)) {
                ret.add(idx);
            }
        }

        return ret;
    }

    private Matrix getTransformationMatrix(
            final int srcWidth,
            final int srcHeight,
            final int dstWidth,
            final int dstHeight,
            final int applyRotation,
            final boolean maintainAspectRatio) {
        final Matrix matrix = new Matrix();

        if (applyRotation != 0) {
            if (applyRotation % 90 != 0) {
                Logger.addWarningLog(String.format("Rotation of %d % 90 != 0", applyRotation));
            }
            // Translate so center of image is at origin.
            matrix.postTranslate(-srcWidth / 2.0f, -srcHeight / 2.0f);
            // Rotate around origin.
            matrix.postRotate(applyRotation);
        }

        // Account for the already applied rotation, if any, and then determine how
        // much scaling is needed for each axis.
        final boolean transpose = (Math.abs(applyRotation) + 90) % 180 == 0;

        final int inWidth = transpose ? srcHeight : srcWidth;
        final int inHeight = transpose ? srcWidth : srcHeight;

        // Apply scaling if necessary.
        if (inWidth != dstWidth || inHeight != dstHeight) {
            final float scaleFactorX = dstWidth / (float) inWidth;
            final float scaleFactorY = dstHeight / (float) inHeight;

            if (maintainAspectRatio) {
                // Scale by minimum factor so that dst is filled completely while
                // maintaining the aspect ratio. Some image may fall off the edge.
                final float scaleFactor = Math.max(scaleFactorX, scaleFactorY);
                matrix.postScale(scaleFactor, scaleFactor);
            } else {
                // Scale exactly to fill dst from src.
                matrix.postScale(scaleFactorX, scaleFactorY);
            }
        }

        if (applyRotation != 0) {
            // Translate back from origin centered reference to destination frame.
            matrix.postTranslate(dstWidth / 2.0f, dstHeight / 2.0f);
        }

        return matrix;
    }
}
