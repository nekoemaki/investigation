package net.nekoemaki.objectdetection_tflite;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.tensorflow.demo.Classifier;

import java.util.List;

public class ObjectDetectionActivity extends AppCompatActivity {


    private TextureView textureView;
    private ImageView previewImageView;

    private CameraController cameraController;

    private Object imageSyncObject = new Object();

    private Bitmap previewImageBitmap;
    private Canvas previewImageCanvas;
    private Paint previewImagePaint;
    private Paint previewImageTextPaint;

    private ObjectDetector objectDetector;

    private boolean okCameraPermission;
    private boolean okStoragePermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_detection);

        View decor = this.getWindow().getDecorView();
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            Logger.addLog("none camera permission");
            okCameraPermission = false;
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CAMERA
            }, 1);
        } else {
            okCameraPermission = true;
        }

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Logger.addLog("none storage permission");
            okStoragePermission = false;
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 1);
        } else {
            okStoragePermission = true;
            InitializeDetector();
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        WindowManager wm = getWindowManager();
        Display disp = wm.getDefaultDisplay();
        Point dispSizePoint = new Point();
        disp.getSize(dispSizePoint);

        ViewGroup.LayoutParams matchLayout = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        RelativeLayout baseLayout = new RelativeLayout(getApplicationContext());
        baseLayout.setBackgroundColor(Color.BLACK);
        addContentView(baseLayout, matchLayout);

        LinearLayout textureLayout = new LinearLayout(getApplicationContext());
        textureLayout.setOrientation(LinearLayout.VERTICAL);
        textureLayout.setBackgroundColor(Color.BLACK);
        baseLayout.addView(textureLayout, matchLayout);

        textureView = new TextureView(getApplicationContext());
        textureView.setSurfaceTextureListener(surfaceTextureListener);
        textureLayout.addView(textureView, new ViewGroup.LayoutParams(1, 1));

        previewImageView = new ImageView(getApplicationContext());
        baseLayout.addView(previewImageView, matchLayout);

        surfaceTextureAvailable = false;

        //////////////////////////////////////////////////////////////////////////////////////////////////////

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 1) {
            if ((permissions.length > 0) && (permissions.length == grantResults.length)) {
                for (int i=0; i<permissions.length; i++) {
                    if (Manifest.permission.CAMERA.equals(permissions[i]) && (grantResults[i] == PackageManager.PERMISSION_GRANTED)) {
                        Logger.addLog("accept camera permission");
                        okCameraPermission = true;
                        InitializeCamera();
                    }

                    if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(permissions[i]) && (grantResults[i] == PackageManager.PERMISSION_GRANTED)) {
                        Logger.addLog("accept storage permission");
                        okStoragePermission = true;
                        InitializeDetector();
                    }
                }

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    Logger.addLog("none camera permission");
                    okCameraPermission = false;
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA
                    }, 1);
                } else {
                    okCameraPermission = true;
                }

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    Logger.addLog("none storage permission");
                    okStoragePermission = false;
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    }, 1);
                } else {
                    okStoragePermission = true;
                    InitializeDetector();
                }

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        //if (this.cameraController != null) {
        //    this.cameraController.CloseCamera();
        //}

        super.onPause();
    }


    private boolean surfaceTextureAvailable;
    private TextureView.SurfaceTextureListener surfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            surfaceTextureAvailable = true;
            if (surfaceTextureAvailable) {
                InitializeCamera();
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };

    private CameraPreviewListener cameraPreviewListener = new CameraPreviewListener(){
        @Override
        public void CameraPreview(byte[] imageData) {
            super.CameraPreview(imageData);

            if (imageData == null) return;

            synchronized (imageSyncObject) {
                cameraImage = imageData;
            }

            if (!detectionFlag) {
                detectionHandler.post(detectionRunnable);
            }
        }
    };

    private void InitializeDetector() {
        if (okStoragePermission) {
            if (objectDetector == null) {
                objectDetector = new ObjectDetector(getApplicationContext(), getAssets());
            }
        }
    }

    private void InitializeCamera() {
        if (okCameraPermission && surfaceTextureAvailable) {
            if ((cameraController == null) || !cameraController.IsOpened()) {
                cameraController = new CameraController(getApplicationContext(), textureView.getSurfaceTexture());
                cameraController.cameraPreviewListener = cameraPreviewListener;
                String[] cameraIDs = cameraController.GetCameraIDs();
                if ((cameraIDs != null) && (cameraIDs.length > 0)) {
                    cameraController.OpenCamera(cameraIDs[0]);
                } else {
                    Logger.addLog("no camera ids");
                }
            }
        }
    }

    private byte[] cameraImage = null;
    private boolean detectionFlag = false;
    private Handler detectionHandler = new Handler();
    private Runnable detectionRunnable = new Runnable() {
        @Override
        public void run() {
            detectionHandler.removeCallbacks(detectionRunnable);

            if (cameraImage == null) return;

            detectionFlag = true;

            Bitmap imageBitmap;

            synchronized (imageSyncObject) {
                // JPEG to Bitmap
                imageBitmap = BitmapFactory.decodeByteArray(cameraImage, 0, cameraImage.length);
            }

            if (imageBitmap != null) {

                if (previewImageBitmap == null) {
                    previewImageBitmap = imageBitmap.copy(Bitmap.Config.ARGB_8888, true);
                    previewImageCanvas = new Canvas(previewImageBitmap);
                    previewImagePaint = new Paint();
                    previewImagePaint.setColor(Color.GREEN);
                    previewImagePaint.setStyle(Paint.Style.STROKE);
                    previewImagePaint.setStrokeWidth(2);
                    previewImagePaint.setTextSize(20);
                    previewImageTextPaint = new Paint();
                    previewImageTextPaint.setColor(Color.WHITE);
                    previewImageTextPaint.setStyle(Paint.Style.STROKE);
                    previewImageTextPaint.setStrokeWidth(2);
                    previewImageTextPaint.setTextSize(20);
                }

                // カメラ画像を描画
                previewImageCanvas.drawBitmap(imageBitmap, 0, 0, previewImagePaint);

                if (objectDetector != null) {
                    int[] detectorImageSize = objectDetector.getPreviewSize();
                    if ((detectorImageSize[0] != imageBitmap.getWidth()) || (detectorImageSize[1] != imageBitmap.getHeight())) {
                        objectDetector.setPreviewSize(imageBitmap.getWidth(), imageBitmap.getHeight());
                    }

                    // 画像からオブジェクトの検出
                    List<Classifier.Recognition> detectedResult = objectDetector.detectImage(imageBitmap);

                    if (detectedResult.size() > 0) {
                        for (Classifier.Recognition rec : detectedResult) {
                            // 検出したオブジェクトの矩形、ラベル、スコア、視差を描画
                            previewImagePaint.setStyle(Paint.Style.FILL);
                            RectF detectedRect = rec.getLocation();
                            RectF stringBack = new RectF(detectedRect.left, detectedRect.top, detectedRect.right, detectedRect.top+30);
                            previewImageCanvas.drawRect(stringBack, previewImagePaint);
                            previewImagePaint.setStyle(Paint.Style.STROKE);
                            previewImageCanvas.drawRect(detectedRect, previewImagePaint);
                            previewImageCanvas.drawText(
                                    String.format("%s : Score=%.2f%%", rec.getTitle(), (rec.getConfidence() * 100f)),
                                    detectedRect.left, detectedRect.top+25, previewImageTextPaint);
                            Logger.addLog(String.format("%s : Score=%.2f%%", rec.getTitle(), (rec.getConfidence() * 100f)));
                        }
                    }
                }

                // 画面に表示
                previewImageView.setImageBitmap(previewImageBitmap);
            }

            detectionFlag = false;
        }
    };
}
