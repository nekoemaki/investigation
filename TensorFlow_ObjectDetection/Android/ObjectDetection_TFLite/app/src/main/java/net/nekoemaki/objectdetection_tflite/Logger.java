package net.nekoemaki.objectdetection_tflite;

import android.util.Log;

public class Logger {
    public static void addLog(String msg) {
        Log.d("ObjectDetection", msg);
    }

    public static void addWarningLog(String msg) {
        Log.w("ObjectDetection", msg);
    }

    public static void addErrorLog(String msg) {
        Log.e("ObjectDetection", msg);
    }

}
